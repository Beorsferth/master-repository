# Russian Missions

conquer_finland = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_areas_list = {
		finland_coastal_area
		finland_inland_area
		karelia_area
	}
	
	target_provinces = {
		owned_by = SWE
	}
	allow = {
		tag = RUS
		is_free_or_tributary_trigger = yes
		mil = 3
		is_neighbor_of = SWE
		NOT = { war_with = SWE }
		NOT = { alliance_with = SWE }
		NOT = { overlord_of = SWE }
		NOT = { is_subject_of = SWE }
		finland_region = { owned_by = SWE }
		NOT = { finland_region = { owned_by = RUS } }
		NOT = { has_country_modifier = baltic_ambition }
		NOT = { has_country_flag = conquered_finland_region_rus }
		NOT = { last_mission = conquer_finland }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			any_target_province = {
				NOT = { owned_by = ROOT }
				NOT = { owned_by = SWE }
			}
		}
	}
	success = {
		NOT = { war_with = SWE }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 3000
		modifier = {
			factor = 2
			mil = 4
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			if = {
				limit = {
					NOT = {
						is_permanent_claim = ROOT
					}
				}
				remove_claim = ROOT
			}
		}
	}
	effect = {
		add_adm_power = 100
		set_country_flag = conquered_finland_region_rus
		add_country_modifier = {
			name = "baltic_ambition"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}

subjugate_tver = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces = {
		owned_by = TVE
	}
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		is_free_or_tributary_trigger = yes
		NOT = { has_country_flag = tver_subjugated }
		TVE = {
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			NOT = { alliance_with = ROOT }
			religion_group = ROOT
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
		}
		NOT = { has_country_flag = tver_subjugated }
		NOT = { last_mission = subjugate_tver }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = TVE }
			NOT = { religion_group = ROOT }
			TVE = { is_subject_of = ROOT }
		}
	}
	success = {
		NOT = { exists = TVE }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 5000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = TVE value = 0 } }
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			if = {
				limit = {
					NOT = {
						is_permanent_claim = ROOT
					}
				}
				remove_claim = ROOT
			}
		}
	}
	effect = {
		add_prestige = 5
		set_country_flag = tver_subjugated
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}

annex_yaroslavl = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces = {
		owned_by = YAR
	}
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		is_free_or_tributary_trigger = yes
		YAR = {
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			NOT = { alliance_with = ROOT }
			religion_group = ROOT
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
		}
		NOT = { has_country_flag = yaroslavl_subjugated }
		NOT = { last_mission = annex_yaroslavl }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = YAR }
			YAR = { NOT = { religion_group = ROOT } }
			YAR = { is_subject_of = ROOT }
		}
	}
	success = {
		NOT = { exists = YAR }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 3000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = YAR value = 0 } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = YAR value = -100 } }
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			if = {
				limit = {
					NOT = {
						is_permanent_claim = ROOT
					}
				}
				remove_claim = ROOT
			}
		}
	}
	effect = {
		add_prestige = 5
		set_country_flag = yaroslavl_subjugated
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}

annex_ryazan = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces = {
		owned_by = RYA
	}
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		is_free_or_tributary_trigger = yes
		RYA = {
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			NOT = { alliance_with = ROOT }
			religion_group = ROOT
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
		}
		NOT = { has_country_flag = ryazan_subjugated }
		NOT = { last_mission = annex_ryazan }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = RYA }
			RYA = { NOT = { religion_group = ROOT } }
			RYA = { is_subject_of = ROOT }
		}
	}
	success = {
		NOT = { exists = RYA }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 5000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = RYA value = 0 } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = RYA value = -100 } }
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			if = {
				limit = {
					NOT = {
						is_permanent_claim = ROOT
					}
				}
				remove_claim = ROOT
			}
		}
	}
	effect = {
		add_prestige = 5
		set_country_flag = ryazan_subjugated
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}

access_to_the_baltic_sea = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces = {
		OR = {
			province_id = 34 # Ingria
			AND = {
				region = baltic_region
				has_port = yes
			}
		}
		NOT = { owned_by = ROOT }
		any_neighbor_province = {
			owned_by = ROOT
		}
		owner = {
			NOT = { alliance_with = ROOT }
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
		}
	}
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		is_free_or_tributary_trigger = yes
		NOT = { has_country_flag = access_to_baltic }
		NOT = { has_country_modifier = baltic_ambition }
		NOT = {
			owns = 34 # Ingria
		}
		NOT = {
			any_owned_province = {
				region = baltic_region
				has_port = yes
			}
		}
		OR = {
			34 = {
				any_neighbor_province = {
					owned_by = ROOT
				}
				owner = { NOT = { alliance_with = ROOT } }
				owner = { NOT = { is_subject_of = ROOT } }
				owner = { NOT = { overlord_of = ROOT } }
			}
			baltic_region = {
				has_port = yes
				owner = { NOT = { alliance_with = ROOT } }
				owner = { NOT = { is_subject_of = ROOT } }
				owner = { NOT = { overlord_of = ROOT } }
				any_neighbor_province = {
					owned_by = ROOT
				}
			}
		}
		NOT = { last_mission = access_to_the_baltic_sea }
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		any_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 4000
		modifier = {
			factor = 2
			dip = 4
		}
		modifier = {
			factor = 2
			naval_ideas = 3
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			if = {
				limit = {
					NOT = {
						is_permanent_claim = ROOT
					}
				}
				remove_claim = ROOT
			}
		}
	}
	effect = {
		add_navy_tradition = 25
		set_country_flag = access_to_baltic
		add_country_modifier = {
			name = "baltic_ambition"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}

russia_discovers_western_siberia = {
	
	type = country
	
	category = DIP
	
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		OR = {
			num_of_colonists = 1
			has_idea = conquest_of_siberia
		}
		steppes_region = { has_discovered = ROOT }
		NOT = { west_siberia_region = { has_discovered = ROOT } }
		west_siberia_region = { range = ROOT }
		NOT = { has_country_modifier = conquest_of_siberia }
		NOT = { last_mission = russia_discovers_western_siberia }
	}
	abort = {
	}
	success = {
		west_siberia_region = { has_discovered = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			innovativeness_ideas = 4
		}
		modifier = {
			factor = 2
			num_of_explorers = 2
		}
	}
	effect = {
		add_treasury = 100
		add_country_modifier = {
			name = "conquest_of_siberia"
			duration = 3650
		}
	}
}

russian_colony_in_west_siberia = {
	
	type = country
	
	category = DIP
	
	allow = {
		tag = RUS
		NOT = { has_country_modifier = conquest_of_siberia }
		west_siberia_region = {
			has_discovered = ROOT
			is_empty = yes
		}
		OR = {
			num_of_colonists = 1
			has_idea = conquest_of_siberia
		}
		NOT = { west_siberia_region = { country_or_vassal_holds = ROOT } }
		NOT = { last_mission = russian_colony_in_west_siberia }
	}
	abort = {
		NOT = { west_siberia_region = { is_empty = yes } }
		NOT = { west_siberia_region = { country_or_vassal_holds = ROOT } }
	}
	success = {
		west_siberia_region = {
			owned_by = ROOT
			colonysize = 400
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			adm = 4
		}
		modifier = {
			factor = 2
			num_of_explorers = 1
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "conquest_of_siberia"
			duration = 3650
		}
	}
}

russia_discovers_eastern_siberia = {
	
	type = country
	
	category = DIP
	
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		OR = {
			num_of_colonists = 1
			has_idea = conquest_of_siberia
		}
		NOT = { has_country_modifier = conquest_of_siberia }
		west_siberia_region = { has_discovered = ROOT }
		NOT = { east_siberia_region = { has_discovered = ROOT } }
		east_siberia_region = { range = ROOT }
		NOT = { last_mission = russia_discovers_eastern_siberia }
	}
	abort = {
	}
	success = {
		east_siberia_region = { has_discovered = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			innovativeness_ideas = 4
		}
	}
	effect = {
		add_prestige = 5
		add_adm_power = 50
		add_country_modifier = {
			name = "conquest_of_siberia"
			duration = 3650
		}
	}
}

russian_colony_in_east_siberia = {
	
	type = country
	
	category = DIP
	
	allow = {
		tag = RUS
		OR = {
			num_of_colonists = 1
			has_idea = conquest_of_siberia
		}
		NOT = { has_country_modifier = conquest_of_siberia }
		west_siberia_region = { country_or_vassal_holds = ROOT }
		east_siberia_region = {
			has_discovered = ROOT
			is_empty = yes
		}
		NOT = { east_siberia_region = { country_or_vassal_holds = ROOT } }
		NOT = { last_mission = russian_colony_in_east_siberia }
	}
	abort = {
		NOT = { east_siberia_region = { is_empty = yes } }
		NOT = { east_siberia_region = { country_or_vassal_holds = ROOT } }
		NOT = { west_siberia_region = { country_or_vassal_holds = ROOT } }
	}
	success = {
		east_siberia_region = {
			owned_by = ROOT
			colonysize = 400
		}
	}
	chance = {
		factor = 3000
		modifier = {
			factor = 2
			adm = 4
		}
		modifier = {
			factor = 2
			num_of_colonists = 2
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "conquest_of_siberia"
			duration = 3650
		}
	}
}

defend_russia_against_the_mongols_whi = {
	
	type = country
	
	category = MIL
	
	target_provinces = {
		owned_by = WHI
		culture_group = east_slavic
	}
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		NOT = { has_country_modifier = controlling_the_steppes }
		WHI = {
			is_neighbor_of = ROOT
			NOT = { war_with = ROOT }
			NOT = { alliance_with = ROOT }
			NOT = { is_subject_of = ROOT }
			any_owned_province = { culture_group = east_slavic }
		}
		NOT = { last_mission = defend_russia_against_the_mongols_whi }
	}
	abort = {
		NOT = { exists = WHI }
		WHI = {
			NOT = {
				any_owned_province = { culture_group = east_slavic }
			}
		}
	}
	success = {
		NOT = { war_with = WHI }
		WHI = {
			NOT = {
				any_owned_province = { culture_group = east_slavic }
			}
		}
	}
	chance = {
		factor = 5000
		modifier = {
			factor = 2
			is_monarch_leader = yes
		}
		modifier = {
			factor = 2
			mil = 4
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			if = {
				limit = {
					NOT = {
						is_permanent_claim = ROOT
					}
				}
				remove_claim = ROOT
			}
		}
	}
	effect = {
		add_prestige = 5
		add_army_tradition = 5
		add_country_modifier = {
			name = "controlling_the_steppes"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}

defend_russia_against_the_mongols_blu = {
	
	type = country
	
	category = MIL
	
	target_provinces = {
		owned_by = BLU
		culture_group = east_slavic
	}
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		NOT = { has_country_modifier = controlling_the_steppes }
		BLU = {
			is_neighbor_of = ROOT
			NOT = { war_with = ROOT }
			NOT = { alliance_with = ROOT }
			NOT = { is_subject_of = ROOT }
			any_owned_province = { culture_group = east_slavic }
		}
		NOT = { last_mission = defend_russia_against_the_mongols_blu }
	}
	abort = {
		OR = {
			NOT = { exists = BLU }
			BLU = {
				NOT = {
					any_owned_province = { culture_group = east_slavic }
				}
			}
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			if = {
				limit = {
					NOT = {
						is_permanent_claim = ROOT
					}
				}
				remove_claim = ROOT
			}
		}
	}
	success = {
		NOT = { war_with = BLU }
		BLU = {
			NOT = {
				any_owned_province = { culture_group = east_slavic }
			}
		}
	}
	chance = {
		factor = 5000
		modifier = {
			factor = 2
			is_monarch_leader = yes
		}
		modifier = {
			factor = 2
			mil = 4
		}
	}
	effect = {
		add_prestige = 5
		add_army_tradition = 5
		add_country_modifier = {
			name = "controlling_the_steppes"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}

defend_russia_against_the_mongols_gol = {
	
	type = country
	
	category = MIL
	
	target_provinces = {
		owned_by = GOL
		culture_group = east_slavic
	}
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		NOT = { has_country_modifier = controlling_the_steppes }
		GOL = {
			is_neighbor_of = ROOT
			NOT = { war_with = ROOT }
			NOT = { alliance_with = ROOT }
			NOT = { is_subject_of = ROOT }
			any_owned_province = { culture_group = east_slavic }
		}
		NOT = { last_mission = defend_russia_against_the_mongols_gol }
	}
	abort = {
		OR = {
			NOT = { exists = GOL }
			GOL = {
				NOT = {
					any_owned_province = { culture_group = east_slavic }
				}
			}
		}
	}
	success = {
		NOT = { war_with = GOL }
		GOL = {
			NOT = {
				any_owned_province = { culture_group = east_slavic }
			}
		}
	}
	chance = {
		factor = 5000
		modifier = {
			factor = 2
			is_monarch_leader = yes
		}
		modifier = {
			factor = 2
			mil = 4
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			if = {
				limit = {
					NOT = {
						is_permanent_claim = ROOT
					}
				}
				remove_claim = ROOT
			}
		}
	}
	effect = {
		add_prestige = 5
		add_army_tradition = 5
		add_country_modifier = {
			name = "controlling_the_steppes"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}

cross_the_mongol_border = {
	
	type = country
	
	category = MIL
	
	target_provinces = {
		owned_by = WHI
	}
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		mil = 3
		WHI = { is_neighbor_of = ROOT }
		NOT = { has_country_modifier = controlling_the_steppes }
		NOT = { last_mission = cross_the_mongol_border }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = WHI }
		}
	}
	success = {
		NOT = { war_with = WHI }
		any_target_province = { # Too hard to conquest thrm all during one mission
			owned_by = ROOT
		}
	}
	chance = {
		factor = 3000
		modifier = {
			factor = 2
			is_monarch_leader = yes
		}
		modifier = {
			factor = 2
			mil = 4
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			if = {
				limit = {
					NOT = {
						is_permanent_claim = ROOT
					}
				}
				remove_claim = ROOT
			}
		}
	}
	effect = {
		add_country_modifier = {
			name = "controlling_the_steppes"
			duration = 3650
		}
		every_target_province = {
			if = {
				limit = {
					owned_by = ROOT
				}
				add_territorial_core_effect = yes
			}
			if = {
				limit = {
					NOT = {
						is_permanent_claim = ROOT
					}
				}
				remove_claim = ROOT
			}
		}
	}
}

subjugate_novgorod = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces = {
		owned_by = NOV
	}
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		is_free_or_tributary_trigger = yes
		mil = 4
		NOT = { has_country_flag = novogorod_subjugated }
		NOT = { has_country_modifier = military_victory }
		NOV = {
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			NOT = { is_subject_of = ROOT }
			NOT = { alliance_with = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = NOV }
			NOV = { is_subject_of = ROOT }
		}
	}
	success = {
		NOT = { exists = NOV }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 5000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = NOV value = 0 } }
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			if = {
				limit = {
					NOT = {
						is_permanent_claim = ROOT
					}
				}
				remove_claim = ROOT
			}
		}
	}
	effect = {
		set_country_flag = novogorod_subjugated
		add_prestige = 10
		add_country_modifier = {
			name = "military_victory"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}

russia_partitions_poland = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_areas_list = {
		moscow_area
		smolensk_area
		ryazan_area
		briansk_area
		tver_area
		west_dniepr_area
		east_dniepr_area
		red_ruthenia_area
		podolia_volhynia_area
	}
	
	target_provinces = {
		OR = {
			owned_by = POL
			owned_by = PLC
			owner = {
				is_subject_of = POL
			}
			owner = {
				is_subject_of = PLC
			}
		}
	}
	allow = {
		tag = RUS
		is_free_or_tributary_trigger = yes
		mil = 4
		OR = {
			is_neighbor_of = POL
			is_neighbor_of = PLC
		}
		OR = {
			NOT = { is_neighbor_of = LIT }
			LIT = {
				is_subject_of = POL
			}
			LIT = {
				is_subject_of = PLC
			}
		}
		OR = {
			PLC = {
				NOT = { alliance_with = ROOT }
				NOT = { is_subject_of = ROOT }
				NOT = { overlord_of = ROOT }
				NOT = { num_of_cities = ROOT }
				is_free_or_tributary_trigger = yes
				any_owned_province = {
					OR = {
						region = ruthenia_region
						region = russia_region
					}
				}
			}
			AND = {
				NOT = { exists = PLC }
				POL = {
					NOT = { is_subject_of = ROOT }
					NOT = { overlord_of = ROOT }
					NOT = { alliance_with = ROOT }
					NOT = { num_of_cities = ROOT }
					is_free_or_tributary_trigger = yes
					any_owned_province = {
						OR = {
							region = ruthenia_region
							region = russia_region
						}
					}
				}
			}
		}
		NOT = { has_country_modifier = polish_partitions }
		NOT = { has_country_flag = partitioned_poland }
		NOT = { last_mission = russia_partitions_poland }
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 2000
		modifier = {
			factor = 4
			any_owned_province = {
				region = ruthenia_region
			}
		}
		modifier = {
			factor = 2
			russia_region = {
				OR = {
					owned_by = POL
					owned_by = PLC
				}
			}
		}
		modifier = {
			factor = 2
			russia_region = {
				type = all
				owned_by = ROOT
			}
		}
		modifier = {
			factor = 5
			is_year = 1700
		}
		modifier = {
			factor = 2
			exists = POL
			POL = {
				has_any_disaster = yes
			}
		}
		modifier = {
			factor = 2
			exists = PLC
			PLC = {
				has_any_disaster = yes
			}
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			if = {
				limit = {
					NOT = {
						is_permanent_claim = ROOT
					}
				}
				remove_claim = ROOT
			}
		}
	}
	effect = {
		add_prestige = 10
		set_country_flag = partitioned_poland
		add_country_modifier = {
			name = "polish_partitions"
			duration = 5475
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}

russia_partitions_lithuania = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_areas_list = {
		moscow_area
		smolensk_area
		ryazan_area
		briansk_area
		tver_area
		west_dniepr_area
		east_dniepr_area
		red_ruthenia_area
		podolia_volhynia_area
	}
	
	target_provinces = {
		OR = {
			owned_by = LIT
			owner = {
				is_subject_of = LIT
			}
		}
	}
	
	allow = {
		tag = RUS
		is_free_or_tributary_trigger = yes
		mil = 4
		LIT = {
			is_neighbor_of = ROOT
			NOT = { alliance_with = ROOT }
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			NOT = { num_of_cities = ROOT }
			is_free_or_tributary_trigger = yes
			any_owned_province = {
				OR = {
					region = ruthenia_region
					region = russia_region
				}
			}
		}
		NOT = { has_country_modifier = polish_partitions }
		NOT = { has_country_flag = partitioned_poland }
		NOT = { last_mission = russia_partitions_lithuania }
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 2000
		modifier = {
			factor = 4
			any_owned_province = {
				region = ruthenia_region
			}
		}
		modifier = {
			factor = 2
			russia_region = {
				owned_by = LIT
			}
		}
		modifier = {
			factor = 2
			russia_region = {
				type = all
				owned_by = ROOT
			}
		}
		modifier = {
			factor = 5
			is_year = 1700
		}
		modifier = {
			factor = 2
			exists = LIT
			LIT = {
				has_any_disaster = yes
			}
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			if = {
				limit = {
					NOT = {
						is_permanent_claim = ROOT
					}
				}
				remove_claim = ROOT
			}
		}
	}
	effect = {
		set_country_flag = partitioned_poland
		add_country_modifier = {
			name = "death_of_lithuania"
			duration = 5475
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}

subjugate_crimea = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces = {
		owned_by = CRI
	}
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		is_free_or_tributary_trigger = yes
		mil = 3
		NOT = { has_country_modifier = controlling_the_steppes }
		NOT = { last_mission = subjugate_crimea }
		CRI = {
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			NOT = { is_subject_of = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = CRI }
			CRI = { is_subject_of = ROOT }
		}
	}
	success = {
		NOT = { exists = CRI }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 3000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			CRI = {
				NOT = { mil = 0 }
			}
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			if = {
				limit = {
					NOT = {
						is_permanent_claim = ROOT
					}
				}
				remove_claim = ROOT
			}
		}
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = "controlling_the_steppes"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}

subjugate_kazan = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces = {
		owned_by = KAZ
	}
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		is_free_or_tributary_trigger = yes
		mil = 3
		NOT = { has_country_modifier = controlling_the_steppes }
		NOT = { last_mission = subjugate_kazan }
		KAZ = {
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			NOT = { is_subject_of = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = KAZ }
			KAZ = { is_subject_of = ROOT }
		}
	}
	success = {
		NOT = { exists = KAZ }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 3500
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			KAZ = {
				NOT = { mil = 0 }
			}
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			if = {
				limit = {
					NOT = {
						is_permanent_claim = ROOT
					}
				}
				remove_claim = ROOT
			}
		}
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = "controlling_the_steppes"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}

subjugate_the_siberian_khanate = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces = {
		owned_by = SIB
	}
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		is_free_or_tributary_trigger = yes
		mil = 3
		NOT = { has_country_modifier = controlling_siberia }
		NOT = { last_mission = subjugate_the_siberian_khanate }
		SIB = {
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			NOT = { is_subject_of = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = SIB }
			SIB = { is_subject_of = ROOT }
		}
	}
	success = {
		NOT = { exists = SIB }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 3500
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			SIB = {
				NOT = { mil = 0 }
			}
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			if = {
				limit = {
					NOT = {
						is_permanent_claim = ROOT
					}
				}
				remove_claim = ROOT
			}
		}
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = "controlling_siberia"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}

subjugate_astrakhan = {
	
	type = country
	
	category = MIL
	
	
	target_provinces = {
		owned_by = AST
	}
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		is_free_or_tributary_trigger = yes
		mil = 3
		NOT = { has_country_modifier = controlling_the_steppes }
		NOT = { last_mission = subjugate_astrakhan }
		AST = {
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			NOT = { is_subject_of = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = AST }
			AST = { is_subject_of = ROOT }
		}
	}
	success = {
		NOT = { exists = AST }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 3000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			AST = {
				NOT = { mil = 0 }
			}
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			if = {
				limit = {
					NOT = {
						is_permanent_claim = ROOT
					}
				}
				remove_claim = ROOT
			}
		}
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = "controlling_the_steppes"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}

subjugate_great_perm = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces = {
		owned_by = PRM
	}
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		is_free_or_tributary_trigger = yes
		mil = 3
		NOT = { has_country_modifier = controlling_siberia }
		NOT = { last_mission = subjugate_great_perm }
		PRM = {
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			NOT = { is_subject_of = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = PRM }
			PRM = { is_subject_of = ROOT }
		}
	}
	success = {
		NOT = { exists = PRM }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 3000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			PRM = {
				NOT = { mil = 0 }
			}
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			if = {
				limit = {
					NOT = {
						is_permanent_claim = ROOT
					}
				}
				remove_claim = ROOT
			}
		}
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = "controlling_siberia"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}

subjugate_pelym = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces = {
		owned_by = PLM
	}
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		is_free_or_tributary_trigger = yes
		mil = 3
		NOT = { has_country_modifier = controlling_siberia }
		NOT = { last_mission = subjugate_great_perm }
		PLM = {
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			NOT = { is_subject_of = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = PLM }
			PLM = { is_subject_of = ROOT }
		}
	}
	success = {
		NOT = { exists = PLM }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 3000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			PLM = {
				NOT = { mil = 0 }
			}
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			if = {
				limit = {
					NOT = {
						is_permanent_claim = ROOT
					}
				}
				remove_claim = ROOT
			}
		}
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = "controlling_siberia"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}

subjugate_obdora = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces = {
		owned_by = OBD
	}
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		is_free_or_tributary_trigger = yes
		mil = 3
		NOT = { has_country_modifier = controlling_siberia }
		NOT = { last_mission = subjugate_great_perm }
		OBD = {
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			NOT = { is_subject_of = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = OBD }
			OBD = { is_subject_of = ROOT }
		}
	}
	success = {
		NOT = { exists = OBD }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 3000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			OBD = {
				NOT = { mil = 0 }
			}
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			if = {
				limit = {
					NOT = {
						is_permanent_claim = ROOT
					}
				}
				remove_claim = ROOT
			}
		}
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = "controlling_siberia"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}

subjugate_koda = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces = {
		owned_by = KOD
	}
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		is_free_or_tributary_trigger = yes
		mil = 3
		NOT = { has_country_modifier = controlling_siberia }
		NOT = { last_mission = subjugate_great_perm }
		KOD = {
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			NOT = { is_subject_of = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = KOD }
			KOD = { is_subject_of = ROOT }
		}
	}
	success = {
		NOT = { exists = KOD }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 3000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			KOD = {
				NOT = { mil = 0 }
			}
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			if = {
				limit = {
					NOT = {
						is_permanent_claim = ROOT
					}
				}
				remove_claim = ROOT
			}
		}
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = "controlling_siberia"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}

subjugate_selkups = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces = {
		owned_by = SKP
	}
	allow = {
		OR = {
			tag = MOS
			tag = RUS
		}
		is_free_or_tributary_trigger = yes
		mil = 3
		NOT = { has_country_modifier = controlling_siberia }
		NOT = { last_mission = subjugate_great_perm }
		SKP = {
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			NOT = { is_subject_of = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = SKP }
			SKP = { is_subject_of = ROOT }
		}
	}
	success = {
		NOT = { exists = SKP }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 3000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			SKP = {
				NOT = { mil = 0 }
			}
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			if = {
				limit = {
					NOT = {
						is_permanent_claim = ROOT
					}
				}
				remove_claim = ROOT
			}
		}
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = "controlling_siberia"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}
