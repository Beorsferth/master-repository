# Trade Missions




increase_spices_trade = {
	
	type = country
	
	category = MIL
	
	allow = {
		is_at_war = no
		NOT = { has_country_modifier = growing_spice_trade }
		OR = {
			NOT = { has_country_flag = increased_spice_trade }
			had_country_flag = { flag = increased_spice_trade days = 14600 }
		}
		capital_scope = {
			OR = {
				continent = europe
				africa_continent_trigger = yes
			}
		}
		num_of_ports = 3
		num_of_owned_provinces_with = {
			value = 1
			or = {
				trade_goods = pepper
				trade_goods = clove
				trade_goods = nutmeg
				trade_goods = cinnamon
			}
		}
		NOT = {
			num_of_owned_provinces_with = {
				value = 3
				or = {
					trade_goods = pepper
					trade_goods = clove
					trade_goods = nutmeg
					trade_goods = cinnamon
				}
			}
		}
	}
	abort = {
		NOT = { num_of_ports = 1 }
	}
	success = {
		num_of_owned_provinces_with = {
			value = 3
			or = {
				trade_goods = pepper
				trade_goods = clove
				trade_goods = nutmeg
				trade_goods = cinnamon
			}
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 1.15
			full_idea_group = naval_ideas
		}
		modifier = {
			factor = 1.10
			full_idea_group = expansion_ideas
		}
		modifier = {
			factor = 1.05
			full_idea_group = exploration_ideas
		}
	}
	effect = {
		add_prestige = 10
		set_country_flag = increased_spice_trade
		add_country_modifier = {
			name = "growing_spice_trade"
			duration = 3650
		}
	}
}


earn_more_from_trade = {
	
	type = country
	
	category = ADM
	
	allow = {
		is_at_war = no
		trade_income_percentage = 0.40
		num_of_merchants = 3
		NOT = {  trade_income_percentage = 0.55 }
		NOT = { last_mission = earn_more_from_trade }
		NOT = { has_country_flag = earned_more_from_trade }
	}
	success = {
		trade_income_percentage = 0.50
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 1.5
			full_idea_group = expansion_ideas
		}
		modifier = {
			factor = 1.5
			full_idea_group = trade_ideas
		}
		modifier = {
			factor = 0.5
			NOT = { num_of_ports = 1 }
		}
		modifier = {
			factor = 0.5
			government = feudal_monarchy
		}
	}
	effect = {
		set_country_flag = earned_more_from_trade
		add_country_modifier = {
			name = "merchant_society"
			duration = 3650
		}
		define_advisor = { type = trader skill = 2 discount = yes }
		add_mercantilism = 1
	}
}

## privateer_in_carribean = {
#	
#	type = country
#
#	category = DIP
#	
#	allow = {
#		NOT = { government = colonial_government }
#		NOT = { has_country_modifier = privateer_boost }
#		NOT = {
#			any_active_trade_node = {
#				province_id = 487	# Jamaica
#			}
#			any_trade_node = {
#				privateer_power = {
#				country = ROOT
#				share = 1
#				}
#			province_id = 487
#			}
#		}
#		num_of_ports = 3
#		is_year = 1550
#		487 = { trade_range = ROOT }
#		has_idea_group = exploration_ideas
#		carribeans_region = {
#			has_discovered = ROOT
#		}
#		NOT = {
#			carribeans_region = {
#				country_or_vassal_holds = ROOT
#			}
#		}
#	}
#	abort = {
#		carribeans_region = {
#				country_or_vassal_holds = ROOT
#			}
#	}
#	success = {
#		has_privateers = yes
#		any_trade_node = {
#			privateer_power = {
#				country = ROOT
#				share = 5
#			}
#			is_overseas = yes
#			province_id = 487
#		}
#	}
#	chance = {
#		factor = 500
#		modifier = {
#			factor = 2
#			has_idea = privateers
#		}
#		modifier = {
#			factor = 2
#			has_idea = quest_for_the_new_world
#		}
#		modifier = {
#			factor = 2
#			has_idea = sea_hawks
#		}
#		modifier = {
#			factor = 2
#			has_idea = private_shipbuilding_incentives
#		}
#	}
#	effect = {
#		add_prestige = 5
#		add_country_modifier = {
##			name = "privateer_boost"
#			duration = 3650
#		}
#	}
# }