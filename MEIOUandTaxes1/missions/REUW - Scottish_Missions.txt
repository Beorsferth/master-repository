# Scottish Missions

conquer_orkney = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces_list = {
		369
	}
	allow = {
		tag = SCO
		is_free_or_tributary_trigger = yes
		369 = {	NOT = { owned_by = ROOT } }		# Orkney
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		owns = 369 # Orkney
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			adm = 4
		}
	}
	immediate = {
		add_claim = 369 # Orkney
	}
	abort_effect = {
		remove_claim = 369 # Orkney
	}
	effect = {
		add_prestige = 5
		369 = {
			add_territorial_core_effect = yes
		}
		if = {
			limit = {
				369 = { NOT = { is_core = ROOT } }
			}
			369 = {
				add_province_modifier = {
					name = "faster_integration"
					duration = 3650
				}
			}
		}
	}
}


# Auld alliance
scotland_france_relations = {
	
	type = country
	
	category = DIP
	
	allow = {
		tag = SCO
		exists = FRA
		exists = ENG
		NOT = { war_with = FRA }
		NOT = { has_opinion = { who = FRA value = 100 } }
		NOT = { FROM = { is_rival = ROOT } }
		NOT = { ROOT = { is_rival = FROM } }
		NOT = { has_country_modifier = foreign_contacts }
	}
	abort = {
		OR = {
			NOT = { exists = FRA }
			NOT = { exists = ENG }
			war_with = FRA
		}
	}
	success = {
		FRA = { has_opinion = { who = SCO value = 150 } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = FRA value = 0 } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = FRA value = -100 } }
		}
	}
	effect = {
		add_stability_1 = yes
		add_country_modifier = {
			name = "foreign_contacts"
			duration = 3650
		}
	}
}


scottish_war_against_england = {
	
	type = country
	
	category = MIL
	
	allow = {
		tag = SCO
		is_free_or_tributary_trigger = yes
		mil = 4
		exists = ENG
		exists = FRA
		NOT = {
			war_with = ENG
			war_with = FRA
		}
		FRA = { war_with = ENG }
	}
	abort = {
		OR = {
			NOT = { exists = FRA }
			NOT = { exists = ENG }
		}
	}
	success = {
		war_with = ENG
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = ENG value = 0 } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = ENG value = -100 } }
		}
	}
	effect = {
		add_army_tradition = 30
		add_stability_1 = yes
	}
}


scottish_control = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		tag = SCO
		exists = ENG
		is_free_or_tributary_trigger = yes
		is_at_war = no
		NOT = { alliance_with = ENG }
		NOT = { highlands_area = { owned_by = ENG } }
		NOT = { lowlands_area = { owned_by = ENG } }
		OR = {
			AND = {
				NOT = { northwest_area = { owned_by = ROOT } }
				NOT = { northwest_area = { NOT = { owned_by = ENG } } }
			}
			AND = {
				NOT = { northumbria_area = { owned_by = ROOT } }
				NOT = { northumbria_area = { NOT = { owned_by = ENG } } }
			}
		}
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		NOT = { war_with = ENG }
		NOT = { highlands_area = { owned_by = ENG } }
		NOT = { lowlands_area = { owned_by = ENG } }
		northwest_area = { owned_by = ROOT }
		northumbria_area = { owned_by = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			ENG = { is_at_war = yes }
		}
	}
	immediate = {
		northwest_area = {
			add_claim = ROOT
		}
		northumbria_area = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		northwest_area = {
			remove_claim = ROOT
		}
		northumbria_area = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 10
		northwest_area = {
			limit = {
				owned_by = ROOT
			}
			add_territorial_core_effect = yes
		}
		northumbria_area = {
			limit = {
				owned_by = ROOT
			}
			add_territorial_core_effect = yes
		}
	}
}


scottish_defense = {
	
	type = country
	
	category = MIL
	
	allow = {
		tag = SCO
		exists = ENG
		is_free_or_tributary_trigger = yes
		is_at_war = no
		OR = {
			highlands_area = { owned_by = ENG }
			lowlands_area = { owned_by = ENG }
		}
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		NOT = { war_with = ENG }
		highlands_area = { type = all owned_by = ROOT }
		lowlands_area = { type = all owned_by = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			ENG = { is_at_war = yes }
		}
	}
	immediate = {
		if = {
			limit = {
				highlands_area = { owned_by = ENG }
			}
			highlands_area = {
				limit = {
					owned_by = ENG
				}
				add_claim = ROOT
			}
		}
		if = {
			limit = {
				lowlands_area = { owned_by = ENG }
			}
			lowlands_area = {
				limit = {
					owned_by = ENG
				}
				add_claim = ROOT
			}
		}
	}
	abort_effect = {
		if = {
			limit = {
				highlands_area = { owned_by = ENG }
			}
			highlands_area = {
				limit = {
					owned_by = ENG
				}
				remove_claim = ROOT
			}
		}
		if = {
			limit = {
				lowlands_area = { owned_by = ENG }
			}
			lowlands_area = {
				limit = {
					owned_by = ENG
				}
				remove_claim = ROOT
			}
		}
	}
	effect = {
		add_stability_1 = yes
		add_war_exhaustion = -5
	}
}
