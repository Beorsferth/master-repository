# Revolt Missions

defeat_rebels_mission = {
	
	type = country
	
	category = MIL
	
	allow = {
		is_at_war = no
		always = no
		num_of_revolts = 1
		NOT = { stability = 3 }
	}
	abort = {
		is_at_war = yes
	}
	success = {
		NOT = { num_of_revolts = 1 }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 1.10
			revolt_percentage = 0.25
		}
		modifier = {
			factor = 1.20
			revolt_percentage = 0.5
		}
		modifier = {
			factor = 1.50
			revolt_percentage = 0.7
		}
	}
	effect = {
		add_stability_2 = yes
	}
}
