
#######################################
#                                     #
#          whatifnations.txt          #
#                                     #
#######################################
#
#2011-jan-16 FB add ai_importance = 200
#2011-aug-01 FB fixes to denmark_norway_nation
# 2013-aug-26 GG EUIV changes
#
#######################################
#
# List of decisions :
#
# restore_latine_empire
#
########################################

country_decisions = {
	
	indonesian_nation = {
		major = yes
		potential = {
			NOT = { exists = IND }
			OR = {
				culture_group = malay
				culture_group = javan_group
				primary_culture = bugis
				primary_culture = moluccan
				primary_culture = papuan
				primary_culture = sulawesi
			}
			OR = {
				religion_group = muslim
				religion_group = christian
				religion_group = taoic
			}
			NOT = { tag = MPH }
			NOT = { tag = SRV }
			NOT = { tag = MLC }
		}
		allow = {
			is_free_or_tributary_trigger = yes
			is_at_war = no
			OR = {
				check_variable = { which = "Demesne_in_Indonesia" value = 40 }
				check_variable = { which = "Cores_on_Indonesia" value = 30 }
			}
			owns = 2108   # Jakarta
			owns = 2105   # Surubaya
			owns = 2095   # Siak
			owns = 622   # Srivijaya
			owns = 641   # Makassar
		}
		effect = {
			sumatra_region = { limit = { owned_by = ROOT } remove_core = IND add_core = IND }
			sumatra_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = IND }
			java_region = { limit = { owned_by = ROOT } remove_core = IND add_core = IND }
			java_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = IND }
			borneo_region = { limit = { owned_by = ROOT } remove_core = IND add_core = IND }
			borneo_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = IND }
			indonesia_region = { limit = { owned_by = ROOT } remove_core = IND add_core = IND }
			indonesia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = IND }
			papua_region = { limit = { owned_by = ROOT } remove_core = IND add_core = IND }
			papua_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = IND }
			philippines_region = { limit = { owned_by = ROOT } remove_core = IND add_core = IND }
			philippines_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = IND }
			add_prestige = 15
			if = {
				limit = { government = monarchy NOT = { has_country_modifier = title_5 has_country_modifier = title_6 } }
				change_title_5 = yes
			}
			
			change_tag = IND
			add_absolutism = 10
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	srivijaya_nation = {
		major = yes
		potential = {
			NOT = { exists = SRV }
			NOT = { tag = MPH }
			NOT = { tag = IND }
			NOT = { tag = MLC }
			culture_group = malay
			OR = {
				religion = buddhism
				religion = hinduism
			}
		}
		allow = {
			is_free_or_tributary_trigger = yes
			is_at_war = no
			OR = {
				check_variable = { which = "Demesne_in_Indonesia" value = 40 }
				check_variable = { which = "Cores_on_Indonesia" value = 30 }
			}
			owns = 2108   # Jakarta
			owns = 2105   # Surubaya
			owns = 2095   # Siak
			owns = 622   # Srivijaya
			owns = 641   # Makassar
		}
		effect = {
			sumatra_region = { limit = { owned_by = ROOT } remove_core = SRV add_core = SRV }
			sumatra_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = SRV }
			java_region = { limit = { owned_by = ROOT } remove_core = SRV add_core = SRV }
			java_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = SRV }
			borneo_region = { limit = { owned_by = ROOT } remove_core = SRV add_core = SRV }
			borneo_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = SRV }
			indonesia_region = { limit = { owned_by = ROOT } remove_core = SRV add_core = SRV }
			indonesia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = SRV }
			papua_region = { limit = { owned_by = ROOT } remove_core = SRV add_core = SRV }
			papua_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = SRV }
			philippines_region = { limit = { owned_by = ROOT } remove_core = SRV add_core = SRV }
			philippines_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = SRV }
			add_prestige = 15
			if = {
				limit = { government = monarchy NOT = { has_country_modifier = title_5 has_country_modifier = title_6 } }
				change_title_5 = yes
			}
			change_tag = SRV
			add_absolutism = 10
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	mongol_nation = {
		major = yes
		potential = {
			OR = {
				culture_group = altaic
				culture_group = tartar_group
			}
			NOT = { tag = BLU }
			NOT = { tag = WHI }
			is_nomad = yes
			NOT = { exists = GGK }
		}
		allow = {
			is_free_or_tributary_trigger = yes
			is_at_war = no
			OR = {
				check_variable = { which = "Demesne_in_Altaic_Throne" value = 45 }
				check_variable = { which = "Cores_on_Altaic_Throne" value = 30 }
			}
		}
		effect = {
			kazakh_region = { limit = { owned_by = ROOT } remove_core = GGK add_core = GGK }
			kazakh_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = GGK }
			steppes_region = { limit = { owned_by = ROOT } remove_core = GGK add_core = GGK }
			steppes_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = GGK }
			east_siberia_region = { limit = { owned_by = ROOT } remove_core = GGK add_core = GGK }
			east_siberia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = GGK }
			west_siberia_region = { limit = { owned_by = ROOT } remove_core = GGK add_core = GGK }
			west_siberia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = GGK }
			add_prestige = 15
			if = {
				limit = { government = monarchy NOT = { has_country_modifier = title_6 } }
				change_title_6 = yes
			}
			change_tag = GGK
			add_absolutism = 10
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	become_kingdom_of_jerusalem = {
		major = yes
		potential = {
			NOT = { tag = ROM }
			OR = {
				tag = CYP
				tag = KNI
				tag = KAM
				dynasty = "de Lusignan"
				AND = {
					OR = {
						tag = KNP
						tag = NAP
					}
					dynasty = "d'Anjou"
				}
			}
			NOT = { exists = KOJ }
			religion = catholic
			palestine_area = { owned_by = ROOT }
		}
		allow = {
			owns = 379 # Al Quds
			is_at_war = no
			OR = {
				check_variable = { which = "Demesne_in_Holy_Land" value = 8 }
				check_variable = { which = "Cores_on_Holy_Land" value = 5 }
			}
		}
		effect = {
			palestine_area = { limit = { owned_by = ROOT } remove_core = KOJ add_core = KOJ }
			palestine_area = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = KOJ }
			every_country = {
				limit = {
					any_owned_province = { area = palestine_area is_core = PREV }
					NOT = { tag = ROOT }
				}
				add_historical_rival = KOJ
				ROOT = { add_historical_rival = PREV }
			}
			add_prestige = 15
			if = {
				limit = { NOT = { has_country_modifier = title_5 has_country_modifier = title_6 } }
				change_title_5 = yes
			}
			if = {
				limit = { OR = { government = theocratic_government	government = monastic_order_government } }
				if = {
					limit = { NOT = { adm_tech = 35 } }
					change_government = feudal_monarchy
				}
				if = {
					limit = { adm_tech = 35 }
					change_government = absolute_monarchy
				}
			}
			change_tag = KOJ
			add_absolutism = 10
			379 = {
				change_province_name = "Jerusalem"
				rename_capital = "Jerusalem"
			}
			Effect_set_capital = { target=379 }
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = KOJ_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	byzantium_empire = {
		major = yes
		potential = {
			OR = {
				culture_group = turko_byzantine
				culture_group = albanese
			}
			OR = {
				religion = orthodox
				religion = coptic
				religion = hellenic_pagan
			}
			forming_BYZ_trigger = yes
			NOT = { exists = BYZ }
			NOT = { tag = ERG }
			NOT = { tag = PAP }
			NOT = { tag = USA }
			NOT = { tag = BRZ }
			NOT = { tag = MEX }
			NOT = { tag = GRE }
			NOT = { tag = TUR }
			NOT = { tag = OTT }
			NOT = { tag = ROM }
			NOT = { tag = LAT }
		}
		allow = {
			owns = 1402	#Kostantiniyye
			is_free_or_tributary_trigger = yes
			is_at_war = no
			is_core = 1402
			OR = {
				check_variable = { which = "Demesne_in_Constantinople" value = 25 }
				check_variable = { which = "Cores_on_Constantinople" value = 15 }
			}
		}
		effect = {
			greece_region = { limit = { owned_by = ROOT } remove_core = BYZ add_core = BYZ }
			greece_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = BYZ }
			north_anatolia_region = { limit = { owned_by = ROOT } remove_core = BYZ add_core = BYZ }
			north_anatolia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = BYZ }
			south_anatolia_region = { limit = { owned_by = ROOT } remove_core = BYZ add_core = BYZ }
			south_anatolia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = BYZ }
			add_prestige = 20
			change_tag = BYZ
			BYZ = { Effect_set_capital = { target=1402 } }
			if = {
				limit = { NOT = { has_country_modifier = title_6 } }
				change_title_6 = yes
			}
			add_absolutism = 10
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = BYZ_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1.0
		}
		ai_importance = 400
	}
	
#	gods_kingdom = {
#		major = yes
#		potential = {
#			has_country_flag = you_know_this_isnt_supposed_to_be_in_the_mod_yet
#			NOT = {
#				exists = ITA
#				exists = ITE
#			}
#			tag = PAP
#			NOT = { has_country_modifier = kingdom_of_god }
#		}
#		allow = {
#			owns = 2530	#Roma
#			owns = 118	#Lazio
#			OR = {
#				check_variable = { which = "Demesne_in_Italy" value = 30 }
#				AND = {
#					check_variable = { which = "Cores_on_Emilia_Romagna" value = 4 }
#					check_variable = { which = "Cores_on_Tuscany" value = 8 }
#					check_variable = { which = "Cores_on_Lombardy" value = 6 }
#					check_variable = { which = "Cores_on_Sardinia" value = 3 }
#					check_variable = { which = "Cores_on_Liguria_Piedmont" value = 4 }
#					check_variable = { which = "Cores_on_Veneto" value = 6 }
#					check_variable = { which = "Cores_on_Umbria_Marcha" value = 4 }
#				}
#			}
#			num_of_cities = 30
#			is_free_or_tributary_trigger = yes
#			is_at_war = no
#		}
#		effect = {
#			east_italy_region = { limit = { owned_by = ROOT } add_core = PAP }
#			east_italy_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = PAP }
#			west_italy_region = { limit = { owned_by = ROOT } add_core = PAP }
#			west_italy_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = PAP }
#			central_italy_region = { limit = { owned_by = ROOT } add_core = PAP }
#			central_italy_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = PAP }
#			latin_union_effect = yes
#			set_country_flag = kingdom_of_god
#			# add_country_modifier = { name = "kingdom_of_god" duration = -1 }
#			add_prestige = 15
#			add_absolutism = 10
#		}
#		ai_will_do = {
#			factor = 1
#		}
#		ai_importance = 400
#	}
	
	lothar_nation = {
		major = yes
		potential = {
			NOT = { exists = LOT }
			OR = {
				culture_group = low_frankish
				primary_culture = lorrain
				primary_culture = bourguignon
			}
			NOT = { tag = FRA }
			NOT = { tag = NED }
			NOT = { tag = PAP }
			NOT = { tag = ERG }
			NOT = { tag = ROM }
			NOT = { tag = LAT }
		}
		allow = {
			is_free_or_tributary_trigger = yes
			is_at_war = no
			OR = {
				check_variable = { which = "Demesne_in_Lotharingia" value = 20 }
				check_variable = { which = "Cores_on_Lotharingia" value = 15 }
			}
			owns = 92	# Brussel
			owns = 97	# Amsterdam
			owns = 189	# Lorraine
		}
		effect = {
			high_countries_region = { limit = { owned_by = ROOT } remove_core = LOT add_core = LOT }
			high_countries_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = LOT }
			low_countries_region = { limit = { owned_by = ROOT } remove_core = LOT add_core = LOT }
			low_countries_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = LOT }
			belgii_region = { limit = { owned_by = ROOT } remove_core = LOT add_core = LOT }
			belgii_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = LOT }
			add_prestige = 15
			if = {
				limit = { government = monarchy NOT = { has_country_modifier = title_5 has_country_modifier = title_6 } }
				change_title_5 = yes
			}
			change_tag = LOT
			add_absolutism = 10
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = LOT_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	unite_scandinavia = {
		major = yes
		potential = {
			culture_group = nord_germanic
			NOT = { exists = KAL }
			forming_KAL_trigger = yes
		}
		allow = {
			is_free_or_tributary_trigger = yes
			OR = {
				AND = {
					tag = DAN
					NOT = { exists = DEN }
					NOT = { exists = NOR }
					NOT = { exists = SWE }
				}
				AND = {
					tag = DEN
					NOT = { exists = DAN }
					NOT = { exists = NOR }
					NOT = { exists = SWE }
				}
				AND = {
					tag = NOR
					NOT = { exists = DAN }
					NOT = { exists = DEN }
					NOT = { exists = SWE }
				}
				AND = {
					tag = SWE
					NOT = { exists = DAN }
					NOT = { exists = DEN }
					NOT = { exists = NOR }
				}
				AND = {
					culture_group = finnic
					NOT = { exists = SWE }
					NOT = { exists = DAN }
					NOT = { exists = DEN }
					NOT = { exists = NOR }
				}
			}
			OR = {
				check_variable = { which = "Demesne_in_Scandinavia" value = 30 }
				check_variable = { which = "Cores_on_Scandinavia" value = 25 }
			}
			owns = 1		# Uppland
			owns = 12		# Sjaelland
			owns = 16		# Akershus
			is_at_war = no
		}
		effect = {
			scandinavia_region = { limit = { owned_by = ROOT } remove_core = KAL add_core = KAL }
			scandinavia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = KAL }
			danish_region = { limit = { owned_by = ROOT } remove_core = KAL add_core = KAL }
			danish_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = KAL }
			norwegian_region = { limit = { owned_by = ROOT } remove_core = KAL add_core = KAL }
			norwegian_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = KAL }
			finland_region = { limit = { owned_by = ROOT } remove_core = KAL add_core = KAL }
			finland_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = KAL }
			add_prestige = 15
			if = {
				limit = { government = monarchy NOT = { has_country_modifier = title_5 has_country_modifier = title_6 } }
				change_title_5 = yes
			}
			change_tag = KAL
			add_absolutism = 10
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = SCA_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	form_suvarnabhumi = {
		major = yes
		potential = {
			NOT = { exists = SUV }
			num_of_cities = 30
			capital_scope = { province_group = suvarnabhumi_group }
		}
		allow = {
			OR = {
				AND = {
					check_variable = { which = "Demesne_in_Thai_Plain" value = 7 }
					check_variable = { which = "Demesne_in_Burma" value = 7 }
					check_variable = { which = "Demesne_in_Malaya_Peninsula" value = 4 }
				}
				AND = {
					check_variable = { which = "Cores_on_Thai_Plain" value = 7 }
					check_variable = { which = "Cores_on_Burma" value = 7 }
					check_variable = { which = "Cores_on_Malaya_Peninsula" value = 4 }
				}
			}
			is_free_or_tributary_trigger = yes
			owns = 609 # Angkor
			owns = 600 # Ayutthaya
			owns = 586 # Hanthawaddy
			owns = 590 # Martaban
			is_at_war = no
		}
		effect = {
			inland_burma_region = { limit = { owned_by = ROOT } remove_core = SUV add_core = SUV }
			inland_burma_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = SUV }
			coastal_burma_region = { limit = { owned_by = ROOT } remove_core = SUV add_core = SUV }
			coastal_burma_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = SUV }
			lan_xang_region = { limit = { owned_by = ROOT } remove_core = SUV add_core = SUV }
			lan_xang_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = SUV }
			cochinchina_region = { limit = { owned_by = ROOT } remove_core = SUV add_core = SUV }
			cochinchina_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = SUV }
			inland_siam_region = { limit = { owned_by = ROOT } remove_core = SUV add_core = SUV }
			inland_siam_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = SUV }
			coastal_siam_region = { limit = { owned_by = ROOT } remove_core = SUV add_core = SUV }
			coastal_siam_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = SUV }
			malaya_region = { limit = { owned_by = ROOT } remove_core = SUV add_core = SUV }
			malaya_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = SUV }
			sumatra_region = { limit = { owned_by = ROOT } remove_core = SUV add_core = SUV }
			sumatra_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = SUV }
			add_prestige = 15
			if = {
				limit = { government = monarchy NOT = { has_country_modifier = title_5 has_country_modifier = title_6 } }
				change_title_5 = yes
			}
			change_tag = SUV
			add_absolutism = 10
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = SUV_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	form_burma = {
		major = yes
		potential = {
			NOT = { tag = SUV }
			NOT = { exists = BRM }
			primary_culture = burmese
			is_year = 1680
		}
		allow = {
			OR = {
				check_variable = { which = "Demesne_in_Burma" value = 7 }
				check_variable = { which = "Cores_on_Burma" value = 5 }
			}
			owns = 586 # Hanthawaddy
			is_free_or_tributary_trigger = yes
			is_at_war = no
		}
		effect = {
			inland_burma_region = { limit = { owned_by = ROOT } remove_core = BRM add_core = BRM }
			inland_burma_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = BRM }
			coastal_burma_region = { limit = { owned_by = ROOT } remove_core = BRM add_core = BRM }
			coastal_burma_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = BRM }
			add_prestige = 15
			if = {
				limit = { government = monarchy NOT = { has_country_modifier = title_5 has_country_modifier = title_6 } }
				change_title_5 = yes
			}
			change_tag = BRM
			add_absolutism = 10
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = TAU_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	form_dai_viet = {
		major = yes
		potential = {
			NOT = { exists = DAI }
			primary_culture = vietnamese
		}
		allow = {
			OR = {
				and = {
					tag = ANN
					NOT = { exists = TOK }
					NOT = { exists = VUU }
					NOT = { exists = MAC }
				}
				and = {
					tag = TOK
					NOT = { exists = ANN }
					NOT = { exists = VUU }
					NOT = { exists = MAC }
				}
				and = {
					tag = VUU
					NOT = { exists = TOK }
					NOT = { exists = ANN }
					NOT = { exists = MAC }
				}
				and = {
					tag = MAC
					NOT = { exists = TOK }
					NOT = { exists = VUU }
					NOT = { exists = ANN }
				}
			}
			is_free_or_tributary_trigger = yes
			OR = {
				check_variable = { which = "Demesne_in_Viet_Region" value = 9 }
				check_variable = { which = "Cores_on_Viet_Region" value = 5 }
			}
			is_at_war = no
		}
		effect = {
			vietnam_region = { limit = { owned_by = ROOT } remove_core = DAI add_core = DAI }
			vietnam_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = DAI }
			song_hong_area = { limit = { owned_by = ROOT } remove_core = DAI add_core = DAI }
			song_hong_area = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = DAI }
			add_prestige = 15
			if = {
				limit = { government = monarchy NOT = { has_country_modifier = title_5 has_country_modifier = title_6 } }
				change_title_5 = yes
			}
			if = {
				limit = { has_country_modifier = puppet_le_emperor }
				remove_country_modifier = puppet_le_emperor
			}
			change_tag = DAI
			add_absolutism = 10
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = DAI_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	form_lan_xang = {
		major = yes
		potential = {
			NOT = { exists = LXA }
			OR = {
				tag = LUA
				tag = VIE
			}
		}
		allow = {
			is_free_or_tributary_trigger = yes
			OR = {
				and = {
					tag = LUA
					NOT = { exists = VIE }
				}
				and = {
					tag = VIE
					NOT = { exists = LUA }
				}
			}
			OR = {
				check_variable = { which = "Demesne_in_Lan_Xang" value = 9 }
				check_variable = { which = "Cores_on_Lan_Xang" value = 5 }
			}
			is_at_war = no
		}
		effect = {
			lan_xang_region = { limit = { owned_by = ROOT } remove_core = LXA add_core = LXA }
			lan_xang_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = LXA }
			add_prestige = 15
			if = {
				limit = { government = monarchy NOT = { has_country_modifier = title_5 has_country_modifier = title_6 } }
				change_title_5 = yes
			}
			change_tag = LXA
			
			add_absolutism = 10
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = LXA_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	silesia_nation = {
		major = yes
		potential = {
			NOT = { exists = SIL }
			OR = {
				primary_culture = silesian
				primary_culture = german_silesian
			}
			NOT = { tag = PAP }
			NOT = { tag = ERG }
			NOT = { tag = ROM }
			NOT = { tag = LAT }
		}
		allow = {
			is_at_war = no
			OR = {
				AND = {
					is_free_or_tributary_trigger = yes
					custom_trigger_tooltip = {
						num_of_owned_provinces_with = {
							value = 7
							OR = {
								area = lower_silesia_area
								area = upper_silesia_area
							}
						}
						tooltip = silesia_nation_claim_big
					}
				}
				AND = {
					is_subject_other_than_tributary_trigger = yes
					custom_trigger_tooltip = {
						num_of_owned_provinces_with = {
							value = 4
							OR = {
								area = lower_silesia_area
								area = upper_silesia_area
							}
						}
						tooltip = silesia_nation_claim_small
					}
				}
			}
		}
		effect = {
			add_prestige = 20
			lower_silesia_area = { limit = { owned_by = ROOT } remove_core = SIL add_core = SIL }
			lower_silesia_area = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = SIL }
			upper_silesia_area = { limit = { owned_by = ROOT } remove_core = SIL add_core = SIL }
			upper_silesia_area = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = SIL }
			change_tag = SIL
			add_absolutism = 10
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = SIL_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	hansa_nation = {
		major = yes
		potential = {
			NOT = { exists = HSA }
			culture_group = low_germanic
			NOT = { tag = PAP }
			NOT = { tag = ERG }
			NOT = { tag = ROM }
			NOT = { tag = LAT }
			OR = {
				government = merchant_imperial_city
				government = imperial_city
			}
		}
		allow = {
			is_at_war = no
			owns_or_non_sovereign_subject_of = 45
			owns_or_non_sovereign_subject_of = 44
			owns_or_non_sovereign_subject_of = 1357
			OR = {
				tag = FRL
				NOT = {
					exists = FRL
				}
				FRL = {
					vassal_of = PREV
				}
			}
			OR = {
				tag = HAM
				NOT = {
					exists = HAM
				}
				HAM = {
					vassal_of = PREV
				}
			}
			OR = {
				tag = FRB
				NOT = {
					exists = FRB
				}
				FRB = {
					vassal_of = PREV
				}
			}
		}
		effect = {
			add_prestige = 20
			change_tag = HSA
			change_government = merchant_oligarchic_republic
			if = {
				limit = {
					exists = FRL
					NOT = {
						tag = FRL
					}
				}
				inherit = FRL
			}
			if = {
				limit = {
					exists = HAM
					NOT = {
						tag = HAM
					}
				}
				inherit = HAM
			}
			if = {
				limit = {
					exists = FRB
					NOT = {
						tag = FRB
					}
				}
				inherit = FRB
			}
			add_absolutism = 10
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = HSA_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	mossi_nation = {
		major = yes
		potential = {
			NOT = { exists = MSI }
			primary_culture = mossi
		}
		allow = {
			is_at_war = no
			primitives = no
			custom_trigger_tooltip = {
				calc_true_if = {
					all_province = {
						area = middle_volta_area
						owned_by = ROOT
						is_core = ROOT
					}
					amount = 5
				}
				tooltip = mossi_nation_claim
			}
		}
		effect = {
			add_prestige = 20
			change_tag = MSI
			every_province = {
				limit = { culture = mossi }
				remove_core = MSI add_core = MSI
			}
			if = {
				limit = { government = monarchy NOT = { has_country_modifier = title_5 has_country_modifier = title_6 } }
				change_title_5 = yes
			}
			add_absolutism = 10
			#if = {
			#	limit = {
			#		has_custom_ideas = no
			#		NOT = { has_idea_group = mossi_ideas }
			#	}
			#	swap_national_ideas_effect = yes
			#}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}

	haussa_nation = {
		major = yes
		potential = {
			NOT = { exists = HAU }
			primary_culture = haussa
		}
		allow = {
			is_at_war = no
			primitives = no
			custom_trigger_tooltip = {
				calc_true_if = {
					all_province = {
						OR = {
							area = east_hausa_area
							area = sokoto_area
						}
						owned_by = ROOT
						is_core = ROOT
					}
					amount = 10
				}
				tooltip = haussa_nation_claim
			}
		}
		effect = {
			add_prestige = 20
			change_tag = HAU
			every_province = {
				limit = { culture = haussa }
				remove_core = HAU add_core = HAU
			}
			if = {
				limit = { government = monarchy NOT = { has_country_modifier = title_5 has_country_modifier = title_6 } }
				change_title_5 = yes
			}
			add_absolutism = 10
			#if = {
			#	limit = {
			#		has_custom_ideas = no
			#		NOT = { has_idea_group = hausa_ideas }
			#	}
			#	swap_national_ideas_effect = yes
			#}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}

	akaa_nation = {
		major = yes
		potential = {
			NOT = { exists = ASH }
			primary_culture = akaa
		}
		allow = {
			is_at_war = no
			primitives = no
			custom_trigger_tooltip = {
				calc_true_if = {
					all_province = {
						OR = {
							area = fante_area
							area = lower_volta_area
						}
						NOT = { province_id = 1127 } # Dagbon
						NOT = { province_id = 1229 } # Gonja
						owned_by = ROOT
						is_core = ROOT
					}
					amount = 8
				}
				tooltip = akaa_nation_claim
			}
		}
		effect = {
			add_prestige = 20
			change_tag = ASH
			every_province = {
				limit = { culture = akaa }
				remove_core = ASH add_core = ASH
			}
			if = {
				limit = { government = monarchy NOT = { has_country_modifier = title_5 has_country_modifier = title_6 } }
				change_title_5 = yes
			}
			add_absolutism = 10
			#if = {
			#	limit = {
			#		has_custom_ideas = no
			#		NOT = { has_idea_group = ashanti_ideas }
			#	}
			#	swap_national_ideas_effect = yes
			#}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}

	yorumba_nation = {
		major = yes
		potential = {
			NOT = { exists = YRB }
			primary_culture = yorumba
		}
		allow = {
			is_at_war = no
			primitives = no
			custom_trigger_tooltip = {
				calc_true_if = {
					all_province = {
						OR = {
							area = oyo_area
							area = ife_ile_area
						}
						has_port = no
						owned_by = ROOT
						is_core = ROOT
					}
					amount = 8
				}
				tooltip = yorumba_nation_claim
			}
		}
		effect = {
			add_prestige = 20
			change_tag = YRB
			every_province = {
				limit = { culture = yorumba }
				remove_core = YRB add_core = YRB
			}
			if = {
				limit = { government = monarchy NOT = { has_country_modifier = title_5 has_country_modifier = title_6 } }
				change_title_5 = yes
			}
			add_absolutism = 10
			#if = {
			#	limit = {
			#		has_custom_ideas = no
			#		NOT = { has_idea_group = yorumba_ideas }
			#	}
			#	swap_national_ideas_effect = yes
			#}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}

	tuareg_nation = {
		major = yes
		potential = {
			NOT = { exists = AHA }
			primary_culture = tuareg
		}
		allow = {
			is_at_war = no
			primitives = no
			owns = 2316			# Agadez
			is_core = 2316		# Agadez
			custom_trigger_tooltip = {
				num_of_owned_provinces_with = {
					value = 10
					culture = tuareg
				}
				tooltip = tuareg_nation_claim
			}
		}
		effect = {
			add_prestige = 20
			change_tag = AHA
			every_province = {
				limit = { culture = tuareg }
				remove_core = AHA add_core = AHA
			}
			if = {
				limit = { government = monarchy NOT = { has_country_modifier = title_5 has_country_modifier = title_6 } }
				change_title_5 = yes
			}
			add_absolutism = 10
			#if = {
			#	limit = {
			#		has_custom_ideas = no
			#		NOT = { has_idea_group = tuareg_ideas }
			#	}
			#	swap_national_ideas_effect = yes
			#}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}

	imperial_city_to_republic = {
		major = yes
		potential = {
			OR = {
				AND = { tag = AUG NOT = { exists = AUH } }
				AND = { tag = BSL NOT = { exists = SWI } }
				AND = { tag = GNV NOT = { exists = SWI } }
				AND = { tag = FRB NOT = { exists = BRE } }
				AND = { tag = FRL NOT = { exists = HSA } }
				AND = { tag = NUR NOT = { exists = NUS } }
			}
			num_of_cities = 3
		}
		allow = {
			is_at_war = no
			is_free_or_tributary_trigger = yes
			OR = {
				ADM = 4
				advisor = statesman
				num_of_cities = 6
			}
		}
		effect = {
			if = {
				limit = { tag = AUG }
				change_tag = AUH
			}
			if = {
				limit = { tag = BSL }
				change_tag = SWI
			}
			if = {
				limit = { tag = GNV }
				change_tag = SWI
			}
			if = {
				limit = { tag = FRB }
				change_tag = BRE
			}
			if = {
				limit = { tag = FRL }
				change_tag = HSA
			}
			if = {
				limit = { tag = NUR }
				change_tag = NUS
			}
			if = {
				limit = { government = imperial_city }
				change_government = administrative_republic
			}
		}
		ai_will_do = { factor = 1 }
	}
	
	dalmatia_nation = {
		major = no
		potential = {
			NOT = { exists = DLM }
			NOT = { tag = ILL }
			NOT = { tag = CRO }
			NOT = { tag = BOS }
			NOT = { tag = PAP }
			NOT = { tag = ERG }
			NOT = { tag = ROM }
			NOT = { tag = LAT }
			OR = {
				primary_culture = croatian
				primary_culture = dalmatian
			}
			is_colonial_nation = no
		}
		allow = {
			is_free_or_tributary_trigger = yes
			owns = 136 # Split
			owns = 137 # Dubrovnik
			owns = 2571 # Zadar
			owns = 2388 # Sibenik
			is_at_war = no
		}
		effect = {
			east_adriatic_coast_area = { limit = { owned_by = ROOT } remove_core = DLM add_core = DLM }
			east_adriatic_coast_area = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = DLM }
			add_prestige = 20
			change_tag = DLM
			add_absolutism = 10
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	resurrect_armenian_kingdom = {
		major = yes
		potential = {
			culture_group = armenian
			OR = {
				religion = orthodox
				religion = coptic
				religion = chaldean
			}
			NOT = { exists = ARM }
			NOT = { tag = ERG }
			NOT = { tag = PAP }
			NOT = { tag = USA }
			NOT = { tag = BRZ }
			NOT = { tag = MEX }
			NOT = { tag = GRE }
			NOT = { tag = TUR }
			NOT = { tag = OTT }
			NOT = { tag = ROM }
			NOT = { tag = BYZ }
			NOT = { tag = LAT }
		}
		allow = {
			owns = 1445	#Vaspurakan
			is_free_or_tributary_trigger = yes
			is_at_war = no
			is_core = 1445
			OR = {
				check_variable = { which = "Demesne_in_Armenia" value = 20 }
				check_variable = { which = "Cores_on_Armenia" value = 15 }
			}
		}
		effect = {
			cilicia_area = { limit = { owned_by = ROOT } remove_core = ARM add_core = ARM }
			cilicia_area = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ARM }
			dulkadir_area = { limit = { owned_by = ROOT } remove_core = ARM add_core = ARM }
			dulkadir_area = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ARM }
			erzurum_area = { limit = { owned_by = ROOT } remove_core = ARM add_core = ARM }
			erzurum_area = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ARM }
			north_kurdistan_area = { limit = { owned_by = ROOT } remove_core = ARM add_core = ARM }
			north_kurdistan_area = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ARM }
			armenia_region = { limit = { owned_by = ROOT } remove_core = ARM add_core = ARM }
			armenia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ARM }
			add_prestige = 20
			change_tag = ARM
			ARM = { Effect_set_capital = { target=1445 } }
			if = {
				limit = { NOT = { has_country_modifier = title_5 } }
				change_title_5 = yes
			}
			add_absolutism = 10
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = ARM_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1.0
		}
		ai_importance = 400
	}
	
}
