country_decisions = {
	settle_in_yakutia = {
		potential = {
			NOT = { has_country_flag = settled_in_yakutia }
			tag = SAK
			government = siberian_native_council
		}
		allow = {
			owns = 2157 # Evenkil
		}
		effect = {
			2157 = {
				# add_base_tax = 1
			}
			4194 = {
				change_culture = yakut
				add_core = SAK
				cede_province = SAK
			}
			change_government = tribal_confederation
			set_country_flag = settled_in_yakutia
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	return_to_the_steppe = {
		potential = {
			NOT = { has_country_flag = settled_in_yakutia }
			tag = SAK
			government = siberian_native_council
		}
		allow = {
			any_owned_province = {
				has_terrain	 = steppe
			}
		}
		effect = {
			change_government = tribal_nomads_steppe
			swap_free_idea_group = yes
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	#unite_selkups = {
	#	potential = {
	#		NOT = { has_country_flag = united_selkups }
	#		tag = SKP
	#		government = siberian_native_council
	#	}
	#	allow = {
	#		owns = 2157 # Evenkil
	#	}
	#	effect = {
	#		capital_scope = {
	#			every_neighbor_province = {
	#				limit = {
	#					culture = selkup
	#				}
	#				add_core = SKP
	#				cede_province = SKP
	#			}
	#		}
	#		change_government = tribal_confederation
	#		set_country_flag = united_selkups
	#	}
	#	ai_will_do = {
	#		factor = 1
	#	}
	#}
	
	#unite_obdora = {
	#	potential = {
	#		NOT = { has_country_flag = united_obdora }
	#		tag = OBD
	#		government = siberian_native_council
	#	}
	#	allow = {
	#		owns = 2157 # Evenkil
	#	}
	#	effect = {
	#		capital_scope = {
	#			every_neighbor_province = {
	#				limit = {
	#					culture = khanty
	#					is_city = no
	#				}
	#				add_core = OBD
	#				cede_province = OBD
	#			}
	#		}
	#		change_government = tribal_confederation
	#		set_country_flag = united_obdora
	#	}
	#	ai_will_do = {
	#		factor = 1
	#	}
	#}
	
	#unite_koda = {
	#	potential = {
	#		NOT = { has_country_flag = united_koda }
	#		tag = KOD
	#		government = siberian_native_council
	#	}
	#	allow = {
	#		owns = 2157 # Evenkil
	#	}
	#	effect = {
	#		capital_scope = {
	#			every_neighbor_province = {
	#				limit = {
	#					culture = khanty
	#					is_city = no
	#				}
	#				add_core = KOD
	#				cede_province = KOD
	#			}
	#		}
	#		change_government = tribal_confederation
	#		set_country_flag = united_koda
	#	}
	#	ai_will_do = {
	#		factor = 1
	#	}
	#}
	
	#unite_mansi = {
	#	potential = {
	#		NOT = { has_country_flag = united_mansi }
	#		tag = PLM
	#		government = siberian_native_council
	#	}
	#	allow = {
	#		owns = 2157 # Evenkil
	#	}
	#	effect = {
	#		capital_scope = {
	#			every_neighbor_province = {
	#				limit = {
	#					culture = mansi
	#					is_city = no
	#				}
	#				add_core = PLM
	#				cede_province = PLM
	#			}
	#		}
	#		change_government = tribal_confederation
	#		set_country_flag = united_mansi
	#	}
	#	ai_will_do = {
	#		factor = 1
	#	}
	#}
	
}