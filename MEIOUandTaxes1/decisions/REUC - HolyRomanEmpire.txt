# D - HolyRomanEmpire decisions

country_decisions = {
	
	#### German Ostsiedlung ####
	
	promote_ostsiedlung = {
		potential = {
			OR = {
				technology_group = western
				technology_group = eastern
			}
			OR = {
				culture_group = low_germanic
				culture_group = high_germanic
				culture_group = west_slavic
			}
			is_year = 1350
			NOT = { is_year = 1425 }
			any_owned_province = {
				is_core = ROOT
				province_group = ostsiedlung_group
				OR = {
					culture = kashubian
					culture = old_prussian
					culture = latvian
					culture = polabian
					culture = polish
					culture = sorbs
					culture = silesian
					culture = czech
					culture = moravian
					culture = lithuanian
					culture = estonian
					culture = slovenian
				}
				has_province_flag = german_ostsiedlung
			}
			NOT = { has_ruler_modifier = ostsiedlung_promoted_western }
			NOT = { has_ruler_modifier = ostsiedlung_promoted_eastern }
			#NOT = { primary_culture = polish }
		}
		allow = {
			dip_power = 100
		}
		
		effect = {
			if = {
				limit = {
					technology_group = western
				}
				add_ruler_modifier = { name = ostsiedlung_promoted_western }
			}
			if = {
				limit = {
					technology_group = eastern
				}
				add_ruler_modifier = { name = ostsiedlung_promoted_eastern }
			}
			every_owned_province = {
				limit = {
					has_province_flag = german_ostsiedlung
				}
				add_unrest = 5
			}
			add_dip_power = -100
		}
		
		ai_will_do = {
			factor = 100
			modifier = {
				factor = 0
				culture_group = west_slavic
			}
		}
	}
	
	
	
	#### transfer electorship ####
	
	transfer_electorship = {
		potential = {
			is_elector = no
			capital_scope = {
				is_part_of_hre = yes
			}
			any_subject_country = {
				is_elector = yes
				is_lesser_in_union = yes
				capital_scope = {
					is_part_of_hre = yes
				}
			}
		}
		allow = {
			emperor = { has_opinion = { who = ROOT value = 100 } }
			dip_power = 100
		}
		effect = {
			add_dip_power = -100
			random_country = {
				limit = {
					is_elector = yes
					junior_union_with = ROOT
					capital_scope = {
						is_part_of_hre = yes
					}
				}
				country_event = { id = holyromanempire.4 days = 1 }
			}
		}
		
		ai_will_do = {
			factor = 100
		}
	}
	
}
