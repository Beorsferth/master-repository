eventbuild_clear_all = { #Clears all flags and variables pertaining to the eventbuild system, country scope only
	clr_country_flag = eventbuild_see_autonomy
	clr_country_flag = eventbuild_greater_nobles
	clr_country_flag = eventbuild_lesser_nobles
	clr_country_flag = eventbuild_burghers
	
	clr_country_flag = eventbuild_start
	clr_country_flag = eventbuild_end
	clr_country_flag = eventbuild_built
	clr_country_flag = eventbuild_allow_road_network
	clr_country_flag = eventbuild_allow_paved_road_network
	clr_country_flag = eventbuild_allow_highway_network
	clr_country_flag = eventbuild_allow_road_and_rail_network
	clr_country_flag = eventbuild_allow_mines_1
	clr_country_flag = eventbuild_allow_mines_2
	clr_country_flag = eventbuild_allow_mines_3
	clr_country_flag = eventbuild_allow_mines_4
	clr_country_flag = eventbuild_allow_mines_5
	clr_country_flag = eventbuild_allow_rural_infrastructure_1
	clr_country_flag = eventbuild_allow_rural_infrastructure_2
	clr_country_flag = eventbuild_allow_rural_infrastructure_3
	clr_country_flag = eventbuild_allow_rural_infrastructure_4
	clr_country_flag = eventbuild_allow_rural_infrastructure_5
	clr_country_flag = eventbuild_allow_rural_infrastructure_6
	clr_country_flag = eventbuild_allow_rural_infrastructure_7
	clr_country_flag = eventbuild_allow_temple
	clr_country_flag = eventbuild_allow_great_temple
	clr_country_flag = eventbuild_allow_small_university
	clr_country_flag = eventbuild_allow_medium_university
	clr_country_flag = eventbuild_allow_big_university
	clr_country_flag = eventbuild_allow_marketplace
	clr_country_flag = eventbuild_allow_merchant_guild
	clr_country_flag = eventbuild_allow_trade_depot
	clr_country_flag = eventbuild_allow_customs_house
	clr_country_flag = eventbuild_allow_bank
	clr_country_flag = eventbuild_allow_stock_exchange
	clr_country_flag = eventbuild_allow_warehouse
	clr_country_flag = eventbuild_allow_warehouse_district
	clr_country_flag = eventbuild_allow_workshop
	clr_country_flag = eventbuild_allow_corporation_guild
	clr_country_flag = eventbuild_allow_manufactory
	clr_country_flag = eventbuild_allow_factory
	clr_country_flag = eventbuild_allow_steam_powered_factory
	clr_country_flag = eventbuild_allow_fort_14th
	clr_country_flag = eventbuild_allow_fort_15th
	clr_country_flag = eventbuild_allow_fort_16th
	clr_country_flag = eventbuild_allow_fort_17th
	clr_country_flag = eventbuild_allow_fort_18th
	clr_country_flag = eventbuild_allow_town_hall
	clr_country_flag = eventbuild_allow_urban_infrastructure_1
	clr_country_flag = eventbuild_allow_urban_infrastructure_2
	clr_country_flag = eventbuild_allow_urban_infrastructure_3
	clr_country_flag = eventbuild_allow_urban_infrastructure_4
	clr_country_flag = eventbuild_allow_urban_infrastructure_5
	clr_country_flag = eventbuild_allow_harbour_infrastructure_1
	clr_country_flag = eventbuild_allow_harbour_infrastructure_2
	clr_country_flag = eventbuild_allow_harbour_infrastructure_3
	clr_country_flag = eventbuild_allow_harbour_infrastructure_4
	clr_country_flag = eventbuild_allow_harbour_infrastructure_5
	clr_country_flag = eventbuild_allow_military_harbour_1
	clr_country_flag = eventbuild_allow_military_harbour_2
	clr_country_flag = eventbuild_allow_military_harbour_3
	clr_country_flag = eventbuild_allow_military_harbour_4
	clr_country_flag = eventbuild_allow_art_corporation
	clr_country_flag = eventbuild_allow_fine_arts_academy
	clr_country_flag = eventbuild_allow_opera
	clr_country_flag = eventbuild_allow_trading_post
	clr_country_flag = eventbuild_allow_trading_company
	#clr_country_flag = eventbuild_allow_local_parliament
	clr_country_flag = eventbuild_allow_regional_capital
	clr_country_flag = eventbuild_allow_bureaucracy_1
	clr_country_flag = eventbuild_allow_bureaucracy_2
	clr_country_flag = eventbuild_allow_bureaucracy_3
	clr_country_flag = eventbuild_allow_bureaucracy_4
	clr_country_flag = eventbuild_allow_bureaucracy_5
	set_variable = { which = eventbuild_cash					value = 0 }
	set_variable = { which = eventbuild_minimum_buildings		value = 0 }
	set_variable = { which = eventbuild_cost_mult				value = 0 }
	set_variable = { which = eventbuild_surplusToRural_ratio	value = 0 }
	set_variable = { which = eventbuild_surplusToUrban_ratio	value = 0 }
	set_variable = { which = eventbuild_rural_softceil			value = 0 }
	set_variable = { which = eventbuild_rural_hardceil			value = 0 }
	set_variable = { which = eventbuild_rural_softfloor			value = 0 }
	set_variable = { which = eventbuild_rural_hardfloor			value = 0 }
	set_variable = { which = eventbuild_urban_softceil			value = 0 }
	set_variable = { which = eventbuild_urban_hardceil			value = 0 }
	set_variable = { which = eventbuild_urban_softfloor			value = 0 }
	set_variable = { which = eventbuild_urban_hardfloor			value = 0 }
	set_variable = { which = eventbuild_surplusToRural_actual	value = 0 }
	set_variable = { which = eventbuild_surplusToUrban_actual	value = 0 }
	set_variable = { which = eventbuild_surplusToRural_split	value = 0 }
	set_variable = { which = eventbuild_surplusToUrban_split	value = 0 }
	set_variable = { which = eventbuild_rural_fail_adjust		value = 0 }
	set_variable = { which = eventbuild_urban_fail_adjust		value = 0 }
	set_variable = { which = eventbuild_tierlimit				value = 0 }
	set_variable = { which = num_of_cities_var					value = 0 }
	every_owned_province = {
		clr_province_flag = eventbuild_block
		clr_province_flag = eventbuild_built_here
		set_variable = { which = eventbuild_cash					value = 0 }
		set_variable = { which = eventbuild_minimum_buildings		value = 0 }
		set_variable = { which = eventbuild_cost_mult				value = 0 }
		set_variable = { which = eventbuild_rural_softceil			value = 0 }
		set_variable = { which = eventbuild_rural_hardceil			value = 0 }
		set_variable = { which = eventbuild_rural_softfloor			value = 0 }
		set_variable = { which = eventbuild_rural_hardfloor			value = 0 }
		set_variable = { which = eventbuild_urban_softceil			value = 0 }
		set_variable = { which = eventbuild_urban_hardceil			value = 0 }
		set_variable = { which = eventbuild_urban_softfloor			value = 0 }
		set_variable = { which = eventbuild_urban_hardfloor			value = 0 }
		set_variable = { which = eventbuild_rural_fail_adjust		value = 0 }
		set_variable = { which = eventbuild_urban_fail_adjust		value = 0 }
		set_variable = { which = building_cost						value = 0 }
		set_variable = { which = eventbuild_surplusToRural_split	value = 0 }
		set_variable = { which = eventbuild_surplusToUrban_split	value = 0 }
		set_variable = { which = eventbuild_tierlimit				value = 0 }
	}
}

eventbuild_renew = { #Restores all eventbuild variables to their default values, country scope only
	set_variable = { which = eventbuild_cash					value = -1 }
	set_variable = { which = eventbuild_minimum_buildings		value = -1 }
	set_variable = { which = eventbuild_cost_mult				value = 1.0 }
	set_variable = { which = eventbuild_surplusToRural_ratio	value = 0.5 }
	set_variable = { which = eventbuild_surplusToUrban_ratio	value = 0.5 }
	set_variable = { which = eventbuild_rural_softceil			value = 1000 }
	set_variable = { which = eventbuild_rural_hardceil			value = 1000 }
	set_variable = { which = eventbuild_rural_softfloor			value = -1 }
	set_variable = { which = eventbuild_rural_hardfloor			value = -1 }
	set_variable = { which = eventbuild_urban_softceil			value = 1000 }
	set_variable = { which = eventbuild_urban_hardceil			value = 1000 }
	set_variable = { which = eventbuild_urban_softfloor			value = -1 }
	set_variable = { which = eventbuild_urban_hardfloor			value = -1 }
	set_variable = { which = eventbuild_rural_fail_adjust		value = 1000 }
	set_variable = { which = eventbuild_urban_fail_adjust		value = 1000 }
	set_variable = { which = eventbuild_tierlimit				value = 10 }
}

eventbuild_allow_all = { #Sets eventbuild to allow all normal buildings, INCLUDING forts, country scope only. (No fort version below)
	set_country_flag = eventbuild_allow_road_network
	set_country_flag = eventbuild_allow_paved_road_network
	set_country_flag = eventbuild_allow_highway_network
	set_country_flag = eventbuild_allow_road_and_rail_network
	set_country_flag = eventbuild_allow_mines_1
	set_country_flag = eventbuild_allow_mines_2
	set_country_flag = eventbuild_allow_mines_3
	set_country_flag = eventbuild_allow_mines_4
	set_country_flag = eventbuild_allow_mines_5
	set_country_flag = eventbuild_allow_rural_infrastructure_1
	set_country_flag = eventbuild_allow_rural_infrastructure_2
	set_country_flag = eventbuild_allow_rural_infrastructure_3
	set_country_flag = eventbuild_allow_rural_infrastructure_4
	set_country_flag = eventbuild_allow_rural_infrastructure_5
	set_country_flag = eventbuild_allow_rural_infrastructure_6
	set_country_flag = eventbuild_allow_rural_infrastructure_7
	set_country_flag = eventbuild_allow_temple
	set_country_flag = eventbuild_allow_great_temple
	set_country_flag = eventbuild_allow_small_university
	set_country_flag = eventbuild_allow_medium_university
	set_country_flag = eventbuild_allow_big_university
	set_country_flag = eventbuild_allow_marketplace
	set_country_flag = eventbuild_allow_merchant_guild
	set_country_flag = eventbuild_allow_trade_depot
	set_country_flag = eventbuild_allow_customs_house
	set_country_flag = eventbuild_allow_warehouse
	set_country_flag = eventbuild_allow_warehouse_district
	set_country_flag = eventbuild_allow_workshop
	set_country_flag = eventbuild_allow_corporation_guild
	set_country_flag = eventbuild_allow_manufactory
	set_country_flag = eventbuild_allow_factory
	set_country_flag = eventbuild_allow_steam_powered_factory
	set_country_flag = eventbuild_allow_fort_14th
	set_country_flag = eventbuild_allow_fort_15th
	set_country_flag = eventbuild_allow_fort_16th
	set_country_flag = eventbuild_allow_fort_17th
	set_country_flag = eventbuild_allow_fort_18th
	set_country_flag = eventbuild_allow_town_hall
	set_country_flag = eventbuild_allow_urban_infrastructure_1
	set_country_flag = eventbuild_allow_urban_infrastructure_2
	set_country_flag = eventbuild_allow_urban_infrastructure_3
	set_country_flag = eventbuild_allow_urban_infrastructure_4
	set_country_flag = eventbuild_allow_urban_infrastructure_5
	set_country_flag = eventbuild_allow_harbour_infrastructure_1
	set_country_flag = eventbuild_allow_harbour_infrastructure_2
	set_country_flag = eventbuild_allow_harbour_infrastructure_3
	set_country_flag = eventbuild_allow_harbour_infrastructure_4
	set_country_flag = eventbuild_allow_harbour_infrastructure_5
	set_country_flag = eventbuild_allow_military_harbour_1
	set_country_flag = eventbuild_allow_military_harbour_2
	set_country_flag = eventbuild_allow_military_harbour_3
	set_country_flag = eventbuild_allow_military_harbour_4
}

eventbuild_allow_all_no_forts = { #Sets eventbuild to allow all normal buildings, except forts, country scope only
	set_country_flag = eventbuild_allow_road_network
	set_country_flag = eventbuild_allow_paved_road_network
	set_country_flag = eventbuild_allow_highway_network
	set_country_flag = eventbuild_allow_road_and_rail_network
	set_country_flag = eventbuild_allow_mines_1
	set_country_flag = eventbuild_allow_mines_2
	set_country_flag = eventbuild_allow_mines_3
	set_country_flag = eventbuild_allow_mines_4
	set_country_flag = eventbuild_allow_mines_5
	set_country_flag = eventbuild_allow_rural_infrastructure_1
	set_country_flag = eventbuild_allow_rural_infrastructure_2
	set_country_flag = eventbuild_allow_rural_infrastructure_3
	set_country_flag = eventbuild_allow_rural_infrastructure_4
	set_country_flag = eventbuild_allow_rural_infrastructure_5
	set_country_flag = eventbuild_allow_rural_infrastructure_6
	set_country_flag = eventbuild_allow_rural_infrastructure_7
	set_country_flag = eventbuild_allow_temple
	set_country_flag = eventbuild_allow_great_temple
	set_country_flag = eventbuild_allow_small_university
	set_country_flag = eventbuild_allow_medium_university
	set_country_flag = eventbuild_allow_big_university
	set_country_flag = eventbuild_allow_marketplace
	set_country_flag = eventbuild_allow_merchant_guild
	set_country_flag = eventbuild_allow_trade_depot
	set_country_flag = eventbuild_allow_customs_house
	set_country_flag = eventbuild_allow_warehouse
	set_country_flag = eventbuild_allow_warehouse_district
	set_country_flag = eventbuild_allow_workshop
	set_country_flag = eventbuild_allow_corporation_guild
	set_country_flag = eventbuild_allow_manufactory
	set_country_flag = eventbuild_allow_factory
	set_country_flag = eventbuild_allow_steam_powered_factory
	set_country_flag = eventbuild_allow_town_hall
	set_country_flag = eventbuild_allow_urban_infrastructure_1
	set_country_flag = eventbuild_allow_urban_infrastructure_2
	set_country_flag = eventbuild_allow_urban_infrastructure_3
	set_country_flag = eventbuild_allow_urban_infrastructure_4
	set_country_flag = eventbuild_allow_urban_infrastructure_5
	set_country_flag = eventbuild_allow_harbour_infrastructure_1
	set_country_flag = eventbuild_allow_harbour_infrastructure_2
	set_country_flag = eventbuild_allow_harbour_infrastructure_3
	set_country_flag = eventbuild_allow_harbour_infrastructure_4
	set_country_flag = eventbuild_allow_harbour_infrastructure_5
	set_country_flag = eventbuild_allow_military_harbour_1
	set_country_flag = eventbuild_allow_military_harbour_2
	set_country_flag = eventbuild_allow_military_harbour_3
	set_country_flag = eventbuild_allow_military_harbour_4
}

eventbuild_allow_rural = { #Sets eventbuild to allow all buildings in rural_buildings.txt, country scope only
	set_country_flag = eventbuild_allow_mines_1
	set_country_flag = eventbuild_allow_mines_2
	set_country_flag = eventbuild_allow_mines_3
	set_country_flag = eventbuild_allow_mines_4
	set_country_flag = eventbuild_allow_mines_5
	set_country_flag = eventbuild_allow_rural_infrastructure_1
	set_country_flag = eventbuild_allow_rural_infrastructure_2
	set_country_flag = eventbuild_allow_rural_infrastructure_3
	set_country_flag = eventbuild_allow_rural_infrastructure_4
	set_country_flag = eventbuild_allow_rural_infrastructure_5
	set_country_flag = eventbuild_allow_rural_infrastructure_6
	set_country_flag = eventbuild_allow_rural_infrastructure_7
}

eventbuild_allow_rural_mines = { #Sets eventbuild to allow all mine buildings, country scope only
	set_country_flag = eventbuild_allow_mines_1
	set_country_flag = eventbuild_allow_mines_2
	set_country_flag = eventbuild_allow_mines_3
	set_country_flag = eventbuild_allow_mines_4
	set_country_flag = eventbuild_allow_mines_5
}

eventbuild_allow_rural_rural_infrastructure = { #Sets eventbuild to allow all farm estate buildings, country scope only
	set_country_flag = eventbuild_allow_rural_infrastructure_1
	set_country_flag = eventbuild_allow_rural_infrastructure_2
	set_country_flag = eventbuild_allow_rural_infrastructure_3
	set_country_flag = eventbuild_allow_rural_infrastructure_4
	set_country_flag = eventbuild_allow_rural_infrastructure_5
	set_country_flag = eventbuild_allow_rural_infrastructure_6
	set_country_flag = eventbuild_allow_rural_infrastructure_7
}

eventbuild_allow_infrastructure = { #Sets eventbuild to allow all buildings in infrastructure_buildings.txt, country scope only
	set_country_flag = eventbuild_allow_town_hall
	set_country_flag = eventbuild_allow_urban_infrastructure_1
	set_country_flag = eventbuild_allow_urban_infrastructure_2
	set_country_flag = eventbuild_allow_urban_infrastructure_3
	set_country_flag = eventbuild_allow_urban_infrastructure_4
	set_country_flag = eventbuild_allow_urban_infrastructure_5
}

eventbuild_allow_naval = { #Sets eventbuild to allow all buildings in naval_buildings.txt, country scope only
	set_country_flag = eventbuild_allow_harbour_infrastructure_1
	set_country_flag = eventbuild_allow_harbour_infrastructure_2
	set_country_flag = eventbuild_allow_harbour_infrastructure_3
	set_country_flag = eventbuild_allow_harbour_infrastructure_4
	set_country_flag = eventbuild_allow_harbour_infrastructure_5
	set_country_flag = eventbuild_allow_military_harbour_1
	set_country_flag = eventbuild_allow_military_harbour_2
	set_country_flag = eventbuild_allow_military_harbour_3
	set_country_flag = eventbuild_allow_military_harbour_4
}

eventbuild_allow_naval_economic = { #Sets eventbuild to allow all harbour infrastructure buildings, country scope only
	set_country_flag = eventbuild_allow_harbour_infrastructure_1
	set_country_flag = eventbuild_allow_harbour_infrastructure_2
	set_country_flag = eventbuild_allow_harbour_infrastructure_3
	set_country_flag = eventbuild_allow_harbour_infrastructure_4
	set_country_flag = eventbuild_allow_harbour_infrastructure_5
}

eventbuild_allow_naval_military = { #Sets eventbuild to allow all military harbour buildings, country scope only
	set_country_flag = eventbuild_allow_military_harbour_1
	set_country_flag = eventbuild_allow_military_harbour_2
	set_country_flag = eventbuild_allow_military_harbour_3
	set_country_flag = eventbuild_allow_military_harbour_4
}

eventbuild_allow_transportation = { #Sets eventbuild to allow all buildings in transportation_buildings.txt, country scope only
	set_country_flag = eventbuild_allow_road_network
	set_country_flag = eventbuild_allow_paved_road_network
	set_country_flag = eventbuild_allow_highway_network
	set_country_flag = eventbuild_allow_road_and_rail_network
}

eventbuild_allow_urban = { #Sets eventbuild to allow all buildings in urban_buildings.txt, country scope only
	set_country_flag = eventbuild_allow_temple
	set_country_flag = eventbuild_allow_great_temple
	set_country_flag = eventbuild_allow_small_university
	set_country_flag = eventbuild_allow_medium_university
	set_country_flag = eventbuild_allow_big_university
	set_country_flag = eventbuild_allow_marketplace
	set_country_flag = eventbuild_allow_merchant_guild
	set_country_flag = eventbuild_allow_trade_depot
	set_country_flag = eventbuild_allow_customs_house
	set_country_flag = eventbuild_allow_warehouse
	set_country_flag = eventbuild_allow_warehouse_district
	set_country_flag = eventbuild_allow_workshop
	set_country_flag = eventbuild_allow_corporation_guild
	set_country_flag = eventbuild_allow_manufactory
	set_country_flag = eventbuild_allow_factory
	set_country_flag = eventbuild_allow_steam_powered_factory
}

eventbuild_allow_urban_religious = { #Sets eventbuild to allow all religious buildings, country scope only
	set_country_flag = eventbuild_allow_temple
	set_country_flag = eventbuild_allow_great_temple
}

eventbuild_allow_urban_education = { #Sets eventbuild to allow all university buildings, country scope only
	set_country_flag = eventbuild_allow_small_university
	set_country_flag = eventbuild_allow_medium_university
	set_country_flag = eventbuild_allow_big_university
}

eventbuild_allow_urban_trade = { #Sets eventbuild to allow all trade buildings, country scope only
	set_country_flag = eventbuild_allow_marketplace
	set_country_flag = eventbuild_allow_merchant_guild
	set_country_flag = eventbuild_allow_trade_depot
	set_country_flag = eventbuild_allow_customs_house
}
eventbuild_allow_urban_warehouse = { #Sets eventbuild to allow all warehouse buildings, country scope only
	set_country_flag = eventbuild_allow_warehouse
	set_country_flag = eventbuild_allow_warehouse_district
}

eventbuild_allow_urban_production = { #Sets eventbuild to allow all production buildings, country scope only
	set_country_flag = eventbuild_allow_workshop
	set_country_flag = eventbuild_allow_corporation_guild
	set_country_flag = eventbuild_allow_manufactory
	set_country_flag = eventbuild_allow_factory
	set_country_flag = eventbuild_allow_steam_powered_factory
}

eventbuild_allow_special = {
	set_country_flag = eventbuild_allow_bank
	set_country_flag = eventbuild_allow_stock_exchange
	set_country_flag = eventbuild_allow_art_corporation
	set_country_flag = eventbuild_allow_fine_arts_academy
	set_country_flag = eventbuild_allow_opera
	set_country_flag = eventbuild_allow_bureaucracy_1
	set_country_flag = eventbuild_allow_bureaucracy_2
	set_country_flag = eventbuild_allow_bureaucracy_3
	set_country_flag = eventbuild_allow_bureaucracy_4
	set_country_flag = eventbuild_allow_bureaucracy_5
	#set_country_flag = eventbuild_allow_local_parliament
	set_country_flag = eventbuild_allow_regional_capital
}

eventbuild_allow_special_finance = {
	set_country_flag = eventbuild_allow_bank
	set_country_flag = eventbuild_allow_stock_exchange
}

eventbuild_allow_special_art = {
	set_country_flag = eventbuild_allow_art_corporation
	set_country_flag = eventbuild_allow_fine_arts_academy
	set_country_flag = eventbuild_allow_opera
}

eventbuild_allow_special_bureaucracy = {
	set_country_flag = eventbuild_allow_bureaucracy_1
	set_country_flag = eventbuild_allow_bureaucracy_2
	set_country_flag = eventbuild_allow_bureaucracy_3
	set_country_flag = eventbuild_allow_bureaucracy_4
	set_country_flag = eventbuild_allow_bureaucracy_5
}

eventbuild_allow_special_administration = {
	#set_country_flag = eventbuild_allow_local_parliament
	set_country_flag = eventbuild_allow_regional_capital
}

eventbuild_buildInProvince_update = { #Keeps eventbuild file abit cleaner, not for general use, province scope only
	set_variable = { which = eventbuild_cash which = building_cost } #changes the local eventbuild_cash to be a standin for building_cost
	subtract_variable = { which = eventbuild_minimum_buildings value = 1 } #reduce free buildings
	set_province_flag = eventbuild_built_here #Flag province so it doesn't get built on more than once per phase
	owner = { #switch to owner scope
		subtract_variable = { which = eventbuild_cash which = PREV } #subtract the local building_cost from our cash
		set_variable = { which = eventbuild_minimum_buildings which = PREV } #update the country free buildings from local
		set_country_flag = eventbuild_built #tell the country it did in fact build this phase
	}
	
	# Will be removed on next census, to make sure urban dev is recalculated before next construction can start
	add_permanent_province_modifier = {
		name = ongoing_or_recent_construction
		duration = -1
	}
}