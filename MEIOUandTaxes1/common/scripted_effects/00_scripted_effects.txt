#	Example:
# 
#	example_effect = {
#		treasury = 100
#		add_adm_power = 50
#	}
#
#
#	In a script file:
#
#	effect = {
#		example_effect = yes
#	}
#
add_or_extend_province_modifier_effect = {
	if = {
		limit = {
			has_province_modifier = $name$
		}
		extend_province_modifier = { name = $name$ duration = $duration$ }
	}
	else = {
		add_province_modifier = { name = $name$ duration = $duration$ }
	}
}
add_or_extend_province_modifier_hidden_effect = {
	if = {
		limit = {
			has_province_modifier = $name$
		}
		extend_province_modifier = { name = $name$ duration = $duration$ }
	}
	else = {
		add_province_modifier = { name = $name$ duration = $duration$ hidden = yes }
	}
}