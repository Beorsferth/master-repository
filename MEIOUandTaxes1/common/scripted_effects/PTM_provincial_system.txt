ptm_subtribe_set_income_PREV = {
	change_variable = { which = ptm_subtribe_Income_Trade 		which = PREV }
	change_variable = { which = ptm_subtribe_Income_RuralProd 	which = PREV }
	change_variable = { which = ptm_subtribe_Income_Mine 		which = PREV }
	change_variable = { which = ptm_subtribe_Income_UrbanProd 	which = PREV }
	change_variable = { which = ptm_subtribe_Income_PopTax  	which = PREV }
	change_variable = { which = ptm_subtribe_Income_WealthTax  	which = PREV }
	change_variable = { which = ptm_subtribe_Income_Manpower 	which = PREV }
	change_variable = { which = ptm_subtribe_being_tribals 		which = PREV }
	
	change_variable = { which = ptm_subtribe_land 				which = PREV }
	change_variable = { which = ptm_subtribe_cavalry_land 		which = PREV }
}

ptm_host_loyalty_effect = {
	if = {
		limit = {
			is_variable_equal = { which = $variable$ 					value = 1 }
		}
		multiply_variable = { which = $variable$ 						which = ptm_substate_loyalty }
		change_variable = { which = $variable$ 							value = 0.5 }
	}
	else = {
		multiply_variable = { which = $variable$ 						which = ptm_substate_loyalty }
	}
}

ptm_subtribe_recalc_all = {
	every_province = {
		limit = {
			is_city = yes
			OR = {
				is_variable_equal = { which = ptm_kingdom_type 		value = 2 }
				is_variable_equal = { which = ptm_duchy_type 		value = 2 }
				is_variable_equal = { which = ptm_county_type 		value = 2 }
			}
		}
		set_variable = { which = ptm_subtribe_Income_Trade 				value = 0 }
		set_variable = { which = ptm_subtribe_Income_RuralProd 			value = 0 }
		set_variable = { which = ptm_subtribe_Income_Mine 				value = 0 }
		set_variable = { which = ptm_subtribe_Income_UrbanProd	 		value = 0 }
		set_variable = { which = ptm_subtribe_Income_PopTax  			value = 0 }
		set_variable = { which = ptm_subtribe_Income_WealthTax  		value = 0 }
		set_variable = { which = ptm_subtribe_Income_Manpower 			value = 0 }
		set_variable = { which = ptm_subtribe_being_tribals 			value = 0 }
		
		set_variable = { which = ptm_subtribe_land 				value = 0 }
		set_variable = { which = ptm_subtribe_cavalry_land 		value = 0 }
	}
	every_province = {### Tribal share of production and trade, for tribal subtribe provinces
		limit = {
			is_city = yes
			OR = {
				is_variable_equal = { which = ptm_kingdom_type 		value = 2 }
				is_variable_equal = { which = ptm_duchy_type 		value = 2 }
				is_variable_equal = { which = ptm_county_type 		value = 2 }
			}
		}
		change_variable = { which = ptm_subtribe_Income_Trade 		which = EstateIncome_Trade }
		change_variable = { which = ptm_subtribe_Income_RuralProd 	which = EstateIncome_RuralProd }
		change_variable = { which = ptm_subtribe_Income_Mine 		which = EstateIncome_Mine }
		change_variable = { which = ptm_subtribe_Income_UrbanProd 	which = EstateIncome_UrbanProd }
		change_variable = { which = ptm_subtribe_Income_PopTax  	which = EstateIncome_PopTax }
		change_variable = { which = ptm_subtribe_Income_WealthTax  	which = EstateIncome_WealthTax }
		change_variable = { which = ptm_subtribe_Income_Manpower 	which = EstateIncome_Manpower }
		change_variable = { which = ptm_subtribe_being_tribals 		which = estate_tribals_being_tribals }
		
		change_variable = { which = ptm_subtribe_land				value = 1 }
		if = {
			limit = {
				NOT = { has_province_flag = no_horses_present }
			}
			change_variable = { which = ptm_subtribe_cavalry_land		value = 1 }
		}
		
		if = {
			limit = {
				is_variable_equal = { which = ptm_kingdom_type 			value = 2 }
				check_variable = { which = ptm_kingdom_id 				value = 1 }
				NOT = { check_variable = { which = ptm_capital_level 	value = 2 } }
			}
			set_variable = { which = prov_to_search 				which = ptm_kingdom_id }
		}
		else_if = {
			limit = {
				is_variable_equal = { which = ptm_duchy_type 			value = 2 }
				check_variable = { which = ptm_duchy_id 				value = 1 }
				NOT = { check_variable = { which = ptm_capital_level 	value = 1 } }
			}
			set_variable = { which = prov_to_search 				which = ptm_duchy_id }
		}
		if = {
			limit = {
				check_variable = { which = prov_to_search 			value = 1 }
			}
			province_binary_search = {
				variable_name = prov_to_search
				scripted_effect = ptm_subtribe_set_income_PREV
			}
			set_variable = { which = ptm_subtribe_Income_Trade 			value = 0 }
			set_variable = { which = ptm_subtribe_Income_RuralProd 		value = 0 }
			set_variable = { which = ptm_subtribe_Income_Mine 			value = 0 }
			set_variable = { which = ptm_subtribe_Income_UrbanProd	 	value = 0 }
			set_variable = { which = ptm_subtribe_Income_PopTax  		value = 0 }
			set_variable = { which = ptm_subtribe_Income_WealthTax  	value = 0 }
			set_variable = { which = ptm_subtribe_Income_Manpower 		value = 0 }
			set_variable = { which = ptm_subtribe_being_tribals 		value = 0 }
			
			set_variable = { which = prov_to_search 					value = 0 }
		}
	}
	
	set_variable = { 	which = ptm_subtribe_maintenance_modifier 			value = 0 } ### This will scale up levy maintenance over time.  Eventually, it becomes more expensive than state troops, incentivizing revokation of levy privileges
	set_variable = { 	which = ptm_subtribe_maintenance_modifier 			value = 250 }
	change_variable = { which = ptm_subtribe_maintenance_modifier 			which = year_ticker }
	divide_variable = { which = ptm_subtribe_maintenance_modifier 			value = 250 }
	
	every_province = {
		limit = {
			is_city = yes
			OR = {
				AND = {
					is_variable_equal = { which = ptm_capital_level 		value = 2 }
					is_variable_equal = { which = ptm_kingdom_type 		value = 2 }
				}
				AND = {
					is_variable_equal = { which = ptm_capital_level 		value = 1 }
					NOT = { check_variable = { which = ptm_kingdom_id 			value = 1 } }
					is_variable_equal = { which = ptm_duchy_type 		value = 2 }
				}
				AND = {
					NOT = { check_variable = { which = ptm_kingdom_id 			value = 1 } }
					NOT = { check_variable = { which = ptm_duchy_id 			value = 1 } }
					is_variable_equal = { which = ptm_county_type 		value = 2 }
				}
			}
		}
		set_variable = { which = ptm_subtribe_maintenance_modifier 		which = PREV }
		set_variable = { which = ptm_subtribe_treasury_annual 			value = 0 }
	
		multiply_variable = { which = ptm_subtribe_Income_Manpower		value = 20 } # 1000 men = 20 ducats
		divide_variable = { which = ptm_subtribe_Income_Manpower		value = 1.5 } # Consider that manpower -> ducat conversion should happen only at construction, so penalty
		divide_variable = { which = ptm_subtribe_Income_Manpower		value = 25 } # 25 years for 0% manpower to reach 100%
		
		change_variable = { which = ptm_subtribe_treasury_annual 		which = ptm_subtribe_Income_Trade }
		change_variable = { which = ptm_subtribe_treasury_annual 		which = ptm_subtribe_Income_RuralProd }
		change_variable = { which = ptm_subtribe_treasury_annual 		which = ptm_subtribe_Income_Mine }
		change_variable = { which = ptm_subtribe_treasury_annual	 	which = ptm_subtribe_Income_UrbanProd }
		change_variable = { which = ptm_subtribe_treasury_annual  		which = ptm_subtribe_Income_PopTax }
		change_variable = { which = ptm_subtribe_treasury_annual  		which = ptm_subtribe_Income_WealthTax }
		change_variable = { which = ptm_subtribe_treasury_annual 		which = ptm_subtribe_Income_Manpower }
		change_variable = { which = ptm_subtribe_treasury_annual		which = ptm_subtribe_being_tribals }
		
		if = {
			limit = {
				has_global_flag = initialising_estates
			}
			set_variable = { which = ptm_subtribe_treasury_startup_ticker 		value = 0 }
			set_variable = { which = ptm_subtribe_wealth 						value = 0 }
			change_variable = { which = ptm_subtribe_wealth 					which = ptm_subtribe_treasury_annual }
			set_variable = { which = ptm_subtribe_treasury_startup_ticker 		which = ptm_subtribe_wealth }
			
			if = {
				limit = {
					owner = { tag = WHI }
					NOT = { is_year = 1358 }
				}
				multiply_variable = { which = ptm_subtribe_treasury_startup_ticker 	value = 20 }
			}
			else_if = {
				limit = {
					owner = { tag = BLU }
					NOT = { is_year = 1358 }
				}
				multiply_variable = { which = ptm_subtribe_treasury_startup_ticker 	value = 10 }
			}
			else_if = {
				limit = {
					owner = { tag = CHU }
					NOT = { is_year = 1358 }
				}
				multiply_variable = { which = ptm_subtribe_treasury_startup_ticker 	value = 7.5 }
			}
			else = {
				multiply_variable = { which = ptm_subtribe_treasury_startup_ticker 	value = 5 }
			}
			
			multiply_variable = { which = ptm_subtribe_treasury_startup_ticker 	value = 0.925 }
			change_variable = { which = ptm_subtribe_wealth						value = 0.1 }
			change_variable = { which = ptm_subtribe_wealth 					which = ptm_subtribe_treasury_startup_ticker }
			
			set_variable = { which = ptm_subtribe_treasury_startup_ticker		value = 0 }
		}
		else = {
			change_variable = { which = ptm_subtribe_treasury_annual			value = 0.1 } ### Giving something to work off of for dinky little tribes
			change_variable = { which = ptm_subtribe_wealth						which = ptm_subtribe_treasury_annual }
		}
		set_variable = { which = ptm_subtribe_wealth_spent		value = 0 }
		set_variable = { which = ptm_subtribe_wealth_spent 		which = ptm_subtribe_wealth }
		
		if = {
			limit = {
				NOT = {
					has_province_flag = ptm_subtribe_support
				}
			}
			if = {
				limit = {
					NOT = { check_variable = { which = ptm_subtribe_cavalry_land		value = 1 } }
				}
				set_variable = { 	  which = ptm_subtribe_infantry_host		value = 0 }
				set_variable = { 	  which = ptm_subtribe_infantry_host		which = ptm_subtribe_wealth_spent }
				multiply_variable = { which = ptm_subtribe_infantry_host		which = ptm_subtribe_maintenance_modifier }
				set_variable = { 	  which = ptm_subtribe_cavalry_host			value = 0.001 }
				divide_variable = {   which = ptm_subtribe_infantry_host		value = 7.5 } ### Troops are infantry only
				divide_variable = {   which = ptm_subtribe_infantry_host		value = 1000 }
				multiply_variable = { which = ptm_subtribe_infantry_host		value = 1000 }
			}
			else = {
				divide_variable = { which = ptm_subtribe_cavalry_land			which = ptm_subtribe_land }
				set_variable = { which = ptm_subtribe_cavalry_ratio				which = ptm_subtribe_cavalry_land }
				if = {
					limit = {
						owner = { is_nomad = yes }
					}
					multiply_variable = { which = ptm_subtribe_cavalry_ratio			value = 0.8 }
				}
				else = {
					multiply_variable = { which = ptm_subtribe_cavalry_ratio			value = 0.4 }
				}
				set_variable = { which = ptm_subtribe_infantry_ratio				value = 1 }
				subtract_variable = { which = ptm_subtribe_infantry_ratio			which = ptm_subtribe_cavalry_ratio }
				
				set_variable = { 	  which = ptm_subtribe_cavalry_host			value = 0 } ### Tribal troops don't pay maintenance and are always at forcelimit unless killed.  Forcelimit is determined by how much each month is spent from the wealth pool.
				set_variable = { 	  which = ptm_subtribe_infantry_host		value = 0 }
				set_variable = { 	  which = ptm_subtribe_cavalry_host			which = ptm_subtribe_wealth_spent }
				multiply_variable = { which = ptm_subtribe_cavalry_host			which = ptm_subtribe_maintenance_modifier }
				set_variable = { 	  which = ptm_subtribe_infantry_host		which = ptm_subtribe_wealth_spent }
				multiply_variable = { which = ptm_subtribe_infantry_host		which = ptm_subtribe_maintenance_modifier }
				
				divide_variable = {   which = ptm_subtribe_cavalry_host			value = 7.5 }
				divide_variable = {   which = ptm_subtribe_infantry_host		value = 7.5 }
				multiply_variable = {   which = ptm_subtribe_cavalry_host		which = ptm_subtribe_cavalry_ratio }
				multiply_variable = {   which = ptm_subtribe_infantry_host		which = ptm_subtribe_infantry_ratio }
				
				divide_variable = {   which = ptm_subtribe_cavalry_host			value = 1000 }
				divide_variable = {   which = ptm_subtribe_infantry_host		value = 1000 }
				multiply_variable = {   which = ptm_subtribe_cavalry_host		value = 1000 }
				multiply_variable = {   which = ptm_subtribe_infantry_host		value = 1000 }
			}
		}
		if = {
			limit = {
				owner = { is_nomad = yes }
			}
			if = {
				limit = {
					NOT = { check_variable = { which = ptm_subtribe_cavalry_host 	value = 1 } }
				}
				set_variable = { which = ptm_subtribe_cavalry_host 	value = 1 }
			}
		}
		else_if = {
			limit = {
				NOT = { check_variable = { which = ptm_subtribe_infantry_host 	value = 1 } }
			}
			set_variable = { which = ptm_subtribe_infantry_host 				value = 1 }
		}
		set_variable = { 	which = ptm_subtribe_total_soldiers					value = 0 }
		change_variable = { which = ptm_subtribe_total_soldiers 				which = ptm_subtribe_cavalry_host }
		change_variable = { which = ptm_subtribe_total_soldiers 				which = ptm_subtribe_infantry_host }
		
		set_variable = { which = ptm_subtribe_power_share 							value = 0 }
		change_variable = { which = ptm_subtribe_power_share 						which = ptm_subtribe_infantry_host }
		change_variable = { which = ptm_subtribe_power_share 						which = ptm_subtribe_cavalry_host }
		
		#cleanup
		set_variable = { which = ptm_subtribe_Income_Trade 				value = 0 }
		set_variable = { which = ptm_subtribe_Income_RuralProd 			value = 0 }
		set_variable = { which = ptm_subtribe_Income_Mine 				value = 0 }
		set_variable = { which = ptm_subtribe_Income_UrbanProd	 		value = 0 }
		set_variable = { which = ptm_subtribe_Income_PopTax  			value = 0 }
		set_variable = { which = ptm_subtribe_Income_WealthTax  		value = 0 }
		set_variable = { which = ptm_subtribe_Income_Manpower 			value = 0 }
		set_variable = { which = ptm_subtribe_being_tribals 			value = 0 }
		
		set_variable = { which = ptm_subtribe_infantry_ratio 			value = 0 }
		set_variable = { which = ptm_subtribe_cavalry_ratio 			value = 0 }
		set_variable = { which = ptm_subtribe_maintenance_modifier 		value = 0 }
	}
	set_variable = { 	which = ptm_subtribe_maintenance_modifier 			value = 0 }
}
	
ptm_subtribe_add_all = {
	set_variable = { which = estate_tribals_subtribe_infantry 					value = 0.001 }
	set_variable = { which = estate_tribals_subtribe_cavalry 					value = 0.001 }
	every_owned_province = { ### Determines treasuries and other money related stats for estates
		limit = {
			is_city = yes
			OR = {
				AND = {
					is_variable_equal = { which = ptm_capital_level 		value = 2 }
					is_variable_equal = { which = ptm_kingdom_type 		value = 2 }
				}
				AND = {
					is_variable_equal = { which = ptm_capital_level 		value = 1 }
					NOT = { check_variable = { which = ptm_kingdom_id 			value = 1 } }
					is_variable_equal = { which = ptm_duchy_type 		value = 2 }
				}
				AND = {
					NOT = { check_variable = { which = ptm_kingdom_id 			value = 1 } }
					NOT = { check_variable = { which = ptm_duchy_id 			value = 1 } }
					is_variable_equal = { which = ptm_county_type 		value = 2 }
				}
			}
		}
		if = {
			limit = {
				check_variable = { which = ptm_subtribe_infantry_host 				value = 1 }
			}
			set_variable = { which = estate_tribals_subtribe_infantry 				value = 0 }
			change_variable = { which = estate_tribals_subtribe_infantry 			which = ptm_subtribe_infantry_host }
			if = {
				limit = {
					NOT = { has_province_flag = ptm_subtribe_support }
				}
				ptm_host_loyalty_effect = {
					variable = estate_tribals_subtribe_infantry
				}
			}
			PREV = {
				change_variable = { which = estate_tribals_subtribe_infantry 		which = PREV }
			}
		}
		if = {
			limit = {
				check_variable = { which = ptm_subtribe_cavalry_host 				value = 1 }
			}
			set_variable = { which = estate_tribals_subtribe_cavalry 				value = 0 }
			change_variable = { which = estate_tribals_subtribe_cavalry 			which = ptm_subtribe_cavalry_host }
			if = {
				limit = {
					NOT = { has_province_flag = ptm_subtribe_support }
				}
				ptm_host_loyalty_effect = {
					variable = estate_tribals_subtribe_cavalry
				}
			}
			PREV = {
				change_variable = { which = estate_tribals_subtribe_cavalry 		which = PREV }
			}
		}
		
		set_variable = { which = estate_tribals_subtribe_infantry 				value = 0 }
		set_variable = { which = estate_tribals_subtribe_cavalry 				value = 0 }
	}
	divide_variable = { which = estate_tribals_subtribe_cavalry					value = 1000 }
	multiply_variable = { which = estate_tribals_subtribe_cavalry				value = 1000 }
	
	divide_variable = { which = estate_tribals_subtribe_infantry				value = 1000 }
	multiply_variable = { which = estate_tribals_subtribe_infantry				value = 1000 }
}

ptm_subtribe_power_share_calc = {
	every_owned_province = { ### Determines treasuries and other money related stats for estates
		limit = {
			is_city = yes
			OR = {
				AND = {
					is_variable_equal = { which = ptm_capital_level 			value = 2 }
					is_variable_equal = { which = ptm_kingdom_type 				value = 2 }
				}
				AND = {
					is_variable_equal = { which = ptm_capital_level 			value = 1 }
					NOT = { check_variable = { which = ptm_kingdom_id 			value = 1 } }
					is_variable_equal = { which = ptm_duchy_type 				value = 2 }
				}
				AND = {
					NOT = { check_variable = { which = ptm_kingdom_id 			value = 1 } }
					NOT = { check_variable = { which = ptm_duchy_id 			value = 1 } }
					is_variable_equal = { which = ptm_county_type 				value = 2 }
				}
			}
		}
		set_variable = { which = estate_tribals_total_soldiers 					value = 0.001 }
		change_variable = { which = estate_tribals_total_soldiers 				which = PREV }
		if = {
			limit = {
				check_variable = { which = ptm_subtribe_power_share 			value = 1 }
			}
			divide_variable = { which = ptm_subtribe_power_share 				which = estate_tribals_total_soldiers }
		}
		multiply_variable = { which = ptm_subtribe_power_share 					value = 100 } # 100-0%
		set_variable = { which = estate_tribals_total_soldiers 					value = 0 }
	}
}

ptm_subtribe_raise_precalc = {
	if = {
		limit = {
			check_variable = { which = ptm_subtribe_infantry_host			value = 1 }
		}
		set_variable = { 	  which = ptm_partial_support_inf							value = 0 }
		ptm_host_loyalty_effect = {
			variable = ptm_partial_support_inf
		}
		change_variable = {   which = ptm_partial_support_inf							which = ptm_subtribe_infantry_host }
		divide_variable = {   which = ptm_partial_support_inf							value = 2 }
		divide_variable = {   which = ptm_partial_support_inf							value = 1000 }
		multiply_variable = { which = ptm_partial_support_inf							value = 1000 }
		
		set_variable = { 	  which = ptm_full_support_inf								value = 0 }
		ptm_host_loyalty_effect = {
			variable = ptm_full_support_inf
		}
		change_variable = {   which = ptm_full_support_inf								which = ptm_subtribe_infantry_host }
		divide_variable = {   which = ptm_full_support_inf								value = 1.3 }
		divide_variable = {   which = ptm_full_support_inf								value = 1000 }
		multiply_variable = { which = ptm_full_support_inf								value = 1000 }
	}
	
	if = {
		limit = {
			check_variable = { which = ptm_subtribe_cavalry_host						value = 1 }
		}
		set_variable = { 	  which = ptm_partial_support_cav 							value = 0 }
		ptm_host_loyalty_effect = {
			variable = ptm_partial_support_cav
		}
		change_variable = {   which = ptm_partial_support_cav							which = ptm_subtribe_cavalry_host }
		divide_variable = {   which = ptm_partial_support_cav							value = 2 }
		divide_variable = {   which = ptm_partial_support_cav							value = 1000 }
		multiply_variable = { which = ptm_partial_support_cav							value = 1000 }
		
		set_variable = { 	  which = ptm_full_support_cav 								value = 0 }
		ptm_host_loyalty_effect = {
			variable = ptm_full_support_cav
		}
		change_variable = {   which = ptm_full_support_cav								which = ptm_subtribe_cavalry_host }
		divide_variable = {   which = ptm_full_support_cav								value = 1.3 }
		divide_variable = {   which = ptm_full_support_cav								value = 1000 }
		multiply_variable = { which = ptm_full_support_cav								value = 1000 }
	}
	
	if = { ### Make sure you always get at least 1 cavalry
		limit = {
			owner = { is_nomad = yes }
		}
		if = {
			limit = {
				check_variable = { which = ptm_subtribe_cavalry_host			value = 1 }
				NOT = { check_variable = { which = ptm_partial_support_cav		value = 1 } }
			}
			set_variable = { which = ptm_partial_support_cav					value = 1 }
		}
		if = {
			limit = {
				check_variable = { which = ptm_subtribe_cavalry_host			value = 1 }
				NOT = { check_variable = { which = ptm_full_support_cav		value = 1 } }
			}
			set_variable = { which = ptm_full_support_cav						value = 1 }
		}
	}
	else = { ### Make sure you always get at least 1 infantry
		if = {
			limit = {
				check_variable = { which = ptm_subtribe_infantry_host				value = 1 }
				NOT = { check_variable = { which = ptm_partial_support_inf			value = 1 } }
			}
			set_variable = { which = ptm_partial_support_inf						value = 1 }
		}
		if = {
			limit = {
				check_variable = { which = ptm_subtribe_infantry_host			value = 1 }
				NOT = { check_variable = { which = ptm_full_support_inf		value = 1 } }
			}
			set_variable = { which = ptm_full_support_inf						value = 1 }
		}
	}
		
	set_variable = {      which = estate_logistical_support_partial		value = 0 }
	change_variable = {   which = estate_logistical_support_partial		which = ptm_partial_support_inf }
	change_variable = {   which = estate_logistical_support_partial		which = ptm_partial_support_cav }
	
	set_variable = {      which = estate_logistical_support_full		value = 0 }
	change_variable = {   which = estate_logistical_support_full		which = ptm_full_support_inf }
	change_variable = {   which = estate_logistical_support_full		which = ptm_full_support_cav }
		
	set_variable = { 	  which = partial_raising_host_cost 			value = 0 }
	change_variable = {	  which = partial_raising_host_cost 			which = estate_logistical_support_partial }
	multiply_variable = { which = partial_raising_host_cost				value = 10 }
	
	set_variable = { 	  which = full_raising_host_cost 				value = 0 }
	change_variable = {   which = full_raising_host_cost 				which = estate_logistical_support_full }
	multiply_variable = { which = full_raising_host_cost				value = 15 }
}

ptm_subtribe_raise_all_precalc = {
	set_variable = { which = ptm_partial_support_inf							value = 0 }
	set_variable = { which = ptm_partial_support_cav							value = 0 }
	set_variable = { which = ptm_full_support_inf								value = 0 }
	set_variable = { which = ptm_full_support_cav								value = 0 }
	
	set_variable = { which = estate_logistical_support_partial_subtribe			value = 0 }
	set_variable = { which = estate_logistical_support_full_subtribe			value = 0 }
	set_variable = { which = partial_raising_host_cost_subtribe					value = 0 }
	set_variable = { which = full_raising_host_cost_subtribe					value = 0 }
	every_owned_province = { ### Determines treasuries and other money related stats for estates
		limit = {
			is_city = yes
			OR = {
				AND = {
					is_variable_equal = { which = ptm_capital_level 			value = 2 }
					is_variable_equal = { which = ptm_kingdom_type 				value = 2 }
				}
				AND = {
					is_variable_equal = { which = ptm_capital_level 			value = 1 }
					NOT = { check_variable = { which = ptm_kingdom_id 			value = 1 } }
					is_variable_equal = { which = ptm_duchy_type 				value = 2 }
				}
				AND = {
					NOT = { check_variable = { which = ptm_kingdom_id 			value = 1 } }
					NOT = { check_variable = { which = ptm_duchy_id 			value = 1 } }
					is_variable_equal = { which = ptm_county_type 				value = 2 }
				}
			}
			NOT = {
				has_province_flag = ptm_subtribe_support
			}
		}
		ptm_subtribe_raise_precalc = yes
		set_variable = { which = estate_logistical_support_partial_subtribe			which = estate_logistical_support_partial }
		set_variable = { which = estate_logistical_support_full_subtribe			which = estate_logistical_support_full }
		set_variable = { which = partial_raising_host_cost_subtribe					which = partial_raising_host_cost }
		set_variable = { which = full_raising_host_cost_subtribe					which = full_raising_host_cost }
		PREV = {
			change_variable = { which = ptm_partial_support_inf						which = PREV }
			change_variable = { which = ptm_partial_support_cav						which = PREV }
			change_variable = { which = ptm_full_support_inf						which = PREV }
			change_variable = { which = ptm_full_support_cav						which = PREV }
			
			change_variable = { which = estate_logistical_support_partial_subtribe	which = PREV }
			change_variable = { which = estate_logistical_support_full_subtribe		which = PREV }
			change_variable = { which = partial_raising_host_cost_subtribe			which = PREV }
			change_variable = { which = full_raising_host_cost_subtribe				which = PREV }
		}
		set_variable = { which = estate_logistical_support_partial_subtribe			value = 0 }
		set_variable = { which = estate_logistical_support_full_subtribe			value = 0 }
		set_variable = { which = partial_raising_host_cost_subtribe					value = 0 }
		set_variable = { which = full_raising_host_cost_subtribe					value = 0 }
	}
}

ptm_subtribe_raise = {
	set_variable = { which = ptm_infantry_support 	which = ptm_$decided_type$_support_inf }
	set_variable = { which = ptm_cavalry_support 	which = ptm_$decided_type$_support_cav }
	set_province_flag = ptm_subtribe_support
	hidden_effect = {
		owner = {
			trigger_switch = {
				on_trigger = has_country_flag
				mil_tech_0_war = { ### Level 0 Levies
					PREV = {
						ptm_substate_raise_type = {
							estate_name = tribes
							type = infantry
							tech = 0
						}
						ptm_substate_raise_type = {
							estate_name = tribes
							type = cavalry
							tech = 0
						}
					}
				}
				mil_tech_10_war = { ### Level 10 Levies
					PREV = {
						ptm_substate_raise_type = {
							estate_name = tribes
							type = infantry
							tech = 10
						}
						ptm_substate_raise_type = {
							estate_name = tribes
							type = cavalry
							tech = 10
						}
					}
				}
				mil_tech_20_war = { ### Level 20 Levies
					PREV = {
						ptm_substate_raise_type = {
							estate_name = tribes
							type = infantry
							tech = 20
						}
						ptm_substate_raise_type = {
							estate_name = tribes
							type = cavalry
							tech = 20
						}
					}
				}
				mil_tech_30_war = { ### Level 30 Levies
					PREV = {
						ptm_substate_raise_type = {
							estate_name = tribes
							type = infantry
							tech = 30
						}
						ptm_substate_raise_type = {
							estate_name = tribes
							type = cavalry
							tech = 30
						}
					}
				}
				mil_tech_40_war = { ### Level 40 Levies
					PREV = {
						ptm_substate_raise_type = {
							estate_name = tribes
							type = infantry
							tech = 40
						}
						ptm_substate_raise_type = {
							estate_name = tribes
							type = cavalry
							tech = 40
						}
					}
				}
				mil_tech_50_war = { ### Level 50 Levies
					PREV = {
						ptm_substate_raise_type = {
							estate_name = tribes
							type = infantry
							tech = 50
						}
						ptm_substate_raise_type = {
							estate_name = tribes
							type = cavalry
							tech = 50
						}
					}
				}
			}
		}
	}
	set_variable = { which = ptm_infantry_fielded 						value = 0 }
	set_variable = { which = ptm_infantry_fielded 						which = ptm_$decided_type$_support_inf }
	set_variable = { which = ptm_cavalry_fielded 						value = 0 }
	set_variable = { which = ptm_cavalry_fielded 						which = ptm_$decided_type$_support_cav }
	
	set_variable = { which = ptm_total_fielded 							value = 0 }
	change_variable = { which = ptm_total_fielded 						which = ptm_infantry_fielded }
	change_variable = { which = ptm_total_fielded 						which = ptm_cavalry_fielded }
	if = {
		limit = {
			NOT = { check_variable = { which = ptm_subtribe_war_spent	value = 0.001 }	}
		}
		set_variable = { which = ptm_subtribe_war_spent					value = 0 }
	}
	change_variable = { which = ptm_subtribe_war_spent 					which = $decided_type$_raising_host_cost }
	subtract_variable = { which = ptm_subtribe_wealth					which = $decided_type$_raising_host_cost }
	
	set_variable = { which = estate_logistical_support					value = 0 }
	set_variable = { which = estate_logistical_support					which = estate_logistical_support_$decided_type$ }
	owner = {
		if = { ### Used to determine how much extra forcelimit the player receives
			limit = {
				NOT = { check_variable = { which = estate_logistical_support	value = 0.01 }	}
			}
			set_variable = { which = estate_logistical_support			value = 0 }
		}
		change_variable = { which = estate_logistical_support			which = PREV }
	}
	
	subtract_variable = { which = ptm_subtribe_infantry_host			which = ptm_infantry_fielded }

	subtract_variable = { which = ptm_subtribe_cavalry_host				which = ptm_cavalry_fielded }
	
	#cleanup
	set_variable = { which = ptm_partial_support_inf 					value = 0 }
	set_variable = { which = ptm_partial_support_cav 					value = 0 }
	set_variable = { which = ptm_full_support_inf 						value = 0 }
	set_variable = { which = ptm_full_support_cav 						value = 0 }
	
	set_variable = { which = ptm_infantry_support 						value = 0 }
	set_variable = { which = ptm_cavalry_support 						value = 0 }
	
	set_variable = { which = estate_logistical_support_partial 			value = 0 }
	set_variable = { which = estate_logistical_support_full 			value = 0 }
	set_variable = { which = estate_logistical_support 					value = 0 }
	
	set_variable = { which = full_raising_host_cost 					value = 0 }
	set_variable = { which = partial_raising_host_cost 					value = 0 }
}

ptm_subtribe_war_share_calc = {
	every_owned_province = { ### Determines treasuries and other money related stats for estates
		limit = {
			is_city = yes
			OR = {
				AND = {
					is_variable_equal = { which = ptm_capital_level 			value = 2 }
					is_variable_equal = { which = ptm_kingdom_type 				value = 2 }
				}
				AND = {
					is_variable_equal = { which = ptm_capital_level 			value = 1 }
					NOT = { check_variable = { which = ptm_kingdom_id 			value = 1 } }
					is_variable_equal = { which = ptm_duchy_type 				value = 2 }
				}
				AND = {
					NOT = { check_variable = { which = ptm_kingdom_id 			value = 1 } }
					NOT = { check_variable = { which = ptm_duchy_id 			value = 1 } }
					is_variable_equal = { which = ptm_county_type 				value = 2 }
				}
			}
		}
		set_variable = { which = $total_infantry_share$							value = 0 }
		change_variable = { which = $total_infantry_share$ 						which = PREV }
		set_variable = { which = $total_cavalry_share$							value = 0 }
		change_variable = { which = $total_cavalry_share$ 						which = PREV }
		
		set_variable = { which = ptm_war_share_infantry							value = 0 }
		set_variable = { which = ptm_war_share_infantry							which = ptm_infantry_fielded }
		set_variable = { which = ptm_war_share_cavalry							value = 0 }
		set_variable = { which = ptm_war_share_cavalry							which = ptm_cavalry_fielded }
		if = {
			limit = {
				check_variable = { which = ptm_war_share_infantry 				value = 1 }
			}
			divide_variable = { which = ptm_war_share_infantry 					which = $total_infantry_share$ }
		}
		if = {
			limit = {
				check_variable = { which = ptm_war_share_cavalry 				value = 1 }
			}
			divide_variable = { which = ptm_war_share_cavalry 					which = $total_cavalry_share$ }
		}
		set_variable = { which = $total_infantry_share$ 						value = 0 }
		set_variable = { which = $total_cavalry_share$ 							value = 0 }
	}
}

ptm_subtribe_raise_all = {
	every_owned_province = { ### Determines treasuries and other money related stats for estates
		limit = {
			is_city = yes
			OR = {
				AND = {
					is_variable_equal = { which = ptm_capital_level 			value = 2 }
					is_variable_equal = { which = ptm_kingdom_type 				value = 2 }
				}
				AND = {
					is_variable_equal = { which = ptm_capital_level 			value = 1 }
					NOT = { check_variable = { which = ptm_kingdom_id 			value = 1 } }
					is_variable_equal = { which = ptm_duchy_type 				value = 2 }
				}
				AND = {
					NOT = { check_variable = { which = ptm_kingdom_id 			value = 1 } }
					NOT = { check_variable = { which = ptm_duchy_id 			value = 1 } }
					is_variable_equal = { which = ptm_county_type 				value = 2 }
				}
			}
			NOT = {
				has_province_flag = ptm_subtribe_support
			}
		}
		ptm_subtribe_raise = {
			decided_type = $decided_type$
		}
	}
}

ptm_subtribe_casualty_calc = {
	every_owned_province = { ### Determines treasuries and other money related stats for estates
		limit = {
			is_city = yes
			OR = {
				AND = {
					is_variable_equal = { which = ptm_capital_level 			value = 2 }
					is_variable_equal = { which = ptm_kingdom_type 				value = 2 }
				}
				AND = {
					is_variable_equal = { which = ptm_capital_level 			value = 1 }
					NOT = { check_variable = { which = ptm_kingdom_id 			value = 1 } }
					is_variable_equal = { which = ptm_duchy_type 				value = 2 }
				}
				AND = {
					NOT = { check_variable = { which = ptm_kingdom_id 			value = 1 } }
					NOT = { check_variable = { which = ptm_duchy_id 			value = 1 } }
					is_variable_equal = { which = ptm_county_type 				value = 2 }
				}
			}
			has_province_flag = ptm_subtribe_support
		}
		set_variable = { which = TR_infantry_remaining				value = 0 }
		set_variable = { which = TR_infantry_remaining				which = PREV }
		multiply_variable = { which = TR_infantry_remaining			which = ptm_war_share_infantry }
		
		set_variable = { which = TR_cavalry_remaining				value = 0 }
		set_variable = { which = TR_cavalry_remaining				which = PREV }
		multiply_variable = { which = TR_cavalry_remaining			which = ptm_war_share_cavalry }
		
		change_variable = { which = ptm_subtribe_infantry_host		which = TR_infantry_remaining }
		change_variable = { which = ptm_subtribe_cavalry_host		which = TR_cavalry_remaining }
		
		set_variable = { which = TR_infantry_remaining				value = 0 }
		set_variable = { which = TR_cavalry_remaining				value = 0 }
	}
}

ptm_subtribe_sum_recruited = {
	set_variable = { which = ptm_infantry_fielded						value = 0 }
	set_variable = { which = ptm_cavalry_fielded						value = 0 }
	every_owned_province = { ### Determines treasuries and other money related stats for estates
		limit = {
			is_city = yes
			OR = {
				AND = {
					is_variable_equal = { which = ptm_capital_level 			value = 2 }
					is_variable_equal = { which = ptm_kingdom_type 				value = 2 }
				}
				AND = {
					is_variable_equal = { which = ptm_capital_level 			value = 1 }
					NOT = { check_variable = { which = ptm_kingdom_id 			value = 1 } }
					is_variable_equal = { which = ptm_duchy_type 				value = 2 }
				}
				AND = {
					NOT = { check_variable = { which = ptm_kingdom_id 			value = 1 } }
					NOT = { check_variable = { which = ptm_duchy_id 			value = 1 } }
					is_variable_equal = { which = ptm_county_type 				value = 2 }
				}
			}
			has_province_flag = ptm_subtribe_support
		}
		PREV = {
			change_variable = { which = ptm_infantry_fielded			which = PREV }
			change_variable = { which = ptm_cavalry_fielded				which = PREV }
		}
	}
}

ptm_subtribe_postwar_cleanup = {
	every_owned_province = { ### Determines treasuries and other money related stats for estates
		limit = {
			is_city = yes
			has_province_flag = ptm_subtribe_support
		}
		set_variable = { which = ptm_war_share_infantry				value = 0 }
		set_variable = { which = ptm_war_share_cavalry				value = 0 }
		set_variable = { which = ptm_infantry_fielded				value = 0 }
		set_variable = { which = ptm_cavalry_fielded				value = 0 }
		set_variable = { which = ptm_total_fielded 					value = 0 }
		clr_province_flag = ptm_subtribe_support
	}
}