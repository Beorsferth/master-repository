court_recalculation = {
	remove_country_modifier = court_worst
	
	if = {
		limit = {
			check_variable = { which = court_level 	value = 90 }
		}
		add_country_modifier = { name = court_best			duration = -1 hidden = yes }
		remove_country_modifier = court_worst
		remove_country_modifier = court_bad
		remove_country_modifier = court_better
		add_country_modifier = { name = court_level_6		duration = -1 }
	}
	else_if = {
		limit = {
			check_variable = { which = court_level 	value = 65 }
		}
		add_country_modifier = { name = court_best			duration = -1 hidden = yes }
		remove_country_modifier = court_worst
		remove_country_modifier = court_bad
		remove_country_modifier = court_better
		add_country_modifier = { name = court_level_5 		duration = -1 }
	}
	else_if = {
		limit = {
			check_variable = { which = court_level 	value = 45 }
		}
		add_country_modifier = { name = court_better 		duration = -1 hidden = yes }
		remove_country_modifier = court_worst
		remove_country_modifier = court_bad
		remove_country_modifier = court_best
		add_country_modifier = { name = court_level_4 		duration = -1 }
	}
	else_if = {
		limit = {
			check_variable = { which = court_level 	value = 30 }
		}
		add_country_modifier = { name = court_better 		duration = -1 hidden = yes }
		remove_country_modifier = court_worst
		remove_country_modifier = court_bad
		remove_country_modifier = court_best
		add_country_modifier = { name = court_level_3 		duration = -1 }
	}
	else_if = {
		limit = {
			check_variable = { which = court_level 	value = 15 }
		}
		add_country_modifier = { name = court_bad 		duration = -1 hidden = yes }
		remove_country_modifier = court_worst
		remove_country_modifier = court_better
		remove_country_modifier = court_best
		add_country_modifier = { name = court_level_2 	duration = -1 }
	}
	else = {
		add_country_modifier = { name = court_worst 	duration = -1 hidden = yes }
		remove_country_modifier = court_bad
		remove_country_modifier = court_better
		remove_country_modifier = court_best
		add_country_modifier = { name = court_level_1 	duration = -1 }
	}
}

change_title_1 = {
	remove_country_modifier = title_2
	remove_country_modifier = title_3
	remove_country_modifier = title_4
	remove_country_modifier = title_5
	remove_country_modifier = title_6
	add_country_modifier = { name = title_1 duration = -1 }
	#clr_country_flag = title_2
	#clr_country_flag = title_3
	#clr_country_flag = title_4
	#clr_country_flag = title_5
	#clr_country_flag = title_6
	#set_country_flag = title_1
}
change_title_2 = {
	remove_country_modifier = title_1
	remove_country_modifier = title_3
	remove_country_modifier = title_4
	remove_country_modifier = title_5
	remove_country_modifier = title_6
	add_country_modifier = { name = title_2 duration = -1 }
	#clr_country_flag = title_1
	#clr_country_flag = title_3
	#clr_country_flag = title_4
	#clr_country_flag = title_5
	#clr_country_flag = title_6
	#set_country_flag = title_2
}
change_title_3 = {
	remove_country_modifier = title_1
	remove_country_modifier = title_2
	remove_country_modifier = title_4
	remove_country_modifier = title_5
	remove_country_modifier = title_6
	add_country_modifier = { name = title_3 duration = -1 }
	#clr_country_flag = title_1
	#clr_country_flag = title_2
	#clr_country_flag = title_4
	#clr_country_flag = title_5
	#clr_country_flag = title_6
	#set_country_flag = title_3
}
change_title_4 = {
	remove_country_modifier = title_1
	remove_country_modifier = title_2
	remove_country_modifier = title_3
	remove_country_modifier = title_5
	remove_country_modifier = title_6
	add_country_modifier = { name = title_4 duration = -1 }
	#clr_country_flag = title_1
	#clr_country_flag = title_2
	#clr_country_flag = title_3
	#clr_country_flag = title_5
	#clr_country_flag = title_6
	#set_country_flag = title_4
}
change_title_5 = {
	remove_country_modifier = title_1
	remove_country_modifier = title_2
	remove_country_modifier = title_3
	remove_country_modifier = title_4
	remove_country_modifier = title_6
	add_country_modifier = { name = title_5 duration = -1 }
	#clr_country_flag = title_1
	#clr_country_flag = title_2
	#clr_country_flag = title_3
	#clr_country_flag = title_4
	#clr_country_flag = title_6
	#set_country_flag = title_5
}
change_title_6 = {
	remove_country_modifier = title_1
	remove_country_modifier = title_2
	remove_country_modifier = title_3
	remove_country_modifier = title_4
	remove_country_modifier = title_5
	add_country_modifier = { name = title_6 duration = -1 }
	#clr_country_flag = title_1
	#clr_country_flag = title_2
	#clr_country_flag = title_3
	#clr_country_flag = title_4
	#clr_country_flag = title_5
	#set_country_flag = title_6
}






