colonial_information_loop = {
	
}

colonial_settler_colony = {
	
}

colonial_plantation_colony = {
	
}

unpopulated_province = {
	
}

native_population_100 = {
	
}

native_population_1000 = {
	
}

native_population_2000 = {
	
}

native_population_3000 = {
	
}

native_population_4000 = {
	
}

native_population_5000 = {
	
}

native_population_6000 = {
	
}

native_population_7000 = {
	
}

native_population_8000 = {
	
}

native_population_9000 = {
	
}

native_population_10000 = {
	
}

native_population_15000 = {
	
}

native_population_20000 = {
	
}

native_population_25000 = {
	
}

native_population_30000 = {
	
}

native_population_40000 = {
	
}

native_population_50000 = {
	
}

native_population_60000 = {
	
}

native_population_70000 = {
	
}

native_population_80000 = {
	
}

native_population_90000 = {
	
}

native_population_100000 = {
	
}

native_population_120000 = {
	
}

native_population_140000 = {
	
}

native_population_160000 = {
	
}

native_population_180000 = {
	
}

native_population_200000 = {
	
}

native_population_220000 = {
	
}

native_population_240000 = {
	
}

native_population_260000 = {
	
}

native_population_280000 = {
	
}

native_population_300000 = {
	
}

native_population_350000 = {
	
}

native_population_400000 = {
	
}

native_population_450000 = {
	
}

native_population_500000 = {
	
}

slave_looting = {
	
}

slave_population_100 = {
	
}

slave_population_1000 = {
	
}

slave_population_2000 = {
	
}

slave_population_3000 = {
	
}

slave_population_4000 = {
	
}

slave_population_5000 = {
	
}

slave_population_6000 = {
	
}

slave_population_7000 = {
	
}

slave_population_8000 = {
	
}

slave_population_9000 = {
	
}

slave_population_10000 = {
	
}

slave_population_15000 = {
	
}

slave_population_20000 = {
	
}

slave_population_25000 = {
	
}

slave_population_30000 = {
	
}

slave_population_40000 = {
	
}

slave_population_50000 = {
	
}

slave_population_60000 = {
	
}

slave_population_70000 = {
	
}

slave_population_80000 = {
	
}

slave_population_90000 = {
	
}

slave_population_100000 = {
	
}

slave_population_120000 = {
	
}

slave_population_140000 = {
	
}

slave_population_160000 = {
	
}

slave_population_180000 = {
	
}

slave_population_200000 = {
	
}

slave_population_220000 = {
	
}

slave_population_240000 = {
	
}

slave_population_260000 = {
	
}

slave_population_280000 = {
	
}

slave_population_300000 = {
	
}

slave_population_350000 = {
	
}

slave_population_400000 = {
	
}

slave_population_450000 = {
	
}

slave_population_500000 = {
	
}

productivity_100 = {
}

productivity_110 = {
	trade_value_modifier = 0.1
}

productivity_120 = {
	trade_value_modifier = 0.2
}

productivity_130 = {
	trade_value_modifier = 0.3
}

productivity_140 = {
	trade_value_modifier = 0.4
}

productivity_150 = {
	trade_value_modifier = 0.5
}

productivity_160 = {
	trade_value_modifier = 0.6
}

productivity_170 = {
	trade_value_modifier = 0.7
}

productivity_180 = {
	trade_value_modifier = 0.8
}

productivity_190 = {
	trade_value_modifier = 0.9
}

productivity_200 = {
	trade_value_modifier = 1
}

productivity_210 = {
	trade_value_modifier = 1.1
}

productivity_220 = {
	trade_value_modifier = 1.2
}

productivity_230 = {
	trade_value_modifier = 1.3
}

productivity_240 = {
	trade_value_modifier = 1.4
}

productivity_250 = {
	trade_value_modifier = 1.5
}

productivity_260 = {
	trade_value_modifier = 1.6
}

productivity_270 = {
	trade_value_modifier = 1.7
}

productivity_280 = {
	trade_value_modifier = 1.8
}

productivity_290 = {
	trade_value_modifier = 1.9
}

productivity_300 = {
	trade_value_modifier = 2.0
}

productivity_310 = {
	trade_value_modifier = 2.1
}

productivity_320 = {
	trade_value_modifier = 2.2
}

productivity_330 = {
	trade_value_modifier = 2.3
}

productivity_340 = {
	trade_value_modifier = 2.4
}

productivity_350 = {
	trade_value_modifier = 2.5
}

productivity_360 = {
	trade_value_modifier = 2.6
}

productivity_370 = {
	trade_value_modifier = 2.7
}

productivity_380 = {
	trade_value_modifier = 2.8
}

productivity_390 = {
	trade_value_modifier = 2.9
}

productivity_400 = {
	trade_value_modifier = 3.0
}

native_plagues_severity_0 = { #Mild
	picture = plague_passed_over
	local_tax_modifier = -0.05
	province_trade_power_modifier  = -0.05
	trade_value_modifier = -0.05
	local_manpower_modifier = -0.05
	trade_goods_size_modifier = 0.01
}

native_plagues_severity_1 = { #Significant
	picture = plague_mild
	local_tax_modifier = -0.10
	province_trade_power_modifier  = -0.10
	trade_value_modifier = -0.10
	local_manpower_modifier = -0.10
	trade_goods_size_modifier = 0.01
}

native_plagues_severity_2 = { #Threatening
	picture = plague_moderate
	local_tax_modifier = -0.15
	province_trade_power_modifier  = -0.15
	trade_value_modifier = -0.15
	local_manpower_modifier = -0.15
	trade_goods_size_modifier = 0.01
}

native_plagues_severity_3 = { #Severe
	picture = plague_severe
	local_tax_modifier = -0.20
	province_trade_power_modifier  = -0.20
	trade_value_modifier = -0.20
	local_manpower_modifier = -0.20
	trade_goods_size_modifier = 0.01
}

native_plagues_severity_4 = { #Disastrous
	picture = plague_terrible
	local_tax_modifier = -0.30
	province_trade_power_modifier  = -0.30
	trade_value_modifier = -0.30
	local_manpower_modifier = -0.30
	trade_goods_size_modifier = 0.01
}

native_plagues_severity_5 = { #Catastrophic
	picture = plague_devastating
	local_tax_modifier = -0.40
	province_trade_power_modifier  = -0.40
	trade_value_modifier = -0.40
	local_manpower_modifier = -0.40
	trade_goods_size_modifier = 0.01
}

native_plagues_severity_6 = { #Apocalyptic
	picture = plague_devastating
	local_tax_modifier = -0.50
	province_trade_power_modifier  = -0.50
	trade_value_modifier = -0.50
	local_manpower_modifier = -0.50
	trade_goods_size_modifier = 0.01
}

native_plagues_chaos_1 = { #Minor Disruptions
	picture = MAPMODE_rural_pop_6
	local_unrest = 2
	local_autonomy = 0.05
	local_missionary_strength = 0.01
	#trade_goods_size_modifier = 0.01
}

native_plagues_chaos_2 = { #Major Disruptions
	picture = MAPMODE_rural_pop_5
	local_unrest = 5
	local_autonomy = 0.10
	local_missionary_strength = 0.02
	#trade_goods_size_modifier = 0.01
}

native_plagues_chaos_3 = { #Widespread Disorder
	picture = MAPMODE_rural_pop_4
	local_unrest = 10
	local_autonomy = 0.20
	local_missionary_strength = 0.04
	#trade_goods_size_modifier = 0.01
}

native_plagues_chaos_4 = { #Severe Disarray
	picture = MAPMODE_rural_pop_3
	local_unrest = 15
	local_autonomy = 0.40
	local_missionary_strength = 0.06
	#trade_goods_size_modifier = 0.01
}

native_plagues_chaos_5 = { #Pervasive Chaos
	picture = MAPMODE_rural_pop_2
	local_unrest = 20
	local_autonomy = 0.60
	local_missionary_strength = 0.08
	#trade_goods_size_modifier = 0.01
}

native_plagues_chaos_6 = { #Complete Anarchy
	picture = MAPMODE_rural_pop_1
	local_unrest = 30
	local_autonomy = 1.00
	local_missionary_strength = 0.10
	#trade_goods_size_modifier = 0.01
}

native_plagues_intensity_0_1 = {
	picture = MAPMODE_urban_pop_10
	trade_goods_size_modifier = 0.01
}

native_plagues_intensity_1_3 = {
	picture = MAPMODE_urban_pop_9
	trade_goods_size_modifier = 0.01
}

native_plagues_intensity_3_10 = {
	picture = MAPMODE_urban_pop_8
	trade_goods_size_modifier = 0.01
}

native_plagues_intensity_10_20 = {
	picture = MAPMODE_urban_pop_7
	trade_goods_size_modifier = 0.01
}

native_plagues_intensity_20_40 = {
	picture = MAPMODE_urban_pop_6
	trade_goods_size_modifier = 0.01
}

native_plagues_intensity_40_80 = {
	picture = MAPMODE_urban_pop_5
	trade_goods_size_modifier = 0.01
}

native_plagues_intensity_80_160 = {
	picture = MAPMODE_urban_pop_4
	trade_goods_size_modifier = 0.01
}

native_plagues_intensity_160_320 = {
	picture = MAPMODE_urban_pop_3
	trade_goods_size_modifier = 0.01
}

native_plagues_intensity_320_640 = {
	picture = MAPMODE_urban_pop_2
	trade_goods_size_modifier = 0.01
}

native_plagues_intensity_640_1280 = {
	picture = MAPMODE_urban_pop_1
	trade_goods_size_modifier = 0.01
}

native_plagues_ended = {
	picture = WEALTH_15
	#local_unrest = -5
	trade_goods_size_modifier = 0.01
}

native_plagues_vulnerability_000_010 = {
	picture = WEALTH_10
	#trade_goods_size_modifier = 0.01
}

native_plagues_vulnerability_010_020 = {
	picture = WEALTH_9
	#trade_goods_size_modifier = 0.01
}

native_plagues_vulnerability_020_030 = {
	picture = WEALTH_8
	#trade_goods_size_modifier = 0.01
}

native_plagues_vulnerability_030_040 = {
	picture = WEALTH_7
	#trade_goods_size_modifier = 0.01
}

native_plagues_vulnerability_040_050 = {
	picture = WEALTH_6
	#trade_goods_size_modifier = 0.01
}

native_plagues_vulnerability_050_060 = {
	picture = WEALTH_5
	#trade_goods_size_modifier = 0.01
}

native_plagues_vulnerability_060_070 = {
	picture = WEALTH_4
	#trade_goods_size_modifier = 0.01
}

native_plagues_vulnerability_070_080 = {
	picture = WEALTH_3
	#trade_goods_size_modifier = 0.01
}

native_plagues_vulnerability_080_090 = {
	picture = WEALTH_2
	#trade_goods_size_modifier = 0.01
}

native_plagues_vulnerability_090_100 = {
	picture = WEALTH_1
	#trade_goods_size_modifier = 0.01
}

native_plagues_outbreak = {
	picture = Pile_of_Shit
	#trade_goods_size_modifier = 0.01
}

native_plagues_spread_pause = {
}

native_plagues_order_pause = {
}

native_plagues_stockpile_food_1 = {
	picture = plague_mild
	manpower_recovery_speed = -0.05
	global_tax_modifier = -0.10
}

native_plagues_stockpile_food_2 = {
	picture = plague_moderate
	manpower_recovery_speed = -0.10
	global_tax_modifier = -0.20
}

native_plagues_stockpile_food_3 = {
	picture = plague_severe
	manpower_recovery_speed = -0.15
	global_tax_modifier = -0.30
}

native_plagues_borders_1 = {
	picture = plague_mild
	global_tax_modifier = -0.05
	manpower_recovery_speed = -0.05
	global_trade_power = -0.05
}

native_plagues_borders_2 = {
	picture = plague_moderate
	global_tax_modifier = -0.10
	manpower_recovery_speed = -0.10
	global_trade_power = -0.10
}

native_plagues_borders_3 = {
	picture = plague_severe
	global_tax_modifier = -0.15
	manpower_recovery_speed = -0.15
	global_trade_power = -0.15
}

native_plagues_stopping_trade_1 = {
	picture = plague_mild
	global_trade_power = -0.20
	global_trade_goods_size_modifier  = -0.20
}

native_plagues_stopping_trade_2 = {
	picture = plague_moderate
	global_trade_power = -0.40
	global_trade_goods_size_modifier  = -0.40
}

native_plagues_stopping_trade_3 = {
	picture = plague_severe
	global_trade_power = -0.60
	global_trade_goods_size_modifier  = -0.60
}

native_plagues_closing_infrastructure_1 = {
	picture = plague_mild
	global_tax_modifier = -0.05
	manpower_recovery_speed = -0.05
}

native_plagues_closing_infrastructure_2 = {
	picture = plague_moderate
	global_tax_modifier = -0.10
	manpower_recovery_speed = -0.10
}

native_plagues_closing_infrastructure_3 = {
	picture = plague_severe
	global_tax_modifier = -0.15
	manpower_recovery_speed = -0.15
}

native_plagues_quarantines_1 = {
	picture = plague_mild
	global_tax_modifier = -0.05
	global_trade_power = -0.05
	manpower_recovery_speed = -0.05
}

native_plagues_quarantines_2 = {
	picture = plague_moderate
	global_tax_modifier = -0.10
	global_trade_power = -0.10
	manpower_recovery_speed = -0.10
}

native_plagues_quarantines_3 = {
	picture = plague_severe
	global_tax_modifier = -0.15
	global_trade_power = -0.15
	manpower_recovery_speed = -0.15
}