#Culverin (20)

type = artillery

maneuver = 2
offensive_morale = 5
defensive_morale = 1
offensive_fire = 4
defensive_fire = 0
offensive_shock = 0
defensive_shock = 2

trigger = {
	OR = {
		technology_group = western
		technology_group = eastern
		technology_group = turkishtech
		technology_group = muslim
		technology_group = indian
		technology_group = austranesian
	}
	NOT = { has_country_flag = raised_special_units }
}


