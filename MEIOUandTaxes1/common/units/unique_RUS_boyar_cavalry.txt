#Light Cossacks - Boyars (26)

unit_type = eastern
type = cavalry
maneuver = 2

offensive_morale = 5
defensive_morale = 2
offensive_fire = 5 #BONUS
defensive_fire = 2
offensive_shock = 3
defensive_shock = 3

trigger = {
	tag = RUS
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}


