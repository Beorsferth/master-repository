#33 - Volley Fire Infantry

type = infantry
unit_type = western
maneuver = 1

offensive_morale = 2
defensive_morale = 5
offensive_fire = 7
defensive_fire = 4
offensive_shock = 2
defensive_shock = 2

trigger = {
	NOT = { tag = KAL } #Gustavians
	NOT = { culture_group = scandinavian } #Gustavians
	NOT = { has_country_flag = raised_special_units }
}


