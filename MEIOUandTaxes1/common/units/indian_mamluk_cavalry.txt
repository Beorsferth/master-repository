# Mamluk Cavalry (10)

unit_type = indian
type = cavalry
maneuver = 2

#Tech 0
offensive_morale = 2
defensive_morale = 3
offensive_fire = 0
defensive_fire = 0
offensive_shock = 3
defensive_shock = 3

trigger = {
	OR = {
		religion_group = muslim
		religion = sikhism
	}
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}
