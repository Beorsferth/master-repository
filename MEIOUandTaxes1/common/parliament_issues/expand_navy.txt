expand_navy = {
	
	category = 2
	
	allow = {
		num_of_ports = 2
		OR = {
			has_idea_group = naval_ideas
			has_idea_group = trade_ideas
			dip_tech = 20
		}
	}
	
	effect = {
	}
	
	modifier = {
		global_ship_cost = -0.1
		sailors_recovery_speed = 0.1
		navy_tradition = 0.1
	}
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0
			navy_size_percentage = 0.7
		}
		modifier = {
			factor = 0
			NOT = { num_of_ports = 5 }
		}
	}
}