
chinese_gov_trigger = {
	OR = {
		government = chinese_monarchy
		government = chinese_monarchy_2
		government = chinese_monarchy_3
		government = chinese_monarchy_4
		government = chinese_monarchy_5
		government = chinese_horde_1
	}
}

chinese_imperial_gov_trigger = {
	OR = {
		government = chinese_monarchy
		government = chinese_monarchy_2
		government = chinese_monarchy_3
		government = chinese_monarchy_4
		government = chinese_monarchy_5
		government = chinese_horde_1
	}
	has_country_modifier = title_6
}

middle_kingdom_ideology_trigger = {
	OR = {
		government = chinese_monarchy
		government = chinese_monarchy_2
		government = chinese_monarchy_3
		government = chinese_monarchy_4
		government = chinese_monarchy_5
		government = chinese_horde_1
	}
	has_country_modifier = title_6
	OR = {
		culture_group = chinese_group
		has_country_flag = barbarian_claimant_china
	}
	has_factions = yes
	num_of_cities = 70
}

mandate_of_heaven_trigger = {
	OR = {
		government = chinese_monarchy
		government = chinese_monarchy_2
		government = chinese_monarchy_3
		government = chinese_monarchy_4
		government = chinese_monarchy_5
		government = chinese_horde_1
	}
	has_country_modifier = title_6
	OR = {
		culture_group = chinese_group
		has_country_flag = barbarian_claimant_china
	}
	NOT = { technology_group = western }
	stability = 0
	legitimacy = 60
	num_of_cities = 50
	# is_westernising = no
	# NOT = { has_disaster = chinese_dynastic_cycle }
}
