########################
# Trade Post Buildings #
########################

# Trade Post
# Trading Post
# Trading Company

##############
# Trade Post #
##############

trading_post = {
	cost = 100
	time =  12
	
	trigger = {
		has_province_flag = TP_trading_post
		owner = { NOT = { government = native_council } }
	}
	
	modifier = {
		#	local_production_efficiency = 0.15
		province_trade_power_value = 2.5
		province_trade_power_modifier = 0.2
	}
	
	ai_will_do = {
		factor = 0
	}
}

trading_company = {
	#cost = 400
	cost = 200
	time =  24
	
	#make_obsolete = trading_post
	
	trigger = {
		has_building = trading_post
		has_province_flag = TP_trading_post
		owner = { NOT = { government = native_council } }
	}
	
	modifier = {
		#	local_production_efficiency = 0.3
		province_trade_power_value = 5
		province_trade_power_modifier = 0.4
	}
	
	ai_will_do = {
		factor = 1000
	}
}

