########################
# Unique buildings #
########################



##############
# Fortress #
##############

theodosian_walls_b = {
	cost = 9999
	time =  12
	
	trigger = {
	}
	
	modifier = {
		garrison_growth = 0.05
		local_defensiveness = 0.50
		local_tax_modifier = -0.10
		fort_level = 2
	}
	
	ai_will_do = {
		factor = 0
	}
}

danevirke_b = {
	cost = 9999
	time =  12
	
	trigger = {
	}
	
	modifier = {
		garrison_growth = 0.05
		local_hostile_attrition = 1
		local_defensiveness = 0.25
		local_tax_modifier = -0.05
		fort_level = 1
	}
	
	ai_will_do = {
		factor = 0
	}
}

hammershus_b = {
	cost = 9999
	time =  12
	
	trigger = {
	}
	
	modifier = {
		garrison_growth = 0.05
		local_defensiveness = 0.25
		local_tax_modifier = -0.05
		fort_level = 1
		province_trade_power_value = 2.5
	}
	
	ai_will_do = {
		factor = 0
	}
}

bock_b = {
	cost = 9999
	time =  12
	
	trigger = {
	}
	
	modifier = {
		garrison_growth = 0.05
		local_defensiveness = 0.25
		local_tax_modifier = -0.05
		fort_level = 1
	}
	
	ai_will_do = {
		factor = 0
	}
}

dubrovnik_b = {
	cost = 9999
	time =  12
	
	trigger = {
	}
	
	modifier = {
		garrison_growth = 0.05
		local_defensiveness = 0.25
		local_tax_modifier = -0.05
		fort_level = 1
	}
	
	ai_will_do = {
		factor = 0
	}
}

the_gates_b = {
	cost = 9999
	time =  12
	
	trigger = {
	}
	
	modifier = {
		garrison_growth = 0.05
		local_defensiveness = 0.25
		local_tax_modifier = -0.05
		fort_level = 1
		province_trade_power_value = 2.5
	}
	
	ai_will_do = {
		factor = 0
	}
}

ordensburg_marienburg_b = {
	cost = 9999
	time =  12
	
	trigger = {
	}
	
	modifier = {
		province_trade_power_value = 1
		garrison_growth = 0.05
		local_defensiveness = 0.50
		local_tax_modifier = -0.10
		fort_level = 2
	}
	
	ai_will_do = {
		factor = 0
	}
}

aurelian_walls_b = {
	cost = 9999
	time =  12
	
	trigger = {
	}
	
	modifier = {
		garrison_growth = 0.05
		local_defensiveness = 0.25
		local_tax_modifier = -0.05
		fort_level = 1
	}
	
	ai_will_do = {
		factor = 0
	}
}

diocletian_palace_b = {
	cost = 9999
	time =  12
	
	trigger = {
	}
	
	modifier = {
		garrison_growth = 0.05
		local_defensiveness = 0.25
		local_tax_modifier = -0.05
		fort_level = 1
		province_trade_power_value = 1
	}
	
	ai_will_do = {
		factor = 0
	}
}

acropolis_b = {
	cost = 9999
	time =  12
	
	trigger = {
	}
	
	modifier = {
		garrison_growth = 0.05
		local_defensiveness = 0.25
		local_tax_modifier = -0.05
		fort_level = 1
	}
	
	ai_will_do = {
		factor = 0
	}
}

tower_london_b = {
	cost = 9999
	time =  12
	
	trigger = {
	}
	
	modifier = {
		garrison_growth = 0.05
		local_defensiveness = 0.25
		local_tax_modifier = -0.05
		fort_level = 1
	}
	
	ai_will_do = {
		factor = 0
	}
}

bastille_b = {
	cost = 9999
	time =  12
	
	trigger = {
	}
	
	modifier = {
		garrison_growth = 0.05
		local_defensiveness = 0.25
		local_tax_modifier = -0.05
		fort_level = 1
	}
	
	ai_will_do = {
		factor = 0
	}
}