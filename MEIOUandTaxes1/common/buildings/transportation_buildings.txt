############################
# Transportation Buildings #
############################

# Transportation
# Road Network
# Paved Road Network
# High Way Network
# Road and Rail Network

##################
# Transportation #
##################

road_network = {
	cost = 9999
	time =  36
	
	modifier = {
		global_tax_income = -0.25
		local_friendly_movement_speed = 0.5
		local_hostile_movement_speed = 0.25
		province_trade_power_value = 0.25
		supply_limit = 1
	}
	
	trigger = {
		OR = {
			can_build_road_network = yes
			AND = {
				can_keep_road_network = yes # Redundant can_keep so that buildings still get removed if they are disqualifed
				has_building = road_network # has_building = itself so that building never removes itself unless it fails the can_keep
			}
		}
	}
	
	ai_will_do = {
		factor = 0
		
		modifier = {
			factor = 0
			OR = {
				owner = { is_at_war = yes }
				owner = { is_bankrupt = yes }
				OR = {
					AND = {
						is_capital = yes
						owner = { NOT = { check_variable = { which = "wellbeing_composite" value = 1 } } }
					}
					owner = { NOT = { check_variable = { which = "wellbeing_composite" value = 1.05 } } }
				}
			}
		}
		
		modifier = {
			factor = 2
			OR = {
				has_terrain = forest
				has_terrain = hills
				has_terrain = jungle
				has_terrain = marsh
				has_terrain = desert
				has_terrain = taiga
				has_terrain = coastal_desert
			}
		}
		
		modifier = {
			factor = 3
			is_capital = yes
		}
		
		modifier = {
			factor = 0
			NOT = { check_variable = { which = "communication_importance" value = 1 } }
		}
		modifier = {
			factor = 0.25
			NOT = { check_variable = { which = "communication_importance" value = 3 } }
		}
		modifier = {
			factor = 0.5
			NOT = { check_variable = { which = "communication_importance" value = 6 } }
		}
		modifier = {
			factor = 0.75
			NOT = { check_variable = { which = "communication_importance" value = 10 } }
		}
		modifier = {
			factor = 1.25
			check_variable = { which = "communication_importance" value = 15 }
		}
		modifier = {
			factor = 1.5
			check_variable = { which = "communication_importance" value = 21 }
		}
		modifier = {
			factor = 1.75
			check_variable = { which = "communication_importance" value = 28 }
		}
		modifier = {
			factor = 2
			check_variable = { which = "communication_importance" value = 36 }
		}
	}
}

paved_road_network = {
	#cost = 225
	cost = 9999
	time =  36
	
	#make_obsolete = road_network
	
	trigger = {
		OR = {
			can_build_paved_road_network = yes
			AND = {
				can_keep_paved_road_network = yes # Redundant can_keep so that buildings still get removed if they are disqualifed
				has_building = paved_road_network # has_building = itself so that building never removes itself unless it fails the can_keep
			}
		}
	}
	
	modifier = {
		global_tax_income = -1.5
		local_friendly_movement_speed = 1
		local_hostile_movement_speed = 0.5
		province_trade_power_value = 0.5
		supply_limit = 2
	}
	
	ai_will_do = {
		factor = 0
		
		modifier = {
			factor = 0
			OR = {
				owner = { is_at_war = yes }
				owner = { is_bankrupt = yes }
				OR = {
					AND = {
						is_capital = yes
						owner = { NOT = { check_variable = { which = "wellbeing_composite" value = 1 } } }
					}
					owner = { NOT = { check_variable = { which = "wellbeing_composite" value = 1.05 } } }
				}
			}
		}
		
		modifier = {
			factor = 3
			is_capital = yes
		}
		
		modifier = {
			factor = 0
			NOT = { check_variable = { which = "communication_importance" value = 3 } }
		}
		modifier = {
			factor = 0.25
			NOT = { check_variable = { which = "communication_importance" value = 6 } }
		}
		modifier = {
			factor = 0.5
			NOT = { check_variable = { which = "communication_importance" value = 10 } }
		}
		modifier = {
			factor = 0.75
			NOT = { check_variable = { which = "communication_importance" value = 15 } }
		}
		modifier = {
			factor = 1.25
			check_variable = { which = "communication_importance" value = 21 }
		}
		modifier = {
			factor = 1.5
			check_variable = { which = "communication_importance" value = 28 }
		}
		modifier = {
			factor = 1.75
			check_variable = { which = "communication_importance" value = 36 }
		}
		modifier = {
			factor = 2
			check_variable = { which = "communication_importance" value = 45 }
		}
	}
}

highway_network = {
	#cost = 300
	cost = 9999
	time =  36
	
	#make_obsolete = paved_road_network
	
	trigger = {
		OR = {
			can_build_highway_network = yes
			AND = {
				can_keep_highway_network = yes # Redundant can_keep so that buildings still get removed if they are disqualifed
				has_building = highway_network # has_building = itself so that building never removes itself unless it fails the can_keep
			}
		}
	}
	
	modifier = {
		global_tax_income = -2
		local_friendly_movement_speed = 1.5
		local_hostile_movement_speed = 0.75
		province_trade_power_value = 1
		supply_limit = 3
	}
	
	ai_will_do = {
		factor = 0
		
		modifier = {
			factor = 0
			OR = {
				owner = { is_at_war = yes }
				owner = { is_bankrupt = yes }
				OR = {
					AND = {
						is_capital = yes
						owner = { NOT = { check_variable = { which = "wellbeing_composite" value = 1 } } }
					}
					owner = { NOT = { check_variable = { which = "wellbeing_composite" value = 1.05 } } }
				}
			}
		}
		
		modifier = {
			factor = 2
			OR = {
				has_terrain = jungle
				has_terrain = marsh
				has_terrain = taiga
				has_terrain = tundra
				has_terrain = mountain
				has_terrain = desert_mountain
				has_terrain = highlands
			}
		}
		
		modifier = {
			factor = 3
			is_capital = yes
		}
		
		modifier = {
			factor = 0
			NOT = { check_variable = { which = "communication_importance" value = 6 } }
		}
		modifier = {
			factor = 0.25
			NOT = { check_variable = { which = "communication_importance" value = 10 } }
		}
		modifier = {
			factor = 0.5
			NOT = { check_variable = { which = "communication_importance" value = 15 } }
		}
		modifier = {
			factor = 0.75
			NOT = { check_variable = { which = "communication_importance" value = 21 } }
		}
		modifier = {
			factor = 1.25
			check_variable = { which = "communication_importance" value = 28 }
		}
		modifier = {
			factor = 1.5
			check_variable = { which = "communication_importance" value = 36 }
		}
		modifier = {
			factor = 1.75
			check_variable = { which = "communication_importance" value = 45 }
		}
		modifier = {
			factor = 2
			check_variable = { which = "communication_importance" value = 55 }
		}
	}
}

road_and_rail_network = {
	#cost = 450
	cost = 9999
	time =  36
	
	#make_obsolete = highway_network
	
	trigger = {
		OR = {
			can_build_road_and_rail_network = yes
			AND = {
				can_keep_road_and_rail_network = yes # Redundant can_keep so that buildings still get removed if they are disqualifed
				has_building = road_and_rail_network # has_building = itself so that building never removes itself unless it fails the can_keep
			}
		}
	}
	
	modifier = {
		global_tax_income = -3
		local_friendly_movement_speed = 2.5
		local_hostile_movement_speed = 1.25
		province_trade_power_value = 2
		supply_limit = 4
	}
	
	ai_will_do = {
		factor = 0
		
		modifier = {
			factor = 0
			OR = {
				owner = { is_at_war = yes }
				owner = { is_bankrupt = yes }
				OR = {
					AND = {
						is_capital = yes
						owner = { NOT = { check_variable = { which = "wellbeing_composite" value = 1 } } }
					}
					owner = { NOT = { check_variable = { which = "wellbeing_composite" value = 1.05 } } }
				}
			}
		}
		
		modifier = {
			factor = 3
			is_capital = yes
		}
		
		modifier = {
			factor = 0
			NOT = { check_variable = { which = "communication_importance" value = 10 } }
		}
		modifier = {
			factor = 0.25
			NOT = { check_variable = { which = "communication_importance" value = 15 } }
		}
		modifier = {
			factor = 0.5
			NOT = { check_variable = { which = "communication_importance" value = 21 } }
		}
		modifier = {
			factor = 0.75
			NOT = { check_variable = { which = "communication_importance" value = 28 } }
		}
		modifier = {
			factor = 1.25
			check_variable = { which = "communication_importance" value = 36 }
		}
		modifier = {
			factor = 1.5
			check_variable = { which = "communication_importance" value = 45 }
		}
		modifier = {
			factor = 1.75
			check_variable = { which = "communication_importance" value = 55 }
		}
		modifier = {
			factor = 2
			check_variable = { which = "communication_importance" value = 66 }
		}
	}
}

upgrade_roads_build = {
	
	cost = 0
	time =  0
	
	trigger = {
		OR = {
			has_building = upgrade_roads_build
			OR = {
				can_build_road_network = yes
				can_build_paved_road_network = yes
				can_build_highway_network = yes
				can_build_road_and_rail_network = yes
			}
		}
	}
	
	modifier = {
	}
	
	ai_will_do = {
		factor = 0
	}
}