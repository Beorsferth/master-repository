#Country Name: Please see filename.

graphical_culture = easterngfx

color = { 93  54  87 } # Dark variation of Byzantium colour

historical_idea_groups = {
	innovativeness_ideas
	diplomatic_ideas
	administrative_ideas
	logistic_ideas
	spy_ideas
	economic_ideas
	quality_ideas
	expansion_ideas
	trade_ideas
}

historical_units = { #Greek group
	eastern_toxotai_infantry
	eastern_horse_archer_cavalry
	eastern_skoutatoi_infantry
	eastern_pronoia_cavalry
	eastern_latinikon_infantry
	eastern_deli_cavalry
	eastern_draby_infantry
	eastern_pomeste_cavalry
	eastern_drabant_infantry
	eastern_light_cossack_cavalry
	eastern_slavic_western_infantry
	eastern_reiter_cavalry
	eastern_regular_infantry
	eastern_armeblanche_cavalry
	eastern_bayonet_infantry
	eastern_uhlan_cavalry
	eastern_drill_infantry
	eastern_columnar_infantry
	eastern_lancer_cavalry
	eastern_breech_infantry
}


monarch_names = {
	"John #0" = 20
	"Basil #0" = 10
	"Constantine #0" = 20
	"Alexius #0" = 45
	"Andronicus #0" = 10
	"Nikephoros #0" = 5
	"Michael #0" = 10
	"Leo #0" = 45
	"Manuel #0" = 5
	"Zoe #0" = -5
	"Georgios #0" = 1
	"Aineias #0" = 1
	"Demetrius #0" = 1
	"Thomas #0" = 1
	"Andreas #0" = 1
	"Skantarios #0" = 1
	"David #0" = 1
	"Alexandros #0" = 1
	"Ambrosios #0" = 1
	"Bartholomaios #0" = 1
	"Chrysanthos #0" = 1
	"Diogenes #0" = 1
	"Dionysos #0" = 1
	"Eirenaios #0" = 1
	"Elpidios #0" = 1
	"Gregorios #0" = 1
	"Hektor #0" = 1
	"Herakles #0" = 1
	"Iason #0" = 1
	"Kastor #0" = 1
	"Matthaios #0" = 1
	"Nestor #0" = 1
	"Nikolaos #0" = 1
	"Nikomedes #0" = 1
	"Orestes #0" = 1
	"Palaemon #0" = 1
	"Pavlos #0" = 1
	"Pelagios #0" = 1
	"Petros #0" = 1
	"Philemon #0" = 1
	"Philippos #0" = 1
	"Prokopios #0" = 1
	"Stefanos #0" = 1
	"Theodoros #0" = 1
	
	"Eirene #1" = -20
	"Theodora #1" = -20
	"Zoe #1" = -10
	"Alexandra #0" = -10
	"Antonia #0" = -5
	"Anna #0" = -5
	"Artemisia #0" = -1
	"Eirene #0" = -1
	"Euphrosyne #0" = -1
	"Laskarina #0" = -1
	"Manto #0" = -1
	"Thalia #0" = -1
	"Sevasti #0" = -1
	"Sophia #0" = -1
}

leader_names = {
	Argyrus Artavasdus
	Basileus
	Diogenes Ducas
	Monomachus
	Romanus
	Stauracius Stratioticus
	Vataces
	Zimisces
	Kommenus
	Paleologus
}

ship_names = {
	Hydra
	Lesbos Limnos
	Mikonios
	Pezopoulos
	Salamis
	Samos
	Simitzopoulos
	Themistoklis
	Votsis
}
