#AVIALABLE CUSTOM LOCALISATION KEYS
#When adding new custom loc keys, add them to the appropriate category (or add a new one) and describe them. Name the strings "String_X" where x is its content.
# To not display anything, you can use 'localisation_key = ""'
# Please add one example sentence for each key.

#As this list grow organize with categories.
#Current loc keys in this file:
#GetTributaryOverlord
#GetOldEra

### Titular for Overlord
#Ex: "On a mission to visit the Son of Heaven"
defined_text = {
	name = GetTributaryOverlord
	
	text = {
		localisation_key = string_son_of_heaven
		trigger = {
			is_emperor_of_china = yes
			is_female = no
		}
	}
	text = {
		localisation_key = string_daughter_of_heaven
		trigger = {
			is_emperor_of_china = yes
			is_female = yes
		}
	}
	text = {
		localisation_key = string_tributary_overlord_title
		trigger = {
			is_emperor_of_china = no
		}
	}
}

#Old Era
#Ex: "A Medieval Manuscript"
defined_text = {
	name = GetOldEra
	
	text = {
		localisation_key = string_song_era
		trigger = {
			capital_scope = {
				superregion = china_superregion
			}
		}
	}
	text = {
		localisation_key = string_middle_ages
		trigger = {
			capital_scope = {
				continent = europe
			}
		}
	}
	text = {
		localisation_key = string_pre_islamic
		trigger = {
			capital_scope = {
				religion_group = muslim
			}
		}
	}
	text = {
		localisation_key = string_vedic
		trigger = {
			capital_scope = {
				religion_group = dharmic
			}
		}
	}
	text = {
		localisation_key = string_ancient_era
		trigger = {
			NOT = {
				capital_scope = {
					NOT = { continent = europe }
					NOT = { superregion = china_superregion }
					NOT = { religion_group = muslim }
					NOT = { religion_group = dharmic }
				}
			}
		}
	}
}

#Low Level Official
#Ex: "A local Mandarin immediately reacted to the..."
defined_text = {
	name = GetLowLevelOfficialForCountry
	
	text = {
		localisation_key = string_mandarin
		trigger = {
			culture_group = east_asian #Chinese
		}
	}
	text = {
		localisation_key = string_zamindar
		trigger = {
			religion_group = muslim
			OR = {
				culture_group = eastern_aryan
				culture_group = hindusthani
				culture_group = western_aryan
				culture_group = dravidian
				culture_group = central_indic
			}
		}
	}
	text = {
		localisation_key = string_bey
		trigger = {
			religion_group = muslim
			NOT = {
				culture_group = eastern_aryan
				culture_group = hindusthani
				culture_group = western_aryan
				culture_group = dravidian
				culture_group = central_indic
			}
		}
	}
	text = {
		localisation_key = string_polygar
		trigger = {
			NOT = { religion_group = muslim }
			culture_group = dravidian
		}
	}
	text = {
		localisation_key = string_thakur
		trigger = {
			NOT = { religion_group = muslim }
			OR = {
				culture_group = eastern_aryan
				culture_group = hindusthani
				culture_group = western_aryan
				culture_group = central_indic
			}
		}
	}
	text = {
		localisation_key = string_zupan
		trigger = {
			culture_group = south_slavic
		}
	}
	text = {
		localisation_key = string_official
		trigger = {
			NOT = { religion_group = muslim }
			NOT = {
				culture_group = east_asian
				culture_group = eastern_aryan
				culture_group = hindusthani
				culture_group = western_aryan
				culture_group = dravidian
				culture_group = central_indic
				culture_group = south_slavic
			}
		}
	}
}

#What Education level do we have
defined_text = {
	name = GetEducationLevel
	
	text = {
		localisation_key = education_level_illiterate
		trigger = {
			has_country_modifier = education_level_illiterate
		}
	}
	
	text = {
		localisation_key = education_level_mediocre
		trigger = {
			has_country_modifier = education_level_mediocre
		}
	}
	
	text = {
		localisation_key = education_level_poor
		trigger = {
			has_country_modifier = education_level_poor
		}
	}
	
	text = {
		localisation_key = education_level_average
		trigger = {
			has_country_modifier = education_level_average
		}
	}
	
	text = {
		localisation_key = education_level_fair
		trigger = {
			has_country_modifier = education_level_fair
		}
	}
	
	text = {
		localisation_key = education_level_good
		trigger = {
			has_country_modifier = education_level_good
		}
	}
	
	text = {
		localisation_key = education_level_high
		trigger = {
			has_country_modifier = education_level_high
		}
	}
	
	text = {
		localisation_key = education_level_exceptional
		trigger = {
			has_country_modifier = education_level_exceptional
		}
	}
	
	text = {
		localisation_key = education_level_enlightened
		trigger = {
			has_country_modifier = education_level_enlightened
		}
	}
}

#What Court level do we have
defined_text = {
	name = GetCourtLevel
	
	text = {
		localisation_key = court_level_1
		trigger = {
			has_country_modifier = court_level_1
		}
	}
	
	text = {
		localisation_key = court_level_2
		trigger = {
			has_country_modifier = court_level_2
		}
	}
	
	text = {
		localisation_key = court_level_3
		trigger = {
			has_country_modifier = court_level_3
		}
	}
	
	text = {
		localisation_key = court_level_4
		trigger = {
			has_country_modifier = court_level_4
		}
	}
	
	text = {
		localisation_key = court_level_5
		trigger = {
			has_country_modifier = court_level_5
		}
	}
	
	text = {
		localisation_key = court_level_6
		trigger = {
			has_country_modifier = court_level_6
		}
	}
}