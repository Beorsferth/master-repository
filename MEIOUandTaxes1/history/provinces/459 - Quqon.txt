# 459 - Quqon (Kokhand)

owner = MGH
controller = MGH
add_core = MGH

capital = "Borotala"
trade_goods = wool
culture = chaghatai
religion = sunni

hre = no

base_tax = 4
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = steppestech

450.1.1 = {
	set_province_flag = tribals_control_province
}
1375.1.1 = {
	owner = YUA
	controller = YUA
	remove_core = MGH
	culture = oirats
	add_core = OIR
}
1392.1.1 = {
	owner = OIR
	controller = OIR
}
1465.1.1 = {
	owner = KZH
	controller = KZH
	add_core = KZH
	remove_core = CHG
}
1501.1.1 = {
	base_tax = 5
}
1530.1.1 = {
	owner = OIR
	controller = OIR
	add_core = OIR
}
1622.1.1 = {
	discovered_by = RUS
}
1635.1.1 = {
	owner = ZUN
	controller = ZUN
	add_core = ZUN
}
1755.1.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
}
