# V�sterbotten
# MEIOU - Gigau

owner = SWE
controller = SWE
add_core = SWE

capital = "Ume�"
trade_goods = lumber
culture = swedish
religion = catholic

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = eastern
discovered_by = western

1200.1.1 = {
	set_province_flag = freeholders_control_province
}
1524.1.1 = {
	shipyard = yes
}
1527.6.1 = {
	religion = protestant
}
1529.12.17 = {
	merchant_guild = yes
}
