# 2240 - Cotentin (Cherbourg)

owner = FRA
controller = FRA

capital = "Chirebourc"
trade_goods = livestock
culture = normand
religion = catholic

hre = no

base_tax = 25
base_production = 1
base_manpower = 1

is_city = yes
local_fortification_1 = yes
temple = yes
harbour_infrastructure_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1354.1.1 = {
	owner = NAV
	controller = NAV
	add_core = NAV
	add_core = FRA
	add_core = NRM
} # Jean le Bon gives Cherbourg to Charles le Mauvais
1369.1.1 = {
	fort_14th = yes
}
1378.4.20 = {
	owner = FRA
	controller = FRA
} # Public trial of Jacques de Rue for regicide and treason. Charles V moves against Charles le Mauvais
1419.1.19 = {
	owner = ENG
	controller = ENG
}
1429.7.17 = {
	owner = ENG
	controller = ENG
}
1434.12.1 = {
	owner = FRA
	controller = FRA
}
1475.8.29 = {
	remove_core = ENG
} # Treaty of Picquigny, ending the Hundred Year War
1520.5.5 = {
	base_tax = 28
	base_production = 2
	base_manpower = 2
}
1588.12.1 = {
	unrest = 5
} # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
1594.1.1 = {
	unrest = 0
} # 'Paris vaut bien une messe!', Henri converts to Catholicism
1631.1.1 = {
	unrest = 3
} # Region is restless
1633.1.1 = {
	unrest = 0
}
1639.1.1 = {
	unrest = 3
}
1641.1.1 = {
	unrest = 0
}
1650.1.1 = {
	fort_14th = no
	fort_15th = yes
}
# Colbert
1760.1.1 = {
	fort_15th = no
	fort_16th = yes
}
1793.3.7 = { } # Guerres de l'Ouest
1796.12.23 = { } # The last rebels are defeated at the battle of Savenay
1799.10.15 = { } # Guerres de l'Ouest
