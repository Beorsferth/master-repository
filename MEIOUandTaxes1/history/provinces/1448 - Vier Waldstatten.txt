# 1448 - Vier Waldstätten
# schwitz uri unterwald luzern

owner = SWI
controller = SWI
add_core = SWI

capital = "Luzern"
trade_goods = cheese
culture = high_alemanisch
religion = catholic

hre = yes

base_tax = 5
base_production = 0
base_manpower = 0

is_city = yes
road_network = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim

1356.1.1 = {
	set_province_flag = freeholders_control_province
}
1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 7
	base_production = 0
	base_manpower = 0
}
1648.10.24 = {
	hre = no
} # Treaty of Westphalia, ending the Thirty Years' War
1798.3.5 = {
	controller = FRA
} # French occupation
1798.4.12 = {
	controller = SWI
} # The establishment of the Helvetic Republic
1798.4.15 = {
	controller = REB
} # The Nidwalden Revolt
1798.9.1 = {
	controller = SWI
} # The revolt is supressed
1802.6.1 = {
	controller = REB
} # Swiss rebellion
1802.9.18 = {
	controller = SWI
}
