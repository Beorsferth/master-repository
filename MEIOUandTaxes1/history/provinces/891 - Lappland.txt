# 891 - Lappland

capital = "Lappland"
trade_goods = subsistence
culture = sapmi
religion = tengri_pagan_reformed

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 5
native_ferocity = 2
native_hostileness = 1

discovered_by = western

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio value = 68 }
	add_permanent_province_modifier = {
		name = "fur_low"
		duration = -1
	}
	set_province_flag = sami_province
#	set_variable = { which = Birkarl_Settlers value = 1 }
#	set_variable = { which = Pomor_Settlers value = 1 }
#	set_variable = { which = Norse_Settlers value = 1 }
#	set_variable = { which = Sami_Natives value = 100 }
}
1640.1.1 = {
	owner = SWE
	controller = SWE
	add_core = SWE
	citysize = 200
	trade_goods = livestock
	discovered_by = western
	discovered_by = eastern
	set_province_flag = trade_good_set
} # The border vs Norway was set earlier but at this point colonialism had also started
1640.1.2 = {
	culture = swedish
	religion = protestant
}
1650.1.1 = {
	trade_goods = iron
	citysize = 320
}
1700.1.1 = {
	citysize = 430
}
1740.1.1 = {
	fort_14th = yes
}
1750.1.1 = {
	citysize = 570
}
1800.1.1 = {
	fort_14th = yes
	citysize = 600
}
