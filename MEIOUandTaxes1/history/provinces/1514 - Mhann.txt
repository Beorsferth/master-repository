# 1514 - Isle of Mann

owner = NOR
controller = NOR

capital = "Balley Chashtal"
trade_goods = iron
culture = highland_scottish
religion = catholic

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern

1266.1.1 = {
	owner = TLI
	controller = TLI
	add_core = TLI
	add_core = ENG
}
1333.8.9 = {
	owner = ENG
	controller = ENG
}
1399.10.19 = {
	remove_core = TLI
	remove_core = SCO
}
1560.8.1 = {
	religion = reformed
}
1643.1.1 = {
	unrest = 4
}
1644.1.1 = {
	unrest = 0
}
1651.10.1 = {
	controller = REB
}
1652.1.1 = {
	controller = ENG
}
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
