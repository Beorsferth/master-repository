# 250 - Fife

owner = SCO
controller = SCO
add_core = SCO

capital = "Aiberdeen"
trade_goods = wheat
culture = lowland_scottish #?
religion = catholic

hre = no

base_tax = 5
base_production = 0
base_manpower = 1

is_city = yes
harbour_infrastructure_1 = yes
temple = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern

450.1.1 = {
	set_province_flag = has_estuary
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "aberdeen_natural_harbour"
		duration = -1
	}
}
1495.1.1 = {
	small_university = yes
}
1520.5.5 = {
	base_tax = 7
	base_production = 0
	base_manpower = 1
}
1560.8.1 = {
	religion = reformed
}
#1600.1.1 = {
#	fort_14th = yes
#} #marketplace Estimated
1644.9.1 = {
	controller = REB
}
1645.9.13 = {
	controller = SCO
} #Battle of Philiphaugh
1651.8.2 = {
	controller = ENG
}
1652.4.21 = {
	controller = SCO
} #Union of Scotland and the COmmonwealth
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
}
