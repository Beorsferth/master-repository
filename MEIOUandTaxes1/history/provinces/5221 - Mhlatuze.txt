# 5221 - Mhlatuze

capital = "Mhlatuze"
trade_goods = unknown
culture = zulu
religion = animism

hre = no

base_tax = 2
base_production = 0
base_manpower = 0

native_size = 5
native_ferocity = 0.5
native_hostileness = 0

450.1.1 = {
	set_province_flag = tribals_control_province
	set_province_flag = has_natural_harbour
	set_province_flag = has_great_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "natal_natural_harbour"
		duration = -1
	}
}
1356.1.1 = {
	add_permanent_province_modifier = {
		name = trading_post_province
		duration = -1
	}
}
1488.1.1 = {
	discovered_by = POR
} # Bartolomeu Dias
