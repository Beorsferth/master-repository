#1351 - Landshut

owner = BAX
controller = BAX
add_core = BAX

capital = "Landshut"
trade_goods = wheat # leather #lederhosen
culture = bavarian
religion = catholic

hre = yes

base_tax = 8
base_production = 1
base_manpower = 1

is_city = yes
local_fortification_1 = yes
town_hall = yes
workshop = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_leather
		duration = -1
	}
}
1340.12.20 = {
	owner = BAV
	controller = BAV
	add_core = BAV
	remove_core = BAX
}
1349.1.1 = {
	owner = BAX
	controller = BAX
	add_core = BAX
	remove_core = BAV
}
1453.1.1 = {
	fort_14th = no
	fort_15th = yes
}
1500.1.1 = {
	road_network = yes
}
1505.7.30 = {
	owner = BAV
	controller = BAV
	add_core = BAV
	remove_core = BAX
} # Diet of Cologne
1520.5.5 = {
	base_tax = 10
	base_production = 1
	base_manpower = 1
}
1704.8.13 = {
	controller = HAB
	owner = HAB
} # Bavaria is conquered by the Emperor, is however 10 years later restored for the balance of power
1714.9.7 = {
	controller = BAV
	owner = BAV
}
1742.2.1 = {
	controller = HAB
} # Austrian troops take Munich and Bavaria
1745.1.1 = {
	controller = BAV
} # Against Peace and their vote in the Emperor election Bavaria is returned to the Wittelsbachs
1777.1.1 = {
	unrest = 3
} # Bavarian line of the Wittlelsbach family dies out and is replaced by the Pwesternate branch of the family. The people in Munich hate the new duke.
1779.1.1 = {
	unrest = 0
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
