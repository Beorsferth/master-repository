owner = AUG
controller = AUG
add_core = AUG

capital = "Augsburg"
trade_goods = wheat # linen
culture = schwabisch
religion = catholic

hre = yes

base_tax = 1
base_production = 2 # Fugger and all
base_manpower = 0

is_city = yes
local_fortification_1 = yes
road_network = yes
corporation_guild = yes
marketplace = yes
town_hall = yes
temple = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_steel
		duration = -1
	}
}
1500.1.1 = {
	fort_14th = yes
}
1501.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 1
	base_production = 3
	base_manpower = 0
}
1530.1.1 = {
	religion = protestant
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1620.1.1 = {
	fort_14th = yes
}
1630.1.1 = {
	controller = SWE
}
1632.1.1 = {
	controller = AUG
}
1806.1.1 = {
	owner = BAV
	controller = BAV
	add_core = BAV
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
