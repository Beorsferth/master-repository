# 4005 - Decapolis

owner = DCP
controller = DCP
add_core = DCP

capital = "H�wenau"
trade_goods = wine
culture = rhine_alemanisch
religion = catholic

hre = yes

base_tax = 7
base_production = 1
base_manpower = 0

is_city = yes
marketplace = yes
town_hall = yes
workshop = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 7
	base_production = 2
	base_manpower = 0
}
1648.10.24 = {
	add_core = FRA
	owner = FRA
	controller = FRA
}
1679.1.26 = {
	hre = no
}
