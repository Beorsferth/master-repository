# 1551 - Bida

owner = NUP
controller = NUP
add_core = NUP

capital = "Bida"
trade_goods = millet
culture = nupe
religion = west_african_pagan_reformed

hre = no

base_tax = 6
base_production = 3
base_manpower = 1

is_city = yes
urban_infrastructure_2 = yes
marketplace = yes
workshop = yes
local_fortification_1 = yes

discovered_by = soudantech
discovered_by = sub_saharan

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 67 }
}
1520.1.1 = {
	base_tax = 13
	base_production = 4
}