# 2289 - Kai
# GG/LS - Japanese Civil War

owner = TKD
controller = TKD
add_core = TKD

capital = "Kofu"
trade_goods = iron
culture = chubu
religion = mahayana #shinbutsu

hre = no

base_tax = 11
base_production = 0
base_manpower = 1

is_city = yes

discovered_by = chinese

1501.1.1 = {
	base_tax = 17
	base_production = 2
	base_manpower = 2
}
1582.1.1 = {
	owner = TGW
	controller = TGW
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
