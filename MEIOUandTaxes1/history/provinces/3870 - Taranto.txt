# 3870 - Taranto

owner = KNP
controller = KNP
add_core = KNP

capital = "Taranto"
trade_goods = olive
culture = neapolitan
religion = catholic

hre = no

base_tax = 4
base_production = 1
base_manpower = 0

is_city = yes
local_fortification_1 = yes
road_network = yes
harbour_infrastructure_1 = yes
town_hall = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

add_cardinal = yes

1300.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_great_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "taranto_large_natural_harbor"
		duration = -1
	}
	add_permanent_province_modifier = {
		name = "principality_of_taranto"
		duration = -1
	}
}
1356.1.1 = {
	add_core = ANJ
}
1442.1.1 = {
	add_core = ARA
}
1495.2.22 = {
	controller = FRA
} # Charles VIII invades Naples
1495.7.7 = {
	controller = KNP
} # Charles VIII leaves Italy
1502.1.1 = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # France and Aragon partitions Naples
1503.6.1 = {
	owner = ARA
	controller = ARA
	add_core = ARA
	remove_core = FRA
	remove_core = ANJ
} # France forced to withdraw
1504.9.22 = {
	remove_core = FRA
} # The Treaty of Blois in 1504
1516.1.23 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	remove_core = ARA
	road_network = no
	paved_road_network = yes
} # Unification of Spain
1520.5.5 = {
	base_tax = 4
	base_production = 2
	base_manpower = 0
}
1530.1.1 = {
	owner = KNP
	controller = KNP
	remove_core = SPA
}
1531.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
}
1620.1.1 = {
	marketplace = yes
}
1714.3.7 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = SPA
}
1734.6.2 = {
	owner = NAP
	controller = NAP
	add_core = NAP
	remove_core = HAB
}
#1815.5.3 = {
#	owner = KNP
#	controller = KNP
#	remove_core = NAP
#}
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
}
