# 1048 - Udskoye

owner = MNA
controller = MNA
add_core = MNA

capital = "Jomgi"
trade_goods = fur
culture = jurchen
religion = tengri_pagan_reformed

hre = no

base_tax = 2
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = chinese
discovered_by = steppestech

450.1.1 = {
	set_province_flag = tribals_control_province
}
1530.1.1 = {
	marketplace = yes
}
1643.1.1 = {
	discovered_by = RUS
	owner = RUS
	controller = RUS
	religion = orthodox
	culture = russian
} # Founded by Pyotr Beketov
1668.1.1 = {
	add_core = RUS
}
1689.8.27 = {
	discovered_by = QNG
	owner = QNG
	controller = QNG
	add_core = QNG
	culture = manchu
} # Treaty of Nerchinsk
1858.1.1 = {
	owner = RUS
	controller = RUS
	religion = orthodox
	culture = russian
} # Treaty of Aigun
