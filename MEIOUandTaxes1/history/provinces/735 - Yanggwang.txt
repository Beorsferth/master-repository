# 735 - Yanggwang
# FL - Korea Universalis
# LS - alpha 5

owner = KOR
controller = KOR
add_core = KOR

capital = "Gwangju"
trade_goods = rice
culture = korean
religion = mahayana #FB-ASSA become confucianism in 1392

hre = no

base_tax = 22
base_production = 1
base_manpower = 1

is_city = yes
town_hall = yes
harbour_infrastructure_1 = yes
road_network = yes

discovered_by = chinese
discovered_by = steppestech

450.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_great_natural_harbour
	set_province_flag = has_estuary
	set_province_flag = great_natural_place
	add_permanent_province_modifier = {
		name = "gyeonggi_large_natural_harbor"
		duration = -1
	}
}
500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_chinaware
		duration = -1
	}
}
1369.3.17 = {
	road_network = no
	paved_road_network = yes
}
1392.6.5 = {
	religion = confucianism
	owner = JOS
	controller = JOS
	add_core = JOS
	remove_core = KOR
	base_production = 3
	town_hall = no
	harbour_infrastructure_1 = no
	urban_infrastructure_1 = yes
	harbour_infrastructure_2 = yes
	marketplace = yes
	workshop = yes
	art_corporation = yes
	small_university = yes
	fort_14th = yes
}
1444.1.1 = {
	base_tax = 40
	base_production = 13
}
1520.5.5 = {
	base_tax = 70
	base_production = 13
	base_manpower = 4
}
1592.4.24 = {
	controller = ODA
} # Japanese invasion
1593.1.1 = {
	controller = JOS
} # With the help of Chinese troops the Japanese are driven back
1637.1.1 = {
	add_core = MNG
} # Tributary of Qing China
1644.1.1 = {
	add_core = QNG
	remove_core = MNG
} # Part of the Manchu empire
1653.1.1 = {
	discovered_by = NED
} # Hendrick Hamel
