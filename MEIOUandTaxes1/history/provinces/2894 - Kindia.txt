# 2894 - Kindia

capital = "Kindia"
trade_goods = millet
culture = temne
religion = west_african_pagan_reformed

base_tax = 5
base_production = 0
base_manpower = 0

native_size = 7
native_ferocity = 6
native_hostileness = 7

discovered_by = sub_saharan

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 84 }
}
1530.1.1 = {
	owner = MNE
	controller = MNE
	add_core = MNE
	is_city = yes
	trade_goods = palm
}
