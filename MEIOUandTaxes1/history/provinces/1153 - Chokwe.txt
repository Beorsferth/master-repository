#1153 - Sheba

owner = CKW
controller = CKW
add_core = CKW

capital = "Kalemia"
trade_goods = millet
culture = chokwe
religion = sunni

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = central_african

450.1.1 = {
	set_province_flag = tribals_control_province
	add_permanent_province_modifier = {
		name = "ivory_medium"
		duration = -1
	}
}
