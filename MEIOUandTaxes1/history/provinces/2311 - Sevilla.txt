# 2311 - Sevilla + Utrera + Cazalla de la Sierra + Alcal� de Guadaira + Guadalcanal + Constantina

owner = CAS #Juan II of Castille
controller = CAS
add_core = CAS

capital = "Sevilla"
trade_goods = wheat #silk
culture = andalucian # culture = western_andalucian
religion = catholic

hre = no

base_tax = 8
base_production = 8
base_manpower = 2

is_city = yes
merchant_guild = yes
road_network = yes
urban_infrastructure_2 = yes
harbour_infrastructure_2 = yes
workshop = yes
warehouse = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech

450.1.1 = {
	set_province_flag = has_estuary
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = great_natural_place
	add_permanent_province_modifier = {
		name = guadalquivir_estuary_modifier
		duration = -1
	}
}
500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_silk
		duration = -1
	}
}
1356.1.1 = {
	set_province_flag = spanish_name
	add_core = ENR
	add_permanent_province_modifier = {
		name = "lordship_of_sevilla"
		duration = -1
	}
}
1369.3.23 = {
	remove_core = ENR
}
1500.3.3 = {
	base_tax = 7
	base_production = 12
	base_manpower = 2
}
### 1503.1.1 = {
#} # The "Casa de la Contrataci�n" is established in Sevilla as the monarchy tries to control American trade through that port.
1505.1.1 = {
	small_university = yes
}
1506.1.1 = {
	temple = yes
}
1516.1.23 = {
	controller = SPA
	owner = SPA
	add_core = SPA
	road_network = no
	paved_road_network = yes
	military_harbour_1 = yes
	fort_14th = yes
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille
1610.1.12 = { } # Decree for the expulsion of the morisques in Andaluc�a, which is speedily and uneventfully performed
1713.4.11 = {
	remove_core = CAS
}
1808.6.6 = {
	controller = REB
}
1813.12.11 = {
	controller = SPA
}
