# 227 - Ja�n + And�jar + Alcal� la Real + Martos + Arjona + Alcaudete + Torredonjimeno + Porcuna

owner = CAS #Juan II of Castille
controller = CAS

capital = "Ja�n"
trade_goods = olive
culture = andalucian # culture = eastern_andalucian
religion = catholic

hre = no

base_tax = 7
base_production = 3
base_manpower = 1

is_city = yes
marketplace = yes
urban_infrastructure_1 = yes
corporation_guild = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = turkishtech

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_silk
		duration = -1
	}
}
1356.1.1 = {
	add_core = CAS
	add_core = ENR
	set_province_flag = spanish_name
	add_permanent_province_modifier = {
		name = "lordship_of_jaen"
		duration = -1
	}
}
1369.3.23 = {
	remove_core = ENR
}
1400.1.1 = {
	fort_14th = yes
}
1500.1.1 = {
	temple = yes
}
1500.3.3 = {
	base_tax = 2
	base_production = 10
	base_manpower = 1
}
1516.1.23 = {
	controller = SPA
	owner = SPA
	add_core = SPA
	road_network = no
	paved_road_network = yes
} # King Fernando dies, Carlos inherits Aragon and becomes co-regent of Castilla
1570.10.28 = { } # To quell the revolt, the morisques in Granada are forcefully deported to other Spanish territories

1713.4.11 = {
	remove_core = CAS
}
1808.6.6 = {
	controller = REB
}
1809.1.1 = {
	controller = SPA
}
1812.10.1 = {
	controller = REB
}
1813.12.11 = {
	controller = SPA
}
