# No previous file for Havelland

owner = BRA
controller = BRA
add_core = BRA

capital = "Brandenburg an der Havel"
trade_goods = livestock
culture = low_saxon
religion = catholic

hre = yes

base_tax = 12
base_production = 1
base_manpower = 1

is_city = yes
town_hall = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 15
	base_production = 0
	base_manpower = 1
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1539.1.1 = {
	religion = protestant
}
1650.1.1 = {
	culture = prussian
}
1701.1.18 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = BRA
} # Friedrich III becomes king in Prussia
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1806.10.27 = {
	controller = FRA
}
1807.7.9 = {
	controller = PRU
} # The Second treaty of Tilsit
