# No previous file for Niitsitapi

capital = "Niitsitapi"
trade_goods = unknown
culture = blackfoot
religion = totemism

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

native_size = 15
native_ferocity = 3
native_hostileness = 4

450.1.1 = {
	set_province_flag = tribals_control_province
}
1760.1.1 = {
	owner = BLA
	controller = BLA
	add_core = BLA
	trade_goods = fur
	is_city = yes
} # Great Plain tribes spread over vast territories
1793.1.1 = {
	owner = GBR
	controller = GBR
	citysize = 500
	culture = english
	religion = protestant
	trade_goods = fur
} # Fort St John, first western settlement in modern British Columbia
1818.1.1 = {
	add_core = GBR
}
