# 3370 - Tudela

owner = NAV
controller = NAV
add_core = NAV

capital = "Tudela" #Tutera in euskara(basque)
trade_goods = wheat
culture = basque
religion = catholic

hre = no

base_tax = 2
base_production = 1
base_manpower = 0

is_city = yes
temple = yes
town_hall = yes
local_fortification_1 = yes
# road_network = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1483.1.29 = {
	unrest = 5
} # Civil War between the Beaumont and Agramont parties in Navarra
1485.2.5 = {
	unrest = 0
} # Peace of Pau between the two parties
1512.7.24 = {
	controller = ARA
	owner = ARA
	add_core = ARA
} # Navarra's alliance with France gives Fernando the perfect excuse to invade the kingdom.
1515.7.7 = {
	controller = CAS
	owner = CAS
	add_core = CAS
	remove_core = ARA
} # Navarra's formally integrated into Castilla
1516.1.23 = {
	controller = SPA
	owner = SPA
	add_core = SPA
	remove_core = CAS
} # King Fernando dies, Carlos inherits Aragon and becomes co-regent of Castille
1521.5.1 = {
	controller = FRA
} # Exploiting the turmoil provoked by the revolt of the Comunidades, France attacks the unprotected Navarrese border.
1521.6.30 = {
	controller = SPA
} # French troops are defeated at the battle of Quir�s
1600.1.1 = {
	fort_14th = yes
}
1713.4.11 = {
	remove_core = NAV
}
1808.6.6 = {
	controller = REB
}
1808.12.1 = {
	controller = SPA
}
1813.6.21 = {
	controller = REB
}
1813.12.11 = {
	controller = SPA
}
