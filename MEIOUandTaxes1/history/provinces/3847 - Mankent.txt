# 3847 - Mankent

owner = MGH
controller = MGH
capital = "Mankent"
trade_goods = wool
culture = chaghatai
religion = sunni
base_tax = 4
base_production = 0
base_manpower = 0
is_city = yes

discovered_by = steppestech
discovered_by = turkishtech
discovered_by = muslim

450.1.1 = {
	set_province_flag = tribals_control_province
}
1356.1.1 = {
	discovered_by = KSH
	add_core = MGH
}
1370.4.1 = {
	owner = TIM
	controller = TIM
	add_core = TIM
	remove_core = MGH
}
1444.1.1 = {
	add_core = SHY
}
1446.1.1 = {
	owner = SHY
	controller = SHY
	culture = uzbehk
	add_core = SHY
	remove_core = TIM
	remove_core = GOL
}
#1465.1.1 = {
#	owner = KZH
#	controller = KZH
#}
1501.1.1 = {
	base_tax = 5
}
1502.1.1 = {
	owner = SHY
	controller = SHY
}
1520.1.1 = {
	owner = BUK
	controller = BUK
	add_core = BUK
	remove_core = SHY
} # Emirate of Bukhara established
1709.1.1 = {
	owner = KOK
	controller = KOK
	add_core = KOK
	remove_core = BUK
}
