# 368 - Madeira

capital = "Madeira"
trade_goods = sugar
culture = portugese
religion = catholic

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 0
native_ferocity = 0
native_hostileness = 0

1419.1.1 = {
	discovered_by = POR
	owner = POR
	controller = POR
	citysize = 500
	is_city = yes
	capital = "Porto Santo"
}
1444.1.1 = {
	add_core = POR
}
1493.1.1 = {
	temple = yes
}
1497.1.1 = {
	capital = "Funchal"
	harbour_infrastructure_1 = yes
	marketplace = yes
}
1500.1.1 = {
	is_city = yes
}
1500.3.3 = {
	base_tax = 2
}
1640.12.1 = {
	trade_goods = wine
} # Sugar production shifted to Brazil
