# KKZ - Kakizaki

government = japanese_monarchy
government_rank = 1
mercantilism = 0.0
primary_culture = tohoku
religion = mahayana
technology_group = chinese
capital = 2299
#daimyo = yes

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
	#set_variable = { which = "centralization_decentralization" value = 2 }
	add_absolutism = -100
	add_absolutism = 30
}

1352.1.1 = {
	monarch = {
		name = "Takahiro"
		dynasty = "Matzumae"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
