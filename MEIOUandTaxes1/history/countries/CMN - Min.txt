# MIN - Min
# LS - Chinese Civil War
# 2010-jan-21 - FB - HT3 changes

government = chinese_monarchy_5 government_rank = 1
# aristocracy_plutocracy = 4
mercantilism = 0.0
# secularism_theocracy = -4
technology_group = chinese
religion = confucianism
primary_culture = minyu
add_accepted_culture = minnan
add_accepted_culture = minbei
capital = 1053

1000.1.1 = {
	add_country_modifier = { name = title_4 duration = -1 }
	set_country_flag = title_4
	#set_variable = { which = "centralization_decentralization" value = 2 }
	add_absolutism = -100
	add_absolutism = 30
}

1319.1.1 = {
	monarch = {
		name = "Youding"
		dynasty = "Chen"
		ADM = 5
		DIP = 3
		MIL = 5
	}
}

1662.1.1 = {
	monarch = {
		name = "Jingzhong"
		dynasty = "Geng"
		ADM = 5
		DIP = 5
		MIL = 6
	}
}
