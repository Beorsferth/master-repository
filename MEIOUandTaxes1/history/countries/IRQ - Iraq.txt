# IRQ - Iraq
# 2010-jan-16 - FB - HT3 changes

government = despotic_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = iraqi
religion = shiite
technology_group = muslim
capital = 410

1000.1.1 = {
	add_country_modifier = { name = title_4 duration = -1 }
	set_country_flag = title_4
	#set_variable = { which = "centralization_decentralization" value = 5 }
	add_absolutism = -100
	add_absolutism = 0
}
