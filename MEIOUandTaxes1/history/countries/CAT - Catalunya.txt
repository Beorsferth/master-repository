# CAT - Catalunya

government = feudal_monarchy government_rank = 1
mercantilism = 0.0
religion = catholic
primary_culture = catalan
technology_group = western
capital = 213	# Barcelona

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5
	#set_variable = { which = "centralization_decentralization" value = 3 }
	add_absolutism = -100
	add_absolutism = 20
}
1516.1.23 = {
	government = despotic_monarchy remove_country_modifier = title_5 clr_country_flag = title_5 add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5
}

1641.1.26 = {
	monarch = {
		name = "Pau Claris i Casademunt"
		ADM = 4
		DIP = 4
		MIL = 3
	}
	government = constitutional_republic
}

# End of the short-lived Republic of Catalonia
