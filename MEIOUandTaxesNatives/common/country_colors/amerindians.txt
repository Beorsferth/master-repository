# Country unit colors.

PAM = {
    color1= { 62 122 189  }
    color2= { 248 229 12 }
    color3= { 62 122 189  }
}

GCC = {
    color1= { 247 206 12  }
    color2= { 50 50 50  }
    color3= { 193 26 14  }
}


ZCT = {
    color1= { 193 26 14  }
    color2= { 255 255 255  }
    color3= { 193 26 14  }
}


TPC = {
    color1= { 62 122 189  }
    color2= { 255 255 255  }
    color3= { 62 122 189  }
}


CXC = {
    color1= { 191 113 133  }
    color2= { 107 94 169  }
    color3= { 121 162 109  }
}


TCX = {
    color1= { 193 26 14  }
    color2= { 247 206 12  }
    color3= { 193 26 14  }
}


GMR = {
    color1= { 255 255 255  }
    color2= { 193 65 56  }
    color3= { 255 255 255  }
}


APA = {
    color1= { 170 0 6  }
    color2= { 60 60 60  }
    color3= { 247 206 12  }
}


NJO = {
    color1= { 45 84 173  }
	color2= { 255 255 255  }
    color3= { 255 255 255  }
}


PUE = {
    color1= { 45 84 173  }
	color2= { 255 255 255  }
    color3= { 247 206 12  }
}


PIM = {
    color1= { 140 207 116  }
    color2= { 255 255 255  }
    color3= { 83 149 105  }
}


COM = {
    color1= { 154 162 225  }
    color2= { 127 137 212  }
    color3= { 204 210 255  }
}


PAW = {
    color1= { 225 154 154  }
    color2= { 127 137 212  }
    color3= { 204 210 255  }
}

