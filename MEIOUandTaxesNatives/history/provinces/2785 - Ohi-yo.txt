# No previous file for Ohi-yo

culture = shawnee
religion = totemism
capital = "Ohi-yo"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 5
native_ferocity = 1 
native_hostileness = 6

1650.1.1  = {
	owner = SHA
	controller = SHA
	add_core = SHA
	is_city = yes
	trade_goods = fur
} #Extent of the Shawnee at start of the Beaver Wars
1656.1.1  = { 	owner = IRO
		controller = IRO
		citysize = 100
		culture = iroquois
}#Driven off by Iroquois raids
1671.1.1  = {  } # Abraham Wood
1679.1.1  = {  } # Ren�-Robert Cavelier
1701.8.14 = {
	owner = XXX
	controller = XXX
	culture = shawnee
	citysize = 0
} #Vast areas left deserted after the Beaver Wars and the Iroquois withdrawal
1707.5.12 = {  }
1750.1.1  = {
	owner = LEN
	controller = LEN
	add_core = LEN
	is_city = yes
	culture = lenape
} #Lenape arrives
1795.8.3  = { owner = USA
		controller = USA
		culture = american
		religion = protestant
	    } # Treaty of Greenville, much of Ohio ceded by Natives.
