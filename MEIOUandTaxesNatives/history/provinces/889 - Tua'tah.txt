# No previous file for Tua'tah

owner = NJO
controller = NJO
add_core = NJO
is_city = yes
culture = navajo
religion = totemism
capital = "Tua'tah"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 20
native_ferocity = 4
native_hostileness = 8


1541.1.1 = {  } # Francisco V�squez de Coronado
1598.1.1 = {
	owner = SPA
	controller = SPA
	capital = "Santa Fe"
	citysize = 480
}# Pedro de Peralta
1623.1.1   = { add_core = SPA is_city = yes }
1750.1.1   = {
	add_core = MEX
	culture = mexican
	citysize = 2000
}
1810.9.16  = {
	owner = MEX
} # Declaration of Independence
1821.8.24  = {
	controller = MEX
	remove_core = SPA
} # Treaty of Cordba
1832.10.1  = {
	unrest = 3
} # Convention of 1832
1833.4.1   = {
	unrest = 6
} # Convention of 1833
