# No previous file for Alligewnenk

culture = erielhonan
religion = totemism
capital = "Alligewnenk"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 35
native_ferocity = 8
native_hostileness = 6

1650.1.1  = { owner = HUR
		controller = HUR
		add_core = HUR
		trade_goods = fur } #Huron escape to the Erie and form alliance
1656.1.1  = { 	owner = IRO
		controller = IRO
		add_core = IRO
		culture = iroquois } #Taken by Iroquois in Beaver Wars.
1679.1.1  = {  }
1707.5.12 = {  }
1750.1.1  = {	owner = FRA
		controller = FRA
		citysize = 300
		culture = francien
		religion = catholic } #Fort Presqu'Isle
1763.2.10 = {	owner = GBR
		controller = GBR
		culture = english
		religion = protestant
	    } # Treaty of Paris - ceded to Britain, France gave up its claim
1763.10.9 = {	owner = IRO
		controller = IRO
		add_core = IRO
		culture = iroquois
		religion = totemism
	    } # Royal proclamation, Britan recognize native lands (as protectorates)
1784.10.22  = {	owner = USA
		controller = USA
		culture = american
		religion = protestant } #Second Treaty of Fort Stanwix, Iroquois confederacy no longer dominant
1794.6.1  = { unrest = 5 }	# Whiskey rebellion, opposition to federal taxation
1794.9.7  = { unrest = 0 } # The revolt is suppressed
1800.1.1  = { citysize = 1200 }
1809.10.22 = { add_core = USA }
