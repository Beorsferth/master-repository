# No previous file for Hidatsa

#owner = KIO
#controller = KIO
#add_core = KIO
#is_city = yes
culture = nakota
religion = totemism
capital = "Hidatsa"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 5
native_ferocity = 3
native_hostileness = 4

1760.1.1  = {	owner = ASI
		controller = ASI
		add_core = ASI
		culture = nakota
		trade_goods = fur
		is_city = yes } #Great Plain tribes spread over vast territories
