########################################
#              Japan Events            #
########################################

# The $DYNASTYNAME$ shogunate has fallen!
#country_event = { # Modded
#	id = japan.1
#	title = "japan.EVTNAME1"
#	desc = "japan.EVTDESC1"
#	picture = SHOGUNATE_eventPicture
#	
#	major = yes
#	is_triggered_only = yes
#	
#	trigger = {
#		FROM = {
#			tag = JAP
#		}
#		OR = {
#			government = early_medieval_japanese_gov
#			government = late_medieval_japanese_gov
#			government = early_modern_japanese_gov
#		}
#		NOT = { government_rank = 6 }
#	}
#	
#	option = {
#		name = "japan.EVTOPTA1"
#		change_tag = JAP
#		swap_free_idea_group = yes		#keep progress
#		if = {
#			limit = {
#				owns = 2283 	# Kyoto
#			}
#			set_capital = 2283
#		}
#		remove_country_modifier = daimyo_reformed_adm_1
#		remove_country_modifier = daimyo_reformed_dip_1
#		remove_country_modifier = daimyo_reformed_mil_1
#		remove_country_modifier = japanese_overextention_daimyo_1
#		remove_country_modifier = daimyo_reformed_adm_2
#		remove_country_modifier = daimyo_reformed_dip_2
#		remove_country_modifier = daimyo_reformed_mil_2
#		remove_country_modifier = japanese_overextention_daimyo_2
#		remove_country_modifier = japanese_overextention_daimyo_3
#		set_government_rank = 5
#	}
#}

country_event = {
	id = japan.2
	title = "japan.EVTNAME1"
	desc = "japan.EVTDESC1"
	picture = SHOGUNATE_eventPicture
	
	major = yes
	is_triggered_only = yes
	
	trigger = {
		OR = {
			government = early_medieval_japanese_gov
			government = late_medieval_japanese_gov
			government = early_modern_japanese_gov
		}
		NOT = { government_rank = 5 }
		FROM = {
			OR = {
				government = early_medieval_japanese_gov
				government = late_medieval_japanese_gov
				government = early_modern_japanese_gov
			}
			NOT = { government_rank = 6 }
			government_rank = 5
		}
	}
	
#	immediate = { set_government_rank = 5 }
	
	option = {
		name = "japan.EVTOPTA1"
#		if = {
#			limit = {
#				government = early_modern_japanese_gov
#			}
#			remove_country_modifier = japanese_overextention_daimyo_3
#		}
#		if = {
#			limit = {
#				government = late_medieval_japanese_gov
#			}
#			remove_country_modifier = daimyo_reformed_adm_2
#			remove_country_modifier = daimyo_reformed_dip_2
#			remove_country_modifier = daimyo_reformed_mil_2
#			remove_country_modifier = japanese_overextention_daimyo_2
#		}
#		if = {
#			limit = {
#				government = early_medieval_japanese_gov
#			}
#			remove_country_modifier = daimyo_reformed_adm_1
#			remove_country_modifier = daimyo_reformed_dip_1
#			remove_country_modifier = daimyo_reformed_mil_1
#			remove_country_modifier = japanese_overextention_daimyo_1
#		}
#		if = {
#			limit = {
#				owns = 2283 	# Kyoto
#			}
#			set_capital = 2283
#		}
#		swap_national_ideas_effect = yes
	}
}

country_event = {
	id = japan.3
	title = "japan.3.n"
	desc = "japan.3.t"
	picture = SHOGUNATE_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "japan.3.a"
		ai_chance = { factor = 100 }
		FROM = {
			country_event = {
				id = japan.4
				days = 5
			}
		}
	}
	
	option = {
		name = "japan.3.b"
		ai_chance = { factor = 0 }
		FROM = {
			country_event = {
				id = japan.5
				days = 5
			}
		}
	}
}

	
country_event = {
	id = japan.4
	title = "japan.4.n"
	desc = "japan.4.t"
	picture = SHOGUNATE_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "japan.4.a"
		set_government_rank = 5
		remove_country_modifier = daimyo_reformed_adm_1
		remove_country_modifier = daimyo_reformed_dip_1
		remove_country_modifier = daimyo_reformed_mil_1
		remove_country_modifier = daimyo_reformed_adm_2
		remove_country_modifier = daimyo_reformed_dip_2
		remove_country_modifier = daimyo_reformed_mil_2
		random_country = {
			limit = { overlord_of = ROOT }
			free_vassal = ROOT 
		}
		FROM = {
			set_government_rank = 4
		remove_country_modifier = shogunate_reformed_adm_1
		remove_country_modifier = shogunate_reformed_dip_1
		remove_country_modifier = shogunate_reformed_mil_1
		remove_country_modifier = shogunate_reformed_adm_2
		remove_country_modifier = shogunate_reformed_dip_2
		remove_country_modifier = shogunate_reformed_mil_2
		}
	}
}

country_event = {
	id = japan.5
	title = "japan.5.n"
	desc = "japan.5.t"
	picture = SHOGUNATE_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "japan.5.a"
		every_province = {
			limit = {
				owned_by = FROM
			}
			add_core = ROOT
		}
		every_subject_country = {
			limit = {
				culture_group = japanese
			}
			add_liberty_desire = 15
		}
	}
}

# no longer seems to be triggered anywhere.
#country_event = {
#	id = japan.6
#	title = "japan.6.n"
#	desc = "japan.6.t"
#	picture = SHOGUNATE_eventPicture
#	
#	is_triggered_only = yes
#	
#	option = {
#		name = "japan.6.a"
#		if = {
#			limit = {
#				FROM = {
#					exists = yes
#				}
#			}
#			FROM = { inherit = PREV }
#		}
#		if = {
#			limit = {
#				FROM = {
#					exists = no
#				}
#			}
#			JAP = { inherit = PREV }
#		}
#	}
#}

# iwami Mine
country_event = {
	id = japan.7
	title = "japan.7.name"
	desc = "japan.7.desc"
	picture = TRADEGOODS_eventPicture
	
	fire_only_once = yes
		
	trigger = {
		adm_tech = 20
		owns = 3965
	}
	
	mean_time_to_happen = {
		months = 600
	}
	
	option = {
		name = "japan.7.opta"
		ai_chance = { factor = 30 }
		3965 = {
			change_trade_goods = silver
			add_permanent_province_modifier = {
				name = reconfiguring_industry_short_em
				duration = -1			# was 7300
			}
			set_province_flag = reconfiguring_industry_short
		}
	}
	
	option = {
		name = "japan.7.optb"
		ai_chance = { factor = 30 }
		3965 = {
			change_trade_goods = silver
			add_permanent_province_modifier = {
				name = "iwami_small_modifier"
				duration = -1
			}
			add_permanent_province_modifier = {
				name = reconfiguring_industry_medium_em
				duration = -1			# was 14600
			}
			set_province_flag = reconfiguring_industry_medium
		}
	}
	
	option = {
		name = "japan.7.optc"
		ai_chance = { factor = 30 }
		3965 = {
			change_trade_goods = silver
			add_permanent_province_modifier = {
				name = "iwami_large_modifier"
				duration = -1
			}
			add_permanent_province_modifier = {
				name = reconfiguring_industry_long_em
				duration = -1			# was 21900
			}
			set_province_flag = reconfiguring_industry_long
		}
	}
	
	option = {
		name = "japan.7.optd"
		ai_chance = { factor = 10 }
		3965 = {
			set_province_flag = silver_discovered
		}
	}
}
