# Daimyo on Japan
cb_daimyo_annex = {
	valid_for_subject = no
	exclusive = yes
	independence = yes

	prerequisites = {
		is_subject_other_than_tributary_trigger = yes
		OR = {
			vassal_of = FROM
			junior_union_with = FROM
		}	
		OR = {
			government = early_medieval_japanese_gov
			government = late_medieval_japanese_gov
			government = early_modern_japanese_gov
		}
		NOT = { government_rank = 6 }
		government_rank = 3
		FROM = { 
			OR = { 
				government = early_medieval_japanese_gov
				government = late_medieval_japanese_gov
				government = early_modern_japanese_gov
			}
			government_rank = 5
		}
		is_revolution_target = no
	}
	
	war_goal = defend_capital_independence
}

cb_independent_daimyo_annex = {
	valid_for_subject = no
	exclusive = yes

	prerequisites = {
		is_free_or_tributary_trigger = yes
		NOT = {
			vassal_of = FROM
			junior_union_with = FROM
		}	
		OR = {
			government = early_medieval_japanese_gov
			government = late_medieval_japanese_gov
			government = early_modern_japanese_gov
		}
		NOT = { government_rank = 6 }
		government_rank = 3
		FROM = { 
			OR = { 
				government = early_medieval_japanese_gov
				government = late_medieval_japanese_gov
				government = early_modern_japanese_gov
			}
			government_rank = 5
		}
		is_revolution_target = no
	}
	
	war_goal = annex_country	
}

cb_shogun_annex = {
	valid_for_subject = no
	prerequisites = {
		OR = { 
			government = early_medieval_japanese_gov



			government = late_medieval_japanese_gov
			government = early_modern_japanese_gov


		}
		government_rank = 5
		FROM = { 
			OR = {
				government = early_medieval_japanese_gov
				government = late_medieval_japanese_gov
				government = early_modern_japanese_gov
			}
			NOT = { government_rank = 5 }
			government_rank = 3
#			OR = {
#				num_of_cities = 10
#				is_free_or_tributary_trigger = yes
#			}			

		}
		is_revolution_target = no
	}
	
	war_goal = annex_country	
}

# Sengoku - Daimyo infighting
cb_sengoku = {
	valid_for_subject = no

	prerequisites = {
		OR = {
			

			government = late_medieval_japanese_gov
			government = early_modern_japanese_gov
		}
		NOT = { government_rank = 5 }
		

		is_neighbor_of = FROM




		from = { 
			OR = {
				government = early_medieval_japanese_gov
				government = late_medieval_japanese_gov
				government = early_modern_japanese_gov
			}
			NOT = { government_rank = 5 }


		}
		is_revolution_target = no
	}
	
	war_goal = annex_country	
}